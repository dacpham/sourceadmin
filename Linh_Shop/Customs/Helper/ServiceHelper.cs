﻿using ShopRetail.Models;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Configuration;
using System.Net.Mail;
using System.Text;
using System.Timers;
using System.Web;
using System.Web.Configuration;

namespace ShopRetail.Customs.Helper
{
    public class ServiceHelper
    {
        public ServiceHelper()
        {

        }
        private static Timer _timer;
        private static readonly object Locker = new object();

        #region Schedule

        public static void SetSchedule(int periodMinutes)
        {
            if (periodMinutes <= 0) return;
            if (_timer != null)
                StopSchedule();
            _timer = new Timer();
            _timer.Elapsed += ScheduleStart;
            _timer.Interval = TimeSpan.FromMinutes(periodMinutes).TotalMilliseconds;
            _timer.Enabled = true;
        }

        public static void StopSchedule()
        {
            _timer.Stop();
            _timer.Dispose();
        }

        private static void ScheduleStart(object sender, ElapsedEventArgs e)
        {
            try
            {
                AutoSendMail();
            }
            catch
            {
                // ignored
            }
        }

        public static void AutoSendMail()
        {
        //    //var guests = GuestRepository.Web_GetSentMails();
        //    string cdverify = string.Empty;
        //    if (guests.Any())
        //    {
        //        foreach (var item in guests)
        //        {
        //            SendMail(item);
        //        }
        //    }
        }
        public static bool SendMail()
        {
            try
            {
                //string cdverify = string.Empty;
                //cdverify = Utils.GeneratePassword(true);
                //string host = ConfigurationManager.AppSettings["StgBase"]; 
                //String path = Locate.Url("/Account/VerifiCode/{0}.html", Utils.StringAlias(cdverify));
                //host = host.TrimEnd("/");
                //path = host + path;
                //string strbody = "Hãy nhấp để vào đường link dưới đây để hoàn tất việc đăng ký tài khoản :" + path;
                //var configurationFile = WebConfigurationManager.OpenWebConfiguration("~/web.config");
                //var mailSettings = configurationFile.GetSectionGroup("system.net/mailSettings") as MailSettingsSectionGroup;
                //MailModel mail = new MailModel() { mailto = guest.Email, body = strbody };
                //if( SendMailHelper.SendMail(mail))
                //{
                //    GuestRepository.Web_SetSentedMail(guest.ID, cdverify);
                //    return true;
                //}
                return false;
            }
            catch (Exception)
            {
                return false;
            }
        }

        #endregion

    }
}