﻿
using Common.Dac;
using ShopRetail.Models;
using ShopRetailModel.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace ShopRetail.Controllers
{
    public class TheLoaiController : BaseController
    {
        public TheLoaiController(IUser resUser, ICategory resCategory, IExport resExport, IEvent resEvent, IExportDetail resExportDetail, IGuest resGuest, IImport resImport, IOrder resOrder, IOrderDetail resOrderDetail, IProduct resProduct, IImportDetail resImportDetail, IProductViewed resProductViewed, ISliderInfor resSliderInfor, IStatus resStatus, IStgFiles resStgFiles) : base(resUser, resCategory, resExport, resEvent, resExportDetail, resGuest, resImport, resOrder, resOrderDetail, resProduct, resImportDetail, resProductViewed, resSliderInfor, resStatus, resStgFiles)
        {
        }

        public ActionResult Index(long id=0)
        {
            var oCategory = _resCategory.GetById(id);
            if (DACS.IsEmpty(oCategory))
            {
                SetError(DACS.T("Thể loại không tồn tại"));
                return RedirectToPath("/home.html");
            }
            CCategory = oCategory;
            Paging.PageSize = 16;
            var oContentOfType = _resProduct.GetListByCategory(id, Paging);
            var oCategories = _resCategory.GetAll();
            var omdContentCategrory = new ContentCategroryModel();
            omdContentCategrory.Contents = oContentOfType;
            omdContentCategrory.Categories = oCategories;
            omdContentCategrory.GetContentDetail();
            return GetCustResultOrView("index", omdContentCategrory);
        }
	}
}