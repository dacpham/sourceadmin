﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using ShopRetailModel.Interfaces;
using ShopRetail.Models;

namespace ShopRetail.Controllers
{
    public class HomeController : BaseController
    {
        private const long thehinhnam = 20018;
        private const long thehinhnu = 20022;

        private const long lichtapgym = 20022;

        private const long videohuongdan = 20022;

        private const long dinhduongthehinh = 20022;
        private const long kienthucthehinh = 20022;
        private const long tapyoga = 20022;

        public HomeController(IUser resUser, ICategory resCategory, IExport resExport, IEvent resEvent, IExportDetail resExportDetail, IGuest resGuest, IImport resImport, IOrder resOrder, IOrderDetail resOrderDetail, IProduct resProduct, IImportDetail resImportDetail, IProductViewed resProductViewed, ISliderInfor resSliderInfor, IStatus resStatus, IStgFiles resStgFiles) : base(resUser, resCategory, resExport, resEvent, resExportDetail, resGuest, resImport, resOrder, resOrderDetail, resProduct, resImportDetail, resProductViewed, resSliderInfor, resStatus, resStgFiles)
        {
        }

        public ActionResult Index()
        {
            var oCategories = _resCategory.GetAll();
            var oEntityGridheader = new ContentCategroryModel
            {
                Contents = _resProduct.GetAll().Take(5).ToList(),
                Categories = oCategories
            };
            var oEntityThehinhNam = new ContentCategroryModel
            {
                Contents = _resProduct.GetListByCategory(thehinhnam),
                Categories = _resCategory.GetListByField("Parent", thehinhnam),
                CategoryRoot=_resCategory.GetById(thehinhnam)
                
            };
            var oEntityThehinhNu = new ContentCategroryModel
            {
                Contents = _resProduct.GetListByCategory(thehinhnu),
                Categories = _resCategory.GetListByField("Parent", thehinhnu),
                CategoryRoot = _resCategory.GetById(thehinhnu)
            };
            var oEntityLichTapGym = new ContentCategroryModel
            {
                Contents = _resProduct.GetListByCategory(lichtapgym),
                Categories = _resCategory.GetListByField("Parent", lichtapgym),
                CategoryRoot = _resCategory.GetById(lichtapgym)
            };
            var oEntityVideoHuongDan = new ContentCategroryModel
            {
                Contents = _resProduct.GetListByCategory(videohuongdan),
                Categories = _resCategory.GetListByField("Parent", videohuongdan),
                CategoryRoot = _resCategory.GetById(videohuongdan)
            };
            var oEntityDinhDuongTheHinh = new ContentCategroryModel
            {
                Contents = _resProduct.GetListByCategory(dinhduongthehinh),
                Categories = _resCategory.GetListByField("Parent", dinhduongthehinh),
                CategoryRoot = _resCategory.GetById(dinhduongthehinh)
            };
            var oEntityKienThucTheHinh = new ContentCategroryModel
            {
                Contents = _resProduct.GetListByCategory(kienthucthehinh),
                Categories = _resCategory.GetListByField("Parent", kienthucthehinh),
                CategoryRoot = _resCategory.GetById(kienthucthehinh)
            };
            var oEntityTapYoga = new ContentCategroryModel
            {
                Contents = _resProduct.GetListByCategory(tapyoga),
                Categories = _resCategory.GetListByField("Parent", tapyoga),
                CategoryRoot = _resCategory.GetById(tapyoga)
            };
            oEntityGridheader.GetContentDetail();
            oEntityThehinhNam.GetContentDetail();
            oEntityThehinhNu.GetContentDetail();
            oEntityVideoHuongDan.GetContentDetail();
            oEntityLichTapGym.GetContentDetail();
            oEntityKienThucTheHinh.GetContentDetail();
            oEntityTapYoga.GetContentDetail();
            oEntityDinhDuongTheHinh.GetContentDetail();
            var omdHome = new HomeModel
            {
                TheHinhNam = oEntityThehinhNam,
                TheHinhNu = oEntityThehinhNu,
                GridHeader = oEntityGridheader,
                KienThucTheHinh = oEntityKienThucTheHinh,
                DinhDuongTheHinh = oEntityDinhDuongTheHinh,
                TapYoga = oEntityTapYoga,
                LichTapGym = oEntityLichTapGym,
                VideoHuongDan =oEntityVideoHuongDan
            };
            return GetCustResultOrView("Index",omdHome);
        }
    }
}