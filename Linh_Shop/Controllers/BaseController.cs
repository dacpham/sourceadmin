﻿using Common.Dac;
using DocProUtil;
using ShopRetail.Customs.Entities;
using ShopRetail.Customs.Params;
using ShopRetail.Customs.Utility;
using ShopRetail.Models;
using ShopRetailModel.Interfaces;
using ShopRetailModel.Models;
using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Web.Mvc;

namespace ShopRetail.Controllers
{
    public class BaseController : Controller
    {
        #region-------Khởi tạo-----------------
        protected IUser _resUser;
        protected ICategory _resCategory;
        protected IEvent _resEvent;
        protected IExport _resExport;
        protected IExportDetail _resExportDetail;
        protected IGuest _resGuest;
        protected IImport _resImport;
        protected IImportDetail _resImportDetail;
        protected IOrder _resOrder;
        protected IOrderDetail _resOrderDetail;
        protected IProduct _resProduct;
        protected IProductViewed _resProductViewed;
        protected ISliderInfor _resSliderInfor;
        protected IStatus _resStatus;
        protected IStgFiles _resStgFiles;

        #endregion-----------------------------

        public BaseController(IUser resUser,
            ICategory resCategory,
            IExport resExport,
            IEvent resEvent,
            IExportDetail resExportDetail,
            IGuest resGuest,
            IImport resImport,
            IOrder resOrder,
            IOrderDetail resOrderDetail,
            IProduct resProduct,
             IImportDetail resImportDetail,
        IProductViewed resProductViewed,
            ISliderInfor resSliderInfor,
            IStatus resStatus,
            IStgFiles resStgFiles)
        {
            _resUser = resUser;
            _resCategory = resCategory;
            _resExport = resExport;
            _resEvent = resEvent;
            _resExportDetail = resExportDetail;
            _resGuest = resGuest;
            _resImport = resImport;
            _resOrder = resOrder;
            _resOrderDetail = resOrderDetail;
            _resProduct = resProduct;
            _resProductViewed = resProductViewed;
            _resSliderInfor = resSliderInfor;
            _resStatus = resStatus;
            _resStgFiles = resStgFiles;
            _resImportDetail = resImportDetail;
        }
        /// <summary>
        ///     Title page
        /// </summary>
        private string _title;
        private readonly List<string> _warns = new List<string>();
        private readonly List<string> _errors = new List<string>();
        private readonly List<string> _success = new List<string>();
        private readonly List<string> _notifies = new List<string>();

        #region Form Data
        private Hashtable _data;
        protected Hashtable DATA
        {
            get
            {
                if (Equals(_data, null))
                    _data = DACS.GetDataPost();

                return _data;
            }
        }

        #endregion

        #region ResResult

        private bool _isLogout;
        private bool _isMsg;

        private bool _isDL;
        private object _wDL;
        private string _htDL;

        private bool _isCust;
        private string _htCust;

        private bool _resOnlyData;
        private dynamic _dataRes;

        internal void SetIsLogout()
        {
            _isLogout = true;
        }
        internal void SetDataResponse(dynamic data)
        {
            _dataRes = data;
        }
        internal void SetOnlyDataResponse(dynamic data)
        {
            _resOnlyData = true;
            _dataRes = data;
        }
        /// <summary>
        /// Set html of dialog
        /// </summary>
        /// <param name="html"></param>
        internal void SetHtmlDialog(string html, object width)
        {
            _isDL = true;
            _wDL = width;
            _htDL = html;
        }

        /// <summary>
        /// Set html of custom placed
        /// </summary>
        /// <param name="html"></param>
        internal void SetHtmlResponse(string html)
        {
            _isCust = true;
            _htCust = html;
        }

        protected JsonResult GetResult()
        {
            if (_resOnlyData)
                return Json(new Hashtable {
                    {"data", _dataRes ?? string.Empty}
                });

            var res = new Hashtable();
            res.Add("data", _dataRes ?? string.Empty);
            res.Add("title", ViewBag.Title ?? string.Empty);

            if (_errors.Any())
                res.Add("isErr", 1);
            if (_isLogout)
                res.Add("isLogout", 1);

            if (_isMsg)
            {
                var messages = new List<string>();
                messages.AddRange(_errors);
                messages.AddRange(_warns);
                messages.AddRange(_notifies);
                messages.AddRange(_success);

                res.Add("isMsg", 1);
                res.Add("isError", HasError);
                res.Add("isWarn", HasWarn);
                res.Add("isNotify", HasNotify);
                res.Add("isSuccess", HasSuccess);

                res.Add("msError", string.Join("\n", _errors));
                res.Add("msWarn", string.Join("\n", _warns));
                res.Add("msNotify", string.Join("\n", _notifies));
                res.Add("msSuccess", string.Join("\n", _success));
                res.Add("htMsg", GetMessages());
            }
            if (_isDL)
            {
                res.Add("isDL", 1);
                res.Add("wDL", _wDL);
                res.Add("htDL", _htDL ?? string.Empty);
            }
            if (_isCust)
            {
                res.Add("isCust", 1);
                res.Add("htCust", _htCust ?? string.Empty);
            }
            return Json(res);
            //json.MaxJsonLength = int.MaxValue;
        }

        #endregion

        #region RenderOptions
        //protected static string RenderOptions(dynamic data, bool hasUndefined = true)
        //{
        //    return HtmlOption.RenderOption(Utils.GetOptions(data, hasUndefined));
        //}
        //protected static string RenderOptions(dynamic data, int selected, bool hasUndefined = true)
        //{
        //    return HtmlOption.RenderOption(Utils.GetOptions(data, selected, hasUndefined));
        //}
        //protected static string RenderOptions(dynamic data, List<int> selecteds, bool hasUndefined = true)
        //{
        //    return HtmlOption.RenderOption(Utils.GetOptions(data, selecteds, hasUndefined));
        //}
        #endregion

        /// <summary>
        ///     Session ID
        /// </summary>
        protected string SessionID
        {
            get
            {
                try
                {
                    return Session.SessionID;
                }
                catch
                {
                    return string.Empty;
                }
            }
        }
        public Category CCategory { get; set; }
        public Product CContent { get; set; }

        /// <summary>
        ///     Current user
        /// </summary>
        //protected Guest CUser;

        /// <summary>
        ///     Pagination
        /// </summary>
        protected Pagination Paging;

        /// <summary>
        ///     Stylesheets
        /// </summary>
        protected List<string> Css;

        /// <summary>
        ///     Javascripts
        /// </summary>
        protected List<string> Scripts;
        internal bool HasError
        {
            get { return _errors.Any(); }
        }
        internal bool HasWarn
        {
            get { return _warns.Any(); }
        }
        internal bool HasNotify
        {
            get { return _notifies.Any(); }
        }
        internal bool HasSuccess
        {
            get { return _success.Any(); }
        }

        /// <summary>
        ///     Set title page
        /// </summary>
        /// <param name="title"></param>
        protected void SetTitle(string title)
        {
            _title = title;
        }

        /// <summary>
        ///     Set success
        /// </summary>
        /// <param name="success"></param>
        internal void SetSuccess(string success)
        {
            _isMsg = true;
            _success.Add(success);
            Session["Success"] = _success;
        }
        internal void SetSuccess(List<string> success)
        {
            if (!Equals(success, null) && success.Any())
            {
                _isMsg = true;
                _success.AddRange(success);
                Session["Success"] = _success;
            }
        }
        internal void SetNotify(string notify)
        {
            _isMsg = true;
            _notifies.Add(notify);
            Session["Notifies"] = _notifies;
        }
        internal void SetNotify(List<string> notifies)
        {
            if (!Equals(notifies, null) && notifies.Any())
            {
                _isMsg = true;
                _notifies.AddRange(notifies);
                Session["Notifies"] = _notifies;
            }
        }

        /// <summary>
        ///     Set errors
        /// </summary>
        /// <param name="error"></param>
        internal void SetError(string error)
        {
            _isMsg = true;
            _errors.Add(error);
            Session["Errors"] = _errors;
        }
        internal void SetErrors(List<string> errors)
        {
            if (!Equals(errors, null) && errors.Any())
            {
                _isMsg = true;
                _errors.AddRange(errors);
                Session["Errors"] = _errors;
            }
        }

        /// <summary>
        ///     Set warnings
        /// </summary>
        /// <param name="warn"></param>
        internal void SetWarn(string warn)
        {
            _isMsg = true;
            _warns.Add(warn);
            Session["Warns"] = _warns;
        }
        internal void SetWarns(List<string> warns)
        {
            if (!Equals(warns, null) && warns.Any())
            {
                _isMsg = true;
                _warns.AddRange(warns);
                Session["Warns"] = _warns;
            }
        }

        /// <summary>
        ///     Include stylesheet
        /// </summary>
        /// <param name="item"></param>
        protected void IncludeCss(string item)
        {
            if (Equals(Css, null))
                Css = new List<string>();

            Css.Add(item);
        }

        /// <summary>
        ///     Include javascript
        /// </summary>
        /// <param name="item"></param>
        protected void IncludeScript(string item)
        {
            if (Equals(Scripts, null))
                Scripts = new List<string>();

            Scripts.Add(item);
        }

        /// <summary>
        ///     Called before the action method is invoked.
        /// </summary>
        /// <param name="filterContext">
        ///     Information about the current request and action.
        /// </param>
        protected override void OnActionExecuting(ActionExecutingContext filterContext)
        {
            base.OnActionExecuting(filterContext);
            ViewBag.Title = ("LShoper");
            Paging = new Pagination(HttpContext.Request);
            GetCUser();
        }
        protected void GetCUser()
        {
            //CUser = AclConfig.CurrentGuest;DacPV
            //ViewBag.CUser = CUser;
        }
        /// <summary>
        ///     Called after the action method is invoked.
        /// </summary>
        /// <param name="filterContext">
        ///     Information about the current request and action.
        /// </param>
        protected override void OnActionExecuted(ActionExecutedContext filterContext)
        {
            base.OnActionExecuted(filterContext);
            if (Request.IsAjaxRequest() == false)
            {
                //if (CUser.ID > 0)
                //{
                //    //TODO DacPV
                //}

                #region Css, JS, CDATA

                //Is mobile
                if (Request.Browser.IsMobileDevice)
                {
                    IncludeCss("~/Assets/jquery/css/jquery.mobile.css");
                    IncludeScript("~/Assets/jquery/js/jquery.mobile.js");
                    IncludeScript("~/Assets/jquery/js/jquery.plugins.js");
                    IncludeScript("~/Assets/jquery/js/jquery.functions.js");
                }

                //Set title
                if (!Equals(_title, null))
                    ViewBag.Title = _title;

                //Set stylesheets
                if (!Equals(Css, null))
                    ViewBag.Css = Css.ToArray();

                //Set javascripts
                if (!Equals(Scripts, null))
                    ViewBag.Scripts = Scripts.ToArray();

                //Set pagination
                ViewBag.Pagination = Paging;

                //set dashboard

                //Set CDATA
                #endregion
            }
            ViewBag.Headers = _resCategory.GetAll();
            var oBreadCumbs = new List<BreadCumdModel>();
            if (!DACS.IsEmpty(CCategory))
            {
                var ids = CCategory.Parents.Split('|');
                var oCategorys = _resCategory.GetListByFields("ID", ids).OrderBy(t => t.ID).ToList();
                oBreadCumbs.Add(new BreadCumdModel
                {
                    Name = "Trang chủ",
                    Link = DACS.ReUrl("/home.html")
                });
                foreach (var item in oCategorys)
                {
                    var oBreadCumb = new BreadCumdModel();
                    oBreadCumb.Name = item.Name;
                    oBreadCumb.Link = DACS.ReUrl("/the-loai/{0}.html", DACS.StringAlias(item.Name, item.ID));
                    oBreadCumbs.Add(oBreadCumb);
                }
                oBreadCumbs.Add(new BreadCumdModel
                {
                    Name = CCategory.Name,
                    Link = ""
                });
            }
            if (!DACS.IsEmpty(CContent))
            {
                var oCategory = _resCategory.GetById(CContent.IDCategory);
                var ids = CCategory.Parents.Split('|');
                var oCategorys = _resCategory.GetListByFields("ID", ids).OrderBy(t => t.ID).ToList();
                oBreadCumbs.Add(new BreadCumdModel
                {
                    Name = "Trang chủ",
                    Link = DACS.ReUrl("/home.html")
                });
                foreach (var item in oCategorys)
                {
                    var oBreadCumb = new BreadCumdModel();
                    oBreadCumb.Name = item.Name;
                    oBreadCumb.Link = DACS.ReUrl("/the-loai/{0}.html", DACS.StringAlias(item.Name, item.ID));
                    oBreadCumbs.Add(oBreadCumb);
                }
                oBreadCumbs.Add(new BreadCumdModel
                {
                    Name = CContent.Name,
                    Link = ""
                });
            }
            ViewBag.BreadCumb = oBreadCumbs;
        }
        protected override void OnResultExecuted(ResultExecutedContext filterContext)
        {
            base.OnResultExecuted(filterContext);

            if (!Equals(Session["user"], null))
                Session.Remove("user");
        }
        protected ViewResult ViewDelete(DeleteParam deleteParam)
        {
            ViewBag.DeleteParam = deleteParam;
            if (!string.IsNullOrEmpty(deleteParam.Title))
                SetTitle(deleteParam.Title);
            return View("~/Views/Shared/IsDelete.cshtml");
        }
        protected ViewResult ViewDeletes(DeletesParam deletesParam)
        {
            ViewBag.DeletesParam = deletesParam;
            if (!string.IsNullOrEmpty(deletesParam.Title))
                SetTitle(deletesParam.Title);
            return View("~/Views/Shared/IsDeletes.cshtml");
        }
        protected string GetFormSearch(string name)
        {
            return string.IsNullOrEmpty(name)
                ? string.Empty
                : GetView(string.Format(
                    "~/Views/Shared/Searchs/{0}.cshtml",
                    name
                ));
        }
        protected string GetView(string viewName, object data = null)
        {
            try
            {
                ViewBag.Pagination = Paging;
                if (!Equals(_title, null))
                    ViewBag.Title = _title;

                using (var sw = new StringWriter())
                {
                    ControllerContext.Controller.ViewData.Model = data;
                    var viewResult = ViewEngines.Engines.FindPartialView(ControllerContext, viewName);
                    var viewContext = new ViewContext(ControllerContext, viewResult.View, ViewData, TempData, sw);
                    viewResult.View.Render(viewContext, sw);

                    return sw.ToString();
                }
            }
            catch (Exception ex)
            {
                //Loger.Log(ex);
                return string.Empty;
            }
        }
        protected string GetMessages()
        {
            return GetView("~/Views/Shared/Message.cshtml");
        }
        protected string GetReferrerOrDefault(string defaultPath)
        {
            if (Equals(Request.UrlReferrer, null))
                return defaultPath;

            return Request.UrlReferrer.ToString();
        }
        protected string GetRedirectOrDefault(string defaultPath)
        {
            var redirectPath = DACS.GetString(DATA, "RedirectPath");
            return string.IsNullOrEmpty(redirectPath)
                ? defaultPath
                : redirectPath;
        }
        protected ActionResult GetResultOrRedirectDefault(string defaultPath)
        {
            if (Request.IsAjaxRequest())
                return GetResult();
            return RedirectToPath(GetRedirectOrDefault(defaultPath));
        }
        protected ActionResult GetResultOrReferrerDefault(string defaultPath)
        {
            if (Request.IsAjaxRequest())
                return GetResult();
            return RedirectToPath(GetReferrerOrDefault(defaultPath));
        }
        protected ActionResult GetResultOrView(string viewName, object data = null)
        {
            if (Request.IsAjaxRequest())
            {
                return GetResult();
            }
            return GetViewResult(viewName, data);
        }
        protected ActionResult GetCustResultOrView(ViewParam viewParam)
        {
            if (Request.IsAjaxRequest())
            {
                if (!Equals(viewParam.Data, null))
                    SetDataResponse(viewParam.Data);

                SetHtmlResponse(GetView(viewParam.ViewNameAjax ?? viewParam.ViewName, viewParam.Data));
                return GetResult();
            }
            return GetViewResult(viewParam.ViewName, viewParam.Data);
        }
        protected ActionResult GetDialogResultOrView(ViewParam viewParam)
        {
            if (Request.IsAjaxRequest())
            {
                if (!Equals(viewParam.Data, null))
                    SetDataResponse(viewParam.Data);

                SetHtmlDialog(GetView(viewParam.ViewNameAjax ?? viewParam.ViewName,
                    viewParam.Data
                ), viewParam.Width ?? 600);
                return GetResult();
            }
            return GetViewResult(viewParam.ViewName, viewParam.Data);
        }
        protected ActionResult GetCustResultOrView(string viewName, object data = null)
        {
            if (Request.IsAjaxRequest())
            {
                //if (!Equals(data, null))
                //    SetDataResponse(data);

                SetHtmlResponse(GetView(viewName, data));
                return GetResult();
            }
            return GetViewResult(viewName, data);
        }
        protected ActionResult GetDialogResultOrView(string viewName, object data = null, object w = null)
        {
            if (Request.IsAjaxRequest())
            {
                //if (!Equals(data, null))
                //    SetDataResponse(data);

                SetHtmlDialog(GetView(viewName, data), w ?? 600);
                return GetResult();
            }
            return GetViewResult(viewName, data);
        }
        protected ActionResult GetViewResult(string viewName, object data = null)
        {
            return View(viewName, data);

        }
        protected RedirectResult RedirectToPath(string path)
        {
            path = (path ?? "/");
            return Redirect(path);
        }
        protected RedirectResult RedirectToPath(string path, params object[] param)
        {
            return Redirect(string.Format(path, param));
        }

    }
}