
//--DOCUMENT READY FUNCTION BEGIN
jQuery(document).ready(function () {


 
	function Init_slider(){
       jQuery(".td_block_wrap").each(function () {
       		jQuery(".td-subcat-item .td-subcat-link").each(function () {
       			if(jQuery(this).hasClass("td-cur-simple-item")){
					var slider_id = jQuery(this).attr("id");
					jQuery(this).parents(".td_block_wrap").find(slider_id).find(".custom_row").owlCarousel({
			            nav: false,
			            autoplay: false,
			            autoplayTimeout: 6000,
			            autoplayHoverPause: false,
			            loop: true,
			            dots: false,
			            slideBy: 3,
			            responsive: {
			                0: {
			                    items: 1,
			                },
			                480: {
			                    items: 2,
			                },
			                767: {
			                    items: 3,

			                },
			                991: {
			                    items: 4,

			                },
			                1200: {
			                    items: 5
			                }
			            }
			        });
			        jQuery(this).parents(".td_block_wrap").find(slider_id).find(".td-next-prev-wrap").find(".td-ajax-prev-page").click(function () {
				        jQuery(this).parents(".td_block_wrap").find(slider_id).find(".custom_row").trigger('prev.owl.carousel');
				        return false;
				    });

				    jQuery(this).parents(".td_block_wrap").find(slider_id).find(".td-next-prev-wrap").find(".td-ajax-next-page").click(function () {
				        jQuery(this).parents(".td_block_wrap").find(slider_id).find(".custom_row").trigger('next.owl.carousel');
				        return false;
				    });


       			}
       		});
      });
   }
   Init_slider();

      jQuery('.td-subcat-item .td-subcat-link').click(function(e){
      	e.preventDefault();
         var tab_id = jQuery(this).attr("id");
         jQuery(this).parents(".td-subcat-list").find(".td-subcat-link").removeClass("td-cur-simple-item");
         jQuery(this).addClass("td-cur-simple-item");
         jQuery(this).parents(".td_block_wrap").find(".td_block_inner").hide();
         jQuery(this).parents(".td_block_wrap").find(tab_id).show();
         jQuery(this).parents(".td_block_wrap").find(tab_id).addClass("td_animated_xlong");
         jQuery(this).parents(".td_block_wrap").find(tab_id).addClass("td_fadeInDown");
         Init_slider(); 
      });









      
});
//--DOCUMENT READY FUNCTION END



function td_smart_list_dropdown() {
	jQuery(".td-smart-list-dropdown").on("change", function() {
		window.location = this.value
	})
}

function td_done_resizing() {
	td_resize_videos()
}

function td_resize_videos() {
	jQuery(document).find('iframe[src*="youtube.com"]').each(function() {
		var a = jQuery(this).parent().parent().parent(),
			b = jQuery(this).parent().hasClass("td_wrapper_playlist_player_vimeo"),
			c = a.hasClass("vc_video-aspect-ratio-43"),
			d = a.hasClass("vc_video-aspect-ratio-235");
		if (b || c || d);
		else {
			var e = jQuery(this);
			e.attr("width", "100%");
			var f = e.width();
			e.css("height", .5625 * f, "important")
		}
	}), jQuery(document).find('iframe[src*="vimeo.com"]').each(function() {
		var a = jQuery(this).parent().parent().parent(),
			b = jQuery(this).parent().hasClass("td_wrapper_playlist_player_vimeo"),
			c = a.hasClass("vc_video-aspect-ratio-43"),
			d = a.hasClass("vc_video-aspect-ratio-235");
		if (b || c || d);
		else {
			var e = jQuery(this);
			e.attr("width", "100%");
			var f = e.width();
			e.css("height", .5625 * f, "important")
		}
	}), jQuery(document).find('iframe[src*="dailymotion.com"]').each(function() {
		var a = jQuery(this).parent().parent().parent(),
			b = a.hasClass("vc_video-aspect-ratio-43"),
			c = a.hasClass("vc_video-aspect-ratio-235");
		if (b || c);
		else {
			var d = jQuery(this);
			d.attr("width", "100%");
			var e = d.width();
			d.css("height", .6 * e, "important")
		}
	}), jQuery(document).find('iframe[src*="facebook.com/plugins/video.php"]').each(function() {
		var a = jQuery(this);
		if (a.parent().hasClass("wpb_video_wrapper")) {
			a.attr("width", "100%");
			var b = a.width();
			a.css("height", .5625 * b, "important")
		} else a.css("max-width", "100%")
	}), jQuery(document).find(".wp-video-shortcode").each(function() {
		var a = jQuery(this),
			b = a.width() + 3;
		jQuery(this).parent().css("height", .56 * b, "important"), a.css("width", "100%", "important"), a.css("height", "100%", "important")
	})
}

function td_mobile_menu() {}

function td_mobile_menu_toogle() {
	jQuery("#td-top-mobile-toggle a, .td-mobile-close a").click(function() {
		jQuery("body").hasClass("td-menu-mob-open-menu") ? jQuery("body").removeClass("td-menu-mob-open-menu") : jQuery("body").addClass("td-menu-mob-open-menu")
	}), jQuery(document).find("#td-mobile-nav .menu-item-has-children").each(function(a) {
		var b = "td_mobile_elem_with_submenu_" + a;
		jQuery(this).addClass(b), jQuery(this).children("a").addClass("td-link-element-after"), jQuery(this).click(function(a) {
			var b = jQuery(a.target);
			!b.length || !b.hasClass("td-element-after") && !b.hasClass("td-link-element-after") || "#" !== b.attr("href") && void 0 !== b.attr("href") || (a.preventDefault(), a.stopPropagation(), jQuery(this).toggleClass("td-sub-menu-open"))
		})
	})
}

function td_retina() {
	window.devicePixelRatio > 1 && (jQuery(".td-retina").each(function(a) {
		var b = jQuery(this).attr("src"),
			c = b.replace(".png", "@2x.png");
		c = c.replace(".jpg", "@2x.jpg"), jQuery(this).attr("src", c)
	}), jQuery(".td-retina-data").each(function(a) {
		jQuery(this).attr("src", jQuery(this).data("retina")), jQuery(this).addClass("td-retina-version")
	}))
}

function td_read_site_cookie(a) {
	for (var b = escape(a) + "=", c = document.cookie.split(";"), d = 0; d < c.length; d++) {
		for (var e = c[d];
			" " == e.charAt(0);) e = e.substring(1, e.length);
		if (0 == e.indexOf(b)) return unescape(e.substring(b.length, e.length))
	}
	return null
}

function td_set_cookies_life(a) {
	var b = new Date;
	b.setTime(b.getTime() + a[2]), document.cookie = a[0] + "=" + a[1] + "; expires=" + b.toGMTString() + "; path=/"
}

function td_events_scroll_scroll_to_top(a) {
	tdIsScrollingAnimation || (a > 400 ? td_scroll_to_top_is_visible === !1 && (td_scroll_to_top_is_visible = !0, jQuery(".td-scroll-up").addClass("td-scroll-up-visible")) : td_scroll_to_top_is_visible === !0 && (td_scroll_to_top_is_visible = !1, jQuery(".td-scroll-up").removeClass("td-scroll-up-visible")))
}

function td_post_template_6_title() {
	function f() {
		e === !1 && window.requestAnimationFrame(g), e = !0
	}

	function g() {
		if (c = jQuery(document).scrollTop(), c <= 950) {
			var f = 1 - c / 800;
			tdDetect.isIe8 === !0 && (f = 1), f = Math.round(100 * f) / 100, a.style.opacity = f;
			var g = -Math.round(c / 4);
			tdUtil.tdMoveY(b, -g), d = -Math.round(c / 8), tdUtil.tdMoveY(a, -d)
		}
		e = !1
	}
	var d, a = document.getElementById("td_parallax_header_6"),
		b = document.getElementById("td-full-screen-header-image"),
		c = "";
	jQuery(window).scroll(function() {
		f()
	});
	var e = !1
}

function td_smart_lists_magnific_popup() {
	jQuery(".td-lightbox-enabled").magnificPopup({
		delegate: "a",
		type: "image",
		tLoading: "Loading image #%curr%...",
		mainClass: "mfp-img-mobile",
		gallery: {
			enabled: !0,
			navigateByImgClick: !0,
			preload: [0, 1],
			tCounter: tdUtil.getBackendVar("td_magnific_popup_translation_tCounter")
		},
		image: {
			tError: "<a href='%url%'>The image #%curr%</a> could not be loaded.",
			titleSrc: function(a) {
				return a.el.attr("data-caption")
			}
		},
		zoom: {
			enabled: !0,
			duration: 300,
			opener: function(a) {
				return a.find("img")
			}
		},
		callbacks: {
			change: function(a) {
				if ("" != a.el[0].id) {
					var b = a.el[0].id.split("_");
					jQuery(".td-iosSlider").iosSlider("goToSlide", parseInt(b[1]) + 1)
				} else tdModalImageLastEl = a.el, setTimeout(function() {
					tdUtil.scrollIntoView(a.el)
				}, 100)
			},
			beforeClose: function() {
				"" != tdModalImageLastEl && tdUtil.scrollIntoView(tdModalImageLastEl)
			}
		}
	})
}

function td_get_document_width() {
	var a = 0;
	return self.innerHeight ? a = self.innerWidth : document.documentElement && document.documentElement.clientHeight ? a = document.documentElement.clientWidth : document.body && (a = document.body.clientWidth), a
}

function td_get_document_height() {
	var a = 0;
	return self.innerHeight ? a = self.innerHeight : document.documentElement && document.documentElement.clientHeight ? a = document.documentElement.clientHeight : document.body && (a = document.body.clientHeight), a
}

function setMenuMinHeight() {
	if ("undefined" == typeof tdEvents.previousWindowInnerWidth) tdEvents.previousWindowInnerWidth = tdEvents.window_innerWidth;
	else if (tdEvents.previousWindowInnerWidth === tdEvents.window_innerWidth) return;
	tdEvents.previousWindowInnerWidth = tdEvents.window_innerWidth;
	var a = jQuery("#td-mobile-nav"),
		b = tdEvents.window_innerHeight + 1;
	if (a.length && a.css("min-height", b + "px"), tdDetect.isMobileDevice) {
		var c = jQuery(".td-menu-background"),
			d = jQuery(".td-search-background");
		c.length && c.css("height", b + 70 + "px"), d.length && d.css("height", b + 70 + "px")
	}
}

function td_comments_form_validation() {
	jQuery(".comment-form").submit(function(a) {
		jQuery("form#commentform :input").each(function() {
			var b = jQuery(this),
				c = jQuery(this).parent().parent();
			b.attr("aria-required") && ("" == b.val() ? (a.preventDefault(), c.addClass("td-comment-form-warnings"), "comment" == b.attr("id") ? (c.find(".td-warning-comment").show(200), b.css("border", "1px solid #ff7a7a")) : "author" == b.attr("id") ? (c.find(".td-warning-author").show(200), b.css("border", "1px solid #ff7a7a")) : "email" == b.attr("id") && (c.find(".td-warning-email").show(200), b.css("border", "1px solid #ff7a7a"))) : "email" == b.attr("id") && tdUtil.isEmail(b.val()) === !1 && (a.preventDefault(), c.addClass("td-comment-form-warnings"), c.find(".td-warning-email-error").show(200)))
		})
	}), jQuery("form#commentform :input").each(function() {
		var a = jQuery(this).parent().parent(),
			b = jQuery(this);
		b.focus(function() {
			"comment" == b.attr("id") ? (a.find(".td-warning-comment").hide(200), b.css("border", "1px solid #e1e1e1")) : "author" == b.attr("id") ? (a.find(".td-warning-author").hide(200), b.css("border", "1px solid #e1e1e1")) : "email" == b.attr("id") && (a.find(".td-warning-email").hide(200), a.find(".td-warning-email-error").hide(200), b.css("border", "1px solid #e1e1e1"))
		})
	})
}

function tdModalImage() {
	jQuery("figure.wp-caption").each(function() {
		var a = jQuery(this).children("figcaption").html();
		jQuery(this).children("a").data("caption", a)
	}), jQuery(".td-modal-image").each(function() {
		jQuery(this).parent().addClass("td-modal-image"), jQuery(this).removeClass("td-modal-image")
	}), jQuery("article").magnificPopup({
		type: "image",
		delegate: ".td-modal-image",
		gallery: {
			enabled: !0,
			tPrev: tdUtil.getBackendVar("td_magnific_popup_translation_tPrev"),
			tNext: tdUtil.getBackendVar("td_magnific_popup_translation_tNext"),
			tCounter: tdUtil.getBackendVar("td_magnific_popup_translation_tCounter")
		},
		ajax: {
			tError: tdUtil.getBackendVar("td_magnific_popup_translation_ajax_tError")
		},
		image: {
			tError: tdUtil.getBackendVar("td_magnific_popup_translation_image_tError"),
			titleSrc: function(a) {
				var b = jQuery(a.el).data("caption");
				return "undefined" != typeof b ? b : ""
			}
		},
		zoom: {
			enabled: !0,
			duration: 300,
			opener: function(a) {
				return a.find("img")
			}
		},
		callbacks: {
			change: function(a) {
				tdModalImageLastEl = a.el, tdUtil.scrollIntoView(a.el)
			},
			beforeClose: function() {
				tdAffix.allow_scroll = !1, tdUtil.scrollIntoView(tdModalImageLastEl);
				var a = setInterval(function() {
					tdIsScrollingAnimation || (clearInterval(a), setTimeout(function() {
						tdAffix.allow_scroll = !0
					}, 100))
				}, 100)
			}
		}
	}), "undefined" == typeof jetpackCarouselStrings && (jQuery("figure.gallery-item").each(function() {
		var a = jQuery(this).children("figcaption").html();
		jQuery(this).find("a").data("caption", a)
	}), jQuery(".tiled-gallery").magnificPopup({
		type: "image",
		delegate: "a",
		gallery: {
			enabled: !0,
			tPrev: tdUtil.getBackendVar("td_magnific_popup_translation_tPrev"),
			tNext: tdUtil.getBackendVar("td_magnific_popup_translation_tNext"),
			tCounter: tdUtil.getBackendVar("td_magnific_popup_translation_tCounter")
		},
		ajax: {
			tError: tdUtil.getBackendVar("td_magnific_popup_translation_ajax_tError")
		},
		image: {
			tError: tdUtil.getBackendVar("td_magnific_popup_translation_image_tError"),
			titleSrc: function(a) {
				var b = jQuery(a.el).parent().find(".tiled-gallery-caption").text();
				return "undefined" != typeof b ? b : ""
			}
		},
		zoom: {
			enabled: !0,
			duration: 300,
			opener: function(a) {
				return a.find("img")
			}
		},
		callbacks: {
			change: function(a) {
				tdModalImageLastEl = a.el, tdUtil.scrollIntoView(a.el)
			},
			beforeClose: function() {
				tdUtil.scrollIntoView(tdModalImageLastEl)
			}
		}
	}), jQuery(".gallery").magnificPopup({
		type: "image",
		delegate: ".gallery-icon > a",
		gallery: {
			enabled: !0,
			tPrev: tdUtil.getBackendVar("td_magnific_popup_translation_tPrev"),
			tNext: tdUtil.getBackendVar("td_magnific_popup_translation_tNext"),
			tCounter: tdUtil.getBackendVar("td_magnific_popup_translation_tCounter")
		},
		ajax: {
			tError: tdUtil.getBackendVar("td_magnific_popup_translation_ajax_tError")
		},
		image: {
			tError: tdUtil.getBackendVar("td_magnific_popup_translation_image_tError"),
			titleSrc: function(a) {
				var b = jQuery(a.el).data("caption");
				return "undefined" != typeof b ? b : ""
			}
		},
		zoom: {
			enabled: !0,
			duration: 300,
			opener: function(a) {
				return a.find("img")
			}
		},
		callbacks: {
			change: function(a) {
				tdModalImageLastEl = a.el, tdUtil.scrollIntoView(a.el)
			},
			beforeClose: function() {
				tdUtil.scrollIntoView(tdModalImageLastEl)
			}
		}
	}))
}

function td_resize_smartlist_slides(a) {
	var b = a.currentSlideNumber,
		c = jQuery(a.data.obj[0]).attr("id");
	tdDetect.isIe8 || (jQuery("#" + c).css("overflow", "none"), jQuery("#" + c + " .td-item").css("overflow", "visible"));
	var d = 0;
	d = jQuery("#" + c + "_item_" + b).outerHeight(!0), jQuery("#" + c + ", #" + c + " .td-slider").css({
		height: d
	})
}

function td_resize_smartlist_sliders_and_update() {
	jQuery(document).find(".td-smart-list-slider").each(function() {
		var a = jQuery(this).attr("id");
		tdDetect.isIe8 || (jQuery("#" + a).css("overflow", "none"), jQuery("#" + a + " .td-item").css("overflow", "visible"));
		var b = 0;
		b = jQuery("#" + a + "_item_" + td_history.get_current_page("slide")).outerHeight(!0), jQuery("#" + a + ", #" + a + " .td-slider").css({
			height: b
		}), tdDetect.isAndroid && setTimeout(function() {
			jQuery("#" + a).iosSlider("update")
		}, 2e3)
	})
}

function td_resize_normal_slide(a) {
	var b = 0,
		c = jQuery(a.data.obj[0]).attr("id"),
		d = td_get_document_width();
	tdDetect.isIe8 || (jQuery("#" + c).css("overflow", "none"), jQuery("#" + c + " .td-item").css("overflow", "visible"));
	var e = 0,
		f = jQuery("#" + c + "_item_" + b).outerWidth(!0),
		g = 780;
	tdDetect.isAndroid && (g = 1e3), d < g && !tdDetect.isIpad && (e = f > 300 ? .5 * f : f, jQuery("#" + c + ", #" + c + " .td-slider, #" + c + " .td-slider .td-module-thumb").css({
		height: e
	}))
}

function td_resize_normal_slide_and_update(a) {
	var b = 0,
		c = jQuery(a.data.obj[0]).attr("id"),
		d = td_get_document_width();
	tdDetect.isIe8 || (jQuery("#" + c).css("overflow", "none"), jQuery("#" + c + " .td-item").css("overflow", "visible"));
	var e = 0,
		f = jQuery("#" + c + "_item_" + b).outerWidth(!0),
		g = 780;
	tdDetect.isAndroid && (g = 1e3), d < g && !tdDetect.isIpad && (e = f > 300 ? .5 * f : f, jQuery("#" + c + ", #" + c + " .td-slider, #" + c + " .td-slider .td-module-thumb").css({
		height: e
	}), setTimeout(function() {
		jQuery("#" + c).iosSlider("update")
	}, 2e3))
}

function td_compute_backstretch_item(a) {
	var b = 100 * (tdEvents.window_innerHeight - a.offset_top) / (tdEvents.window_innerHeight + a.full_height),
		c = 100 * tdEvents.window_innerHeight / (tdEvents.window_innerHeight + a.full_height),
		d = a.offset_top / 4;
	0 == d && (d = 100);
	var e = -d / 2,
		f = (100 - b) * (d / 1.2) / (c - b);
	e += .5, a.remove_item_property("move_y"), a.add_item_property("move_y", b, 100, e, f, "");
	var g = parseFloat(1 + Math.abs(d) / a.full_height).toFixed(2);
	delete a.animation_callback, a.animation_callback = function() {
		var b = parseFloat(a.computed_item_properties.move_y).toFixed();
		a.jqueryObj.css({
			left: "50%",
			"-webkit-transform": "translate3d(-50%," + b + "px, 0px) scale(" + g + "," + g + ")",
			transform: "translate3d(-50%," + b + "px, 0px) scale(" + g + "," + g + ")"
		}), a.redraw = !1
	}
}

function td_date_i18n(a, b) {
	var d, e, c = this,
		f = /\\?(.?)/gi,
		g = function(a, b) {
			return e[a] ? e[a]() : b
		},
		h = function(a, b) {
			for (a = String(a); a.length < b;) a = "0" + a;
			return a
		};
	return e = {
		d: function() {
			return h(e.j(), 2)
		},
		D: function() {
			return tdDateNamesI18n.day_names_short[e.w()]
		},
		j: function() {
			return d.getDate()
		},
		l: function() {
			return tdDateNamesI18n.day_names[e.w()]
		},
		N: function() {
			return e.w() || 7
		},
		S: function() {
			var a = e.j(),
				b = a % 10;
			return b <= 3 && 1 == parseInt(a % 100 / 10, 10) && (b = 0), ["st", "nd", "rd"][b - 1] || "th"
		},
		w: function() {
			return d.getDay()
		},
		z: function() {
			var a = new Date(e.Y(), e.n() - 1, e.j()),
				b = new Date(e.Y(), 0, 1);
			return Math.round((a - b) / 864e5)
		},
		W: function() {
			var a = new Date(e.Y(), e.n() - 1, e.j() - e.N() + 3),
				b = new Date(a.getFullYear(), 0, 4);
			return h(1 + Math.round((a - b) / 864e5 / 7), 2)
		},
		F: function() {
			return tdDateNamesI18n.month_names[e.n() - 1]
		},
		m: function() {
			return h(e.n(), 2)
		},
		M: function() {
			return tdDateNamesI18n.month_names_short[e.n() - 1]
		},
		n: function() {
			return d.getMonth() + 1
		},
		t: function() {
			return new Date(e.Y(), e.n(), 0).getDate()
		},
		L: function() {
			var a = e.Y();
			return a % 4 === 0 & a % 100 !== 0 | a % 400 === 0
		},
		o: function() {
			var a = e.n(),
				b = e.W(),
				c = e.Y();
			return c + (12 === a && b < 9 ? 1 : 1 === a && b > 9 ? -1 : 0)
		},
		Y: function() {
			return d.getFullYear()
		},
		y: function() {
			return e.Y().toString().slice(-2)
		},
		a: function() {
			return d.getHours() > 11 ? "pm" : "am"
		},
		A: function() {
			return e.a().toUpperCase()
		},
		B: function() {
			var a = 3600 * d.getUTCHours(),
				b = 60 * d.getUTCMinutes(),
				c = d.getUTCSeconds();
			return h(Math.floor((a + b + c + 3600) / 86.4) % 1e3, 3)
		},
		g: function() {
			return e.G() % 12 || 12
		},
		G: function() {
			return d.getHours()
		},
		h: function() {
			return h(e.g(), 2)
		},
		H: function() {
			return h(e.G(), 2)
		},
		i: function() {
			return h(d.getMinutes(), 2)
		},
		s: function() {
			return h(d.getSeconds(), 2)
		},
		u: function() {
			return h(1e3 * d.getMilliseconds(), 6)
		},
		e: function() {
			console.log("Not supported (see source code of date() for timezone on how to add support)")
		},
		I: function() {
			var a = new Date(e.Y(), 0),
				b = Date.UTC(e.Y(), 0),
				c = new Date(e.Y(), 6),
				d = Date.UTC(e.Y(), 6);
			return a - b !== c - d ? 1 : 0
		},
		O: function() {
			var a = d.getTimezoneOffset(),
				b = Math.abs(a);
			return (a > 0 ? "-" : "+") + h(100 * Math.floor(b / 60) + b % 60, 4)
		},
		P: function() {
			var a = e.O();
			return a.substr(0, 3) + ":" + a.substr(3, 2)
		},
		T: function() {
			return "UTC"
		},
		Z: function() {
			return 60 * -d.getTimezoneOffset()
		},
		c: function() {
			return "Y-m-d\\TH:i:sP".replace(f, g)
		},
		r: function() {
			return "D, d M Y H:i:s O".replace(f, g)
		},
		U: function() {
			return d / 1e3 | 0
		}
	}, this.date = function(a, b) {
		return c = this, d = void 0 === b ? new Date : b instanceof Date ? new Date(b) : new Date(1e3 * b), a.replace(f, g)
	}, this.date(a, b)
}


jQuery.easing.jswing = jQuery.easing.swing, jQuery.extend(jQuery.easing, {
		def: "easeOutQuad",
		swing: function(a, b, c, d, e) {
			return jQuery.easing[jQuery.easing.def](a, b, c, d, e)
		},
		easeInQuad: function(a, b, c, d, e) {
			return d * (b /= e) * b + c
		},
		easeOutQuad: function(a, b, c, d, e) {
			return -d * (b /= e) * (b - 2) + c
		},
		easeInOutQuad: function(a, b, c, d, e) {
			return 1 > (b /= e / 2) ? d / 2 * b * b + c : -d / 2 * (--b * (b - 2) - 1) + c
		},
		easeInCubic: function(a, b, c, d, e) {
			return d * (b /= e) * b * b + c
		},
		easeOutCubic: function(a, b, c, d, e) {
			return d * ((b = b / e - 1) * b * b + 1) + c
		},
		easeInOutCubic: function(a, b, c, d, e) {
			return 1 > (b /= e / 2) ? d / 2 * b * b * b + c : d / 2 * ((b -= 2) * b * b + 2) + c
		},
		easeInQuart: function(a, b, c, d, e) {
			return d * (b /= e) * b * b * b + c
		},
		easeOutQuart: function(a, b, c, d, e) {
			return -d * ((b = b / e - 1) * b * b * b - 1) + c
		},
		easeInOutQuart: function(a, b, c, d, e) {
			return 1 > (b /= e / 2) ? d / 2 * b * b * b * b + c : -d / 2 * ((b -= 2) * b * b * b - 2) + c
		},
		easeInQuint: function(a, b, c, d, e) {
			return d * (b /= e) * b * b * b * b + c
		},
		easeOutQuint: function(a, b, c, d, e) {
			return d * ((b = b / e - 1) * b * b * b * b + 1) + c
		},
		easeInOutQuint: function(a, b, c, d, e) {
			return 1 > (b /= e / 2) ? d / 2 * b * b * b * b * b + c : d / 2 * ((b -= 2) * b * b * b * b + 2) + c
		},
		easeInSine: function(a, b, c, d, e) {
			return -d * Math.cos(b / e * (Math.PI / 2)) + d + c
		},
		easeOutSine: function(a, b, c, d, e) {
			return d * Math.sin(b / e * (Math.PI / 2)) + c
		},
		easeInOutSine: function(a, b, c, d, e) {
			return -d / 2 * (Math.cos(Math.PI * b / e) - 1) + c
		},
		easeInExpo: function(a, b, c, d, e) {
			return 0 == b ? c : d * Math.pow(2, 10 * (b / e - 1)) + c
		},
		easeOutExpo: function(a, b, c, d, e) {
			return b == e ? c + d : d * (-Math.pow(2, -10 * b / e) + 1) + c
		},
		easeInOutExpo: function(a, b, c, d, e) {
			return 0 == b ? c : b == e ? c + d : 1 > (b /= e / 2) ? d / 2 * Math.pow(2, 10 * (b - 1)) + c : d / 2 * (-Math.pow(2, -10 * --b) + 2) + c
		},
		easeInCirc: function(a, b, c, d, e) {
			return -d * (Math.sqrt(1 - (b /= e) * b) - 1) + c
		},
		easeOutCirc: function(a, b, c, d, e) {
			return d * Math.sqrt(1 - (b = b / e - 1) * b) + c
		},
		easeInOutCirc: function(a, b, c, d, e) {
			return 1 > (b /= e / 2) ? -d / 2 * (Math.sqrt(1 - b * b) - 1) + c : d / 2 * (Math.sqrt(1 - (b -= 2) * b) + 1) + c
		},
		easeInElastic: function(a, b, c, d, e) {
			a = 1.70158;
			var f = 0,
				g = d;
			return 0 == b ? c : 1 == (b /= e) ? c + d : (f || (f = .3 * e), g < Math.abs(d) ? (g = d, a = f / 4) : a = f / (2 * Math.PI) * Math.asin(d / g), -(g * Math.pow(2, 10 * (b -= 1)) * Math.sin(2 * (b * e - a) * Math.PI / f)) + c)
		},
		easeOutElastic: function(a, b, c, d, e) {
			a = 1.70158;
			var f = 0,
				g = d;
			return 0 == b ? c : 1 == (b /= e) ? c + d : (f || (f = .3 * e), g < Math.abs(d) ? (g = d, a = f / 4) : a = f / (2 * Math.PI) * Math.asin(d / g), g * Math.pow(2, -10 * b) * Math.sin(2 * (b * e - a) * Math.PI / f) + d + c)
		},
		easeInOutElastic: function(a, b, c, d, e) {
			a = 1.70158;
			var f = 0,
				g = d;
			return 0 == b ? c : 2 == (b /= e / 2) ? c + d : (f || (f = .3 * e * 1.5), g < Math.abs(d) ? (g = d, a = f / 4) : a = f / (2 * Math.PI) * Math.asin(d / g), 1 > b ? -.5 * g * Math.pow(2, 10 * (b -= 1)) * Math.sin(2 * (b * e - a) * Math.PI / f) + c : .5 * g * Math.pow(2, -10 * (b -= 1)) * Math.sin(2 * (b * e - a) * Math.PI / f) + d + c)
		},
		easeInBack: function(a, b, c, d, e, f) {
			return void 0 == f && (f = 1.70158), d * (b /= e) * b * ((f + 1) * b - f) + c
		},
		easeOutBack: function(a, b, c, d, e, f) {
			return void 0 == f && (f = 1.70158), d * ((b = b / e - 1) * b * ((f + 1) * b + f) + 1) + c
		},
		easeInOutBack: function(a, b, c, d, e, f) {
			return void 0 == f && (f = 1.70158), 1 > (b /= e / 2) ? d / 2 * b * b * (((f *= 1.525) + 1) * b - f) + c : d / 2 * ((b -= 2) * b * (((f *= 1.525) + 1) * b + f) + 2) + c
		},
		easeInBounce: function(a, b, c, d, e) {
			return d - jQuery.easing.easeOutBounce(a, e - b, 0, d, e) + c
		},
		easeOutBounce: function(a, b, c, d, e) {
			return (b /= e) < 1 / 2.75 ? 7.5625 * d * b * b + c : b < 2 / 2.75 ? d * (7.5625 * (b -= 1.5 / 2.75) * b + .75) + c : b < 2.5 / 2.75 ? d * (7.5625 * (b -= 2.25 / 2.75) * b + .9375) + c : d * (7.5625 * (b -= 2.625 / 2.75) * b + .984375) + c
		},
		easeInOutBounce: function(a, b, c, d, e) {
			return b < e / 2 ? .5 * jQuery.easing.easeInBounce(a, 2 * b, 0, d, e) + c : .5 * jQuery.easing.easeOutBounce(a, 2 * b - e, 0, d, e) + .5 * d + c
		}
	}),
	function(a) {
		a.fn.supersubs = function(b) {
			var c = a.extend({}, a.fn.supersubs.defaults, b);
			return this.each(function() {
				var b = a(this),
					d = a.meta ? a.extend({}, c, b.data()) : c,
					e = b.find("ul").show(),
					f = a('<li id="menu-fontsize">&#8212;</li>').css({
						padding: 0,
						position: "absolute",
						top: "-999em",
						width: "auto"
					}).appendTo(b)[0].clientWidth;
				a("#menu-fontsize").remove(), e.each(function(b) {
					var c = a(this),
						e = c.children(),
						g = e.children("a"),
						h = e.css("white-space", "nowrap").css("float");
					c.add(e).add(g).css({
						float: "none",
						width: "auto"
					});
					var i = c[0].clientWidth / f;
					i += d.extraWidth, i > d.maxWidth ? i = d.maxWidth : i < d.minWidth && (i = d.minWidth), i += "em", c.css("width", i), e.css({
						float: h,
						width: "100%",
						"white-space": "normal"
					}).each(function() {
						var b = a(this).children("ul"),
							c = void 0 !== b.css("left") ? "left" : "right";
						b.css(c, "100%")
					})
				}).hide()
			})
		}, a.fn.supersubs.defaults = {
			minWidth: 9,
			maxWidth: 25,
			extraWidth: 0
		}
	}(jQuery),
	function(a) {
		var b = 0,
			c = 0,
			d = 0,
			e = 0,
			f = "ontouchstart" in window || 0 < navigator.msMaxTouchPoints,
			g = "onorientationchange" in window,
			h = !1,
			i = !1,
			j = !1,
			k = !1,
			l = !1,
			m = "pointer",
			n = "pointer",
			o = [],
			p = [],
			q = [],
			r = [],
			s = [],
			t = [],
			u = [],
			v = [],
			w = [],
			x = [],
			y = [],
			z = {
				showScrollbar: function(b, c) {
					b.scrollbarHide && a("." + c).css({
						opacity: b.scrollbarOpacity,
						filter: "alpha(opacity:" + 100 * b.scrollbarOpacity + ")"
					})
				},
				hideScrollbar: function(a, b, c, d, e, f, g, h, i, j) {
					if (a.scrollbar && a.scrollbarHide)
						for (var k = c; k < c + 25; k++) b[b.length] = z.hideScrollbarIntervalTimer(10 * k, d[c], (c + 24 - k) / 24, e, f, g, h, i, j, a)
				},
				hideScrollbarInterval: function(b, c, d, f, g, h, i, j, k) {
					e = -1 * b / w[j] * (g - h - i - f), z.setSliderOffset("." + d, e), a("." + d).css({
						opacity: k.scrollbarOpacity * c,
						filter: "alpha(opacity:" + k.scrollbarOpacity * c * 100 + ")"
					})
				},
				slowScrollHorizontalInterval: function(b, c, d, f, g, h, i, j, k, l, m, n, o, p, q, r, y, A, B) {
					if (B.infiniteSlider) {
						if (d <= -1 * w[r]) {
							var C = a(b).width();
							if (d <= -1 * x[r]) {
								var D = -1 * m[0];
								a(c).each(function(b) {
									z.setSliderOffset(a(c)[b], D + y), b < n.length && (n[b] = -1 * D), D += q[b]
								}), d += -1 * n[0], v[r] = -1 * n[0] + y, w[r] = v[r] + C - h, u[r] = 0
							} else {
								var E = 0,
									F = z.getSliderOffset(a(c[0]), "x");
								a(c).each(function(a) {
									z.getSliderOffset(this, "x") < F && (F = z.getSliderOffset(this, "x"), E = a)
								}), o = v[r] + C, z.setSliderOffset(a(c)[E], o), v[r] = -1 * n[1] + y, w[r] = v[r] + C - h, n.splice(0, 1), n.splice(n.length, 0, -1 * o + y), u[r]++
							}
						}
						if (d >= -1 * v[r] || 0 <= d) {
							if (C = a(b).width(), 0 < d)
								for (D = -1 * m[0], a(c).each(function(b) {
										z.setSliderOffset(a(c)[b], D + y), b < n.length && (n[b] = -1 * D), D += q[b]
									}), d -= -1 * n[0], v[r] = -1 * n[0] + y, w[r] = v[r] + C - h, u[r] = p; 0 < -1 * n[0] - C + y;) {
									var G = 0,
										H = z.getSliderOffset(a(c[0]), "x");
									a(c).each(function(a) {
										z.getSliderOffset(this, "x") > H && (H = z.getSliderOffset(this, "x"), G = a)
									}), o = v[r] - q[G], z.setSliderOffset(a(c)[G], o), n.splice(0, 0, -1 * o + y), n.splice(n.length - 1, 1), v[r] = -1 * n[0] + y, w[r] = v[r] + C - h, u[r]--, s[r]++
								}
							0 > d && (G = 0, H = z.getSliderOffset(a(c[0]), "x"), a(c).each(function(a) {
								z.getSliderOffset(this, "x") > H && (H = z.getSliderOffset(this, "x"), G = a)
							}), o = v[r] - q[G], z.setSliderOffset(a(c)[G], o), n.splice(0, 0, -1 * o + y), n.splice(n.length - 1, 1), v[r] = -1 * n[0] + y, w[r] = v[r] + C - h, u[r]--)
						}
					}
					m = !1, h = z.calcActiveOffset(B, d, n, h, u[r], p, l, r), o = (h + u[r] + p) % p, B.infiniteSlider ? o != t[r] && (m = !0) : h != s[r] && (m = !0), m && (p = new z.args("change", B, b, a(b).children(":eq(" + o + ")"), o, A), a(b).parent().data("args", p), "" != B.onSlideChange) && B.onSlideChange(p), s[r] = h, t[r] = o, d = Math.floor(d), z.setSliderOffset(b, d), B.scrollbar && (e = Math.floor((-1 * d - v[r] + y) / (w[r] - v[r] + y) * (i - j - g)), b = g - k, d >= -1 * v[r] + y ? (b = g - k - -1 * e, z.setSliderOffset(a("." + f), 0)) : (d <= -1 * w[r] + 1 && (b = i - j - k - e), z.setSliderOffset(a("." + f), e)), a("." + f).css({
						width: b + "px"
					}))
				},
				slowScrollHorizontal: function(b, c, d, e, f, g, h, i, j, k, l, m, n, o, p, q, x, y, A, B, C) {
					var D = z.getSliderOffset(b, "x");
					g = [];
					var E = 0,
						F = 25 / 1024 * i;
					if (frictionCoefficient = C.frictionCoefficient, elasticFrictionCoefficient = C.elasticFrictionCoefficient, snapFrictionCoefficient = C.snapFrictionCoefficient, f > C.snapVelocityThreshold && C.snapToChildren && !A ? E = 1 : f < -1 * C.snapVelocityThreshold && C.snapToChildren && !A && (E = -1), f < -1 * F ? f = -1 * F : f > F && (f = F), a(b)[0] !== a(y)[0] && (E *= -1, f *= -2), y = u[p], C.infiniteSlider) var G = v[p],
						H = w[p];
					A = [];
					for (var F = [], I = 0; I < n.length; I++) A[I] = n[I], I < c.length && (F[I] = z.getSliderOffset(a(c[I]), "x"));
					for (; 1 < f || -1 > f;) {
						if (f *= frictionCoefficient, D += f, (D > -1 * v[p] || D < -1 * w[p]) && !C.infiniteSlider && (f *= elasticFrictionCoefficient, D += f), C.infiniteSlider) {
							if (D <= -1 * H) {
								for (var H = a(b).width(), J = 0, K = F[0], I = 0; I < F.length; I++) F[I] < K && (K = F[I], J = I);
								I = G + H, F[J] = I, G = -1 * A[1] + B, H = G + H - i, A.splice(0, 1), A.splice(A.length, 0, -1 * I + B), y++
							}
							if (D >= -1 * G) {
								for (H = a(b).width(), J = 0, K = F[0], I = 0; I < F.length; I++) F[I] > K && (K = F[I], J = I);
								I = G - o[J], F[J] = I, A.splice(0, 0, -1 * I + B), A.splice(A.length - 1, 1), G = -1 * A[0] + B, H = G + H - i, y--
							}
						}
						g[g.length] = D
					}
					if (F = !1, f = z.calcActiveOffset(C, D, A, i, y, x, s[p], p), G = (f + y + x) % x, C.snapToChildren && (C.infiniteSlider ? G != t[p] && (F = !0) : f != s[p] && (F = !0), 0 > E && !F ? (f++, f >= n.length && !C.infiniteSlider && (f = n.length - 1)) : 0 < E && !F && (f--, 0 > f && !C.infiniteSlider && (f = 0))), C.snapToChildren || (D > -1 * v[p] || D < -1 * w[p]) && !C.infiniteSlider) {
						for ((D > -1 * v[p] || D < -1 * w[p]) && !C.infiniteSlider ? g.splice(0, g.length) : (g.splice(.1 * g.length, g.length), D = 0 < g.length ? g[g.length - 1] : D); D < A[f] - .5 || D > A[f] + .5;) D = (D - A[f]) * snapFrictionCoefficient + A[f], g[g.length] = D;
						g[g.length] = A[f]
					}
					for (E = 1, 0 != g.length % 2 && (E = 0), D = 0; D < d.length; D++) clearTimeout(d[D]);
					for (y = (f + y + x) % x, G = 0, D = E; D < g.length; D += 2)(D == E || 1 < Math.abs(g[D] - G) || D >= g.length - 2) && (G = g[D], d[d.length] = z.slowScrollHorizontalIntervalTimer(10 * D, b, c, g[D], e, h, i, j, k, l, f, m, n, q, x, o, p, B, y, C));
					G = (f + u[p] + x) % x, "" != C.onSlideComplete && 1 < g.length && (d[d.length] = z.onSlideCompleteTimer(10 * (D + 1), C, b, a(b).children(":eq(" + G + ")"), y, p)), r[p] = d, z.hideScrollbar(C, d, D, g, e, h, i, k, l, p)
				},
				onSlideComplete: function(b, c, d, e, f) {
					d = new z.args("complete", b, a(c), d, e, e), a(c).parent().data("args", d), "" != b.onSlideComplete && b.onSlideComplete(d)
				},
				getSliderOffset: function(b, c) {
					var d = 0;
					if (c = "x" == c ? 4 : 5, !h || i || j) d = parseInt(a(b).css("left"), 10);
					else {
						for (var e, d = ["-webkit-transform", "-moz-transform", "transform"], f = 0; f < d.length; f++)
							if (void 0 != a(b).css(d[f]) && 0 < a(b).css(d[f]).length) {
								e = a(b).css(d[f]).split(",");
								break
							}
						d = void 0 == e[c] ? 0 : parseInt(e[c], 10)
					}
					return d
				},
				setSliderOffset: function(b, c) {
					c = parseInt(c, 10), !h || i || j ? a(b).css({
						left: c + "px"
					}) : a(b).css({
						msTransform: "matrix(1,0,0,1," + c + ",0)",
						webkitTransform: "matrix(1,0,0,1," + c + ",0)",
						MozTransform: "matrix(1,0,0,1," + c + ",0)",
						transform: "matrix(1,0,0,1," + c + ",0)"
					})
				},
				setBrowserInfo: function() {
					null != navigator.userAgent.match("WebKit") ? (m = "-webkit-grab", n = "-webkit-grabbing") : null != navigator.userAgent.match("Gecko") ? (l = !0, m = "move", n = "-moz-grabbing") : null != navigator.userAgent.match("MSIE 7") ? k = i = !0 : null != navigator.userAgent.match("MSIE 8") ? k = j = !0 : null != navigator.userAgent.match("MSIE 9") && (k = !0)
				},
				has3DTransform: function() {
					var b = !1,
						c = a("<div />").css({
							msTransform: "matrix(1,1,1,1,1,1)",
							webkitTransform: "matrix(1,1,1,1,1,1)",
							MozTransform: "matrix(1,1,1,1,1,1)",
							transform: "matrix(1,1,1,1,1,1)"
						});
					return "" == c.attr("style") ? b = !1 : l && 21 <= parseInt(navigator.userAgent.split("/")[3], 10) ? b = !1 : void 0 != c.attr("style") && (b = !0), b
				},
				getSlideNumber: function(a, b, c) {
					return (a - u[b] + c) % c
				},
				calcActiveOffset: function(a, b, c, d, e, f, g, h) {
					e = !1, a = [];
					var i;
					for (b > c[0] && (i = 0), b < c[c.length - 1] && (i = f - 1), f = 0; f < c.length; f++) c[f] <= b && c[f] > b - d && (e || c[f] == b || (a[a.length] = c[f - 1]), a[a.length] = c[f], e = !0);
					for (0 == a.length && (a[0] = c[c.length - 1]), f = e = 0; f < a.length; f++) g = Math.abs(b - a[f]), g < d && (e = a[f], d = g);
					for (f = 0; f < c.length; f++) e == c[f] && (i = f);
					return i
				},
				changeSlide: function(b, c, d, e, f, g, h, i, j, k, l, m, n, o, p, q, v, w) {
					z.autoSlidePause(o);
					for (var x = 0; x < e.length; x++) clearTimeout(e[x]);
					var y = Math.ceil(w.autoSlideTransTimer / 10) + 1,
						A = z.getSliderOffset(c, "x"),
						B = m[b],
						C = B - A;
					if (w.infiniteSlider)
						for (b = (b - u[o] + 2 * q) % q, x = !1, 0 == b && 2 == q && (b = q, m[b] = m[b - 1] - a(d).eq(0).outerWidth(!0), x = !0), B = m[b], C = B - A, B = [m[b] - a(c).width(), m[b] + a(c).width()], x && m.splice(m.length - 1, 1), x = 0; x < B.length; x++) Math.abs(B[x] - A) < Math.abs(C) && (C = B[x] - A);
					var D, B = [];
					for (z.showScrollbar(w, f), x = 0; x <= y; x++) D = x, D /= y, D--, D = A + C * (Math.pow(D, 5) + 1), B[B.length] = D;
					for (y = (b + u[o] + q) % q, x = A = 0; x < B.length; x++)(0 == x || 1 < Math.abs(B[x] - A) || x >= B.length - 2) && (A = B[x], e[x] = z.slowScrollHorizontalIntervalTimer(10 * (x + 1), c, d, B[x], f, g, h, i, j, k, b, l, m, p, q, n, o, v, y, w)), 0 == x && "" != w.onSlideStart && (C = (s[o] + u[o] + q) % q, w.onSlideStart(new z.args("start", w, c, a(c).children(":eq(" + C + ")"), C, b)));
					A = !1, w.infiniteSlider ? y != t[o] && (A = !0) : b != s[o] && (A = !0), A && "" != w.onSlideComplete && (e[e.length] = z.onSlideCompleteTimer(10 * (x + 1), w, c, a(c).children(":eq(" + y + ")"), y, o)), r[o] = e, z.hideScrollbar(w, e, x, B, f, g, h, j, k, o), z.autoSlide(c, d, e, f, g, h, i, j, k, l, m, n, o, p, q, v, w)
				},
				autoSlide: function(a, b, c, d, e, f, g, h, i, j, k, l, m, n, q, r, t) {
					return !!p[m].autoSlide && (z.autoSlidePause(m), void(o[m] = setTimeout(function() {
						!t.infiniteSlider && s[m] > k.length - 1 && (s[m] -= q), z.changeSlide((s[m] + u[m] + k.length + 1) % k.length, a, b, c, d, e, f, g, h, i, j, k, l, m, n, q, r, t), z.autoSlide(a, b, c, d, e, f, g, h, i, j, k, l, m, n, q, r, t)
					}, t.autoSlideTimer + t.autoSlideTransTimer)))
				},
				autoSlidePause: function(a) {
					clearTimeout(o[a])
				},
				isUnselectable: function(b, c) {
					return "" != c.unselectableSelector && 1 == a(b).closest(c.unselectableSelector).length
				},
				slowScrollHorizontalIntervalTimer: function(a, b, c, d, e, f, g, h, i, j, k, l, m, n, o, p, q, r, s, t) {
					return setTimeout(function() {
						z.slowScrollHorizontalInterval(b, c, d, e, f, g, h, i, j, k, l, m, n, o, p, q, r, s, t)
					}, a)
				},
				onSlideCompleteTimer: function(a, b, c, d, e, f) {
					return setTimeout(function() {
						z.onSlideComplete(b, c, d, e, f)
					}, a)
				},
				hideScrollbarIntervalTimer: function(a, b, c, d, e, f, g, h, i, j) {
					return setTimeout(function() {
						z.hideScrollbarInterval(b, c, d, e, f, g, h, i, j)
					}, a)
				},
				args: function(b, c, d, e, f, g) {
					this.prevSlideNumber = void 0 == a(d).parent().data("args") ? void 0 : a(d).parent().data("args").prevSlideNumber, this.prevSlideObject = void 0 == a(d).parent().data("args") ? void 0 : a(d).parent().data("args").prevSlideObject, this.targetSlideNumber = g + 1, this.targetSlideObject = a(d).children(":eq(" + g + ")"), this.slideChanged = !1, "load" == b ? this.targetSlideObject = this.targetSlideNumber = void 0 : "start" == b ? this.targetSlideObject = this.targetSlideNumber = void 0 : "change" == b ? (this.slideChanged = !0, this.prevSlideNumber = void 0 == a(d).parent().data("args") ? c.startAtSlide : a(d).parent().data("args").currentSlideNumber, this.prevSlideObject = a(d).children(":eq(" + this.prevSlideNumber + ")")) : "complete" == b && (this.slideChanged = a(d).parent().data("args").slideChanged), this.settings = c, this.data = a(d).parent().data("iosslider"), this.sliderObject = d, this.sliderContainerObject = a(d).parent(), this.currentSlideObject = e, this.currentSlideNumber = f + 1, this.currentSliderOffset = -1 * z.getSliderOffset(d, "x")
				},
				preventDrag: function(a) {
					a.preventDefault()
				},
				preventClick: function(a) {
					return a.stopImmediatePropagation(), !1
				},
				enableClick: function() {
					return !0
				}
			};
		z.setBrowserInfo();
		var A = {
			init: function(l, o) {
				h = z.has3DTransform();
				var A = a.extend(!0, {
					elasticPullResistance: .6,
					frictionCoefficient: .92,
					elasticFrictionCoefficient: .6,
					snapFrictionCoefficient: .92,
					snapToChildren: !1,
					snapSlideCenter: !1,
					startAtSlide: 1,
					scrollbar: !1,
					scrollbarDrag: !1,
					scrollbarHide: !0,
					scrollbarLocation: "top",
					scrollbarContainer: "",
					scrollbarOpacity: .4,
					scrollbarHeight: "4px",
					scrollbarBorder: "0",
					scrollbarMargin: "5px",
					scrollbarBackground: "#000",
					scrollbarBorderRadius: "100px",
					scrollbarShadow: "0 0 0 #000",
					scrollbarElasticPullResistance: .9,
					desktopClickDrag: !1,
					keyboardControls: !1,
					tabToAdvance: !1,
					responsiveSlideContainer: !0,
					responsiveSlides: !0,
					navSlideSelector: "",
					navPrevSelector: "",
					navNextSelector: "",
					autoSlideToggleSelector: "",
					autoSlide: !1,
					autoSlideTimer: 5e3,
					autoSlideTransTimer: 750,
					autoSlideHoverPause: !0,
					infiniteSlider: !1,
					snapVelocityThreshold: 5,
					slideStartVelocityThreshold: 0,
					horizontalSlideLockThreshold: 5,
					verticalSlideLockThreshold: 3,
					stageCSS: {
						position: "relative",
						top: "0",
						left: "0",
						overflow: "hidden",
						zIndex: 1
					},
					unselectableSelector: "",
					onSliderLoaded: "",
					onSliderUpdate: "",
					onSliderResize: "",
					onSlideStart: "",
					onSlideChange: "",
					onSlideComplete: ""
				}, l);
				return void 0 == o && (o = this), a(o).each(function(h) {
					function l() {
						z.autoSlidePause(o), oa = a(Z).find("a"), pa = a(Z).find("[onclick]"), qa = a(Z).find("*"), a(N).css("width", ""), a(N).css("height", ""), a(Z).css("width", ""), $ = a(Z).children().not("script").get(), _ = [], aa = [], A.responsiveSlides && a($).css("width", ""), w[o] = 0, U = [], K = a(N).parent().width(), O = a(N).outerWidth(!0), A.responsiveSlideContainer && (O = a(N).outerWidth(!0) > K ? K : a(N).width()), a(N).css({
							position: A.stageCSS.position,
							top: A.stageCSS.top,
							left: A.stageCSS.left,
							overflow: A.stageCSS.overflow,
							zIndex: A.stageCSS.zIndex,
							webkitPerspective: 1e3,
							webkitBackfaceVisibility: "hidden",
							msTouchAction: "pan-y",
							width: O
						}), a(A.unselectableSelector).css({
							cursor: "default"
						});
						for (var b = 0; b < $.length; b++) {
							_[b] = a($[b]).width(), aa[b] = a($[b]).outerWidth(!0);
							var c = aa[b];
							A.responsiveSlides && (aa[b] > O ? (c = O + -1 * (aa[b] - _[b]), _[b] = c, aa[b] = O) : c = _[b], a($[b]).css({
								width: c
							})), a($[b]).css({
								webkitBackfaceVisibility: "hidden",
								overflow: "hidden",
								position: "absolute"
							}), U[b] = -1 * w[o], w[o] = w[o] + c + (aa[b] - _[b])
						}
						for (A.snapSlideCenter && (M = .5 * (O - aa[0]), A.responsiveSlides && aa[0] > O && (M = 0)), x[o] = 2 * w[o], b = 0; b < $.length; b++) z.setSliderOffset(a($[b]), -1 * U[b] + w[o] + M), U[b] -= w[o];
						if (!A.infiniteSlider && !A.snapSlideCenter) {
							for (b = 0; b < U.length && !(U[b] <= -1 * (2 * w[o] - O)); b++) da = b;
							U.splice(da + 1, U.length), U[U.length] = -1 * (2 * w[o] - O)
						}
						for (b = 0; b < U.length; b++) V[b] = U[b];
						if (T && (p[o].startAtSlide = p[o].startAtSlide > U.length ? U.length : p[o].startAtSlide, A.infiniteSlider ? (p[o].startAtSlide = (p[o].startAtSlide - 1 + ba) % ba, s[o] = p[o].startAtSlide) : (p[o].startAtSlide = 0 > p[o].startAtSlide - 1 ? U.length - 1 : p[o].startAtSlide, s[o] = p[o].startAtSlide - 1), t[o] = s[o]), v[o] = w[o] + M, a(Z).css({
								position: "relative",
								cursor: m,
								webkitPerspective: "0",
								webkitBackfaceVisibility: "hidden",
								width: w[o] + "px"
							}), na = w[o], w[o] = 2 * w[o] - O + 2 * M, (ha = na + M < O || 0 == O) && a(Z).css({
								cursor: "default"
							}), L = a(N).parent().outerHeight(!0), P = a(N).height(), A.responsiveSlideContainer && (P = P > L ? L : P), a(N).css({
								height: P
							}), z.setSliderOffset(Z, U[s[o]]), A.infiniteSlider && !ha) {
							for (b = z.getSliderOffset(a(Z), "x"), c = (u[o] + ba) % ba * -1; 0 > c;) {
								var d = 0,
									e = z.getSliderOffset(a($[0]), "x");
								a($).each(function(a) {
									z.getSliderOffset(this, "x") < e && (e = z.getSliderOffset(this, "x"), d = a)
								});
								var f = v[o] + na;
								z.setSliderOffset(a($)[d], f), v[o] = -1 * U[1] + M, w[o] = v[o] + na - O, U.splice(0, 1), U.splice(U.length, 0, -1 * f + M), c++
							}
							for (; 0 < -1 * U[0] - na + M && A.snapSlideCenter && T;) {
								var g = 0,
									h = z.getSliderOffset(a($[0]), "x");
								a($).each(function(a) {
									z.getSliderOffset(this, "x") > h && (h = z.getSliderOffset(this, "x"), g = a)
								}), f = v[o] - aa[g], z.setSliderOffset(a($)[g], f), U.splice(0, 0, -1 * f + M), U.splice(U.length - 1, 1), v[o] = -1 * U[0] + M, w[o] = v[o] + na - O, u[o]--, s[o]++
							}
							for (; b <= -1 * w[o];) d = 0, e = z.getSliderOffset(a($[0]), "x"), a($).each(function(a) {
								z.getSliderOffset(this, "x") < e && (e = z.getSliderOffset(this, "x"), d = a)
							}), f = v[o] + na, z.setSliderOffset(a($)[d], f), v[o] = -1 * U[1] + M, w[o] = v[o] + na - O, U.splice(0, 1), U.splice(U.length, 0, -1 * f + M), u[o]++, s[o]--
						}
						return z.setSliderOffset(Z, U[s[o]]), A.desktopClickDrag || a(Z).css({
							cursor: "default"
						}), A.scrollbar && (a("." + E).css({
							margin: A.scrollbarMargin,
							overflow: "hidden",
							display: "none"
						}), a("." + E + " ." + F).css({
							border: A.scrollbarBorder
						}), Q = parseInt(a("." + E).css("marginLeft")) + parseInt(a("." + E).css("marginRight")), R = parseInt(a("." + E + " ." + F).css("borderLeftWidth"), 10) + parseInt(a("." + E + " ." + F).css("borderRightWidth"), 10), I = "" != A.scrollbarContainer ? a(A.scrollbarContainer).width() : O, J = O / na * (I - Q), A.scrollbarHide || (W = A.scrollbarOpacity), a("." + E).css({
							position: "absolute",
							left: 0,
							width: I - Q + "px",
							margin: A.scrollbarMargin
						}), "top" == A.scrollbarLocation ? a("." + E).css("top", "0") : a("." + E).css("bottom", "0"), a("." + E + " ." + F).css({
							borderRadius: A.scrollbarBorderRadius,
							background: A.scrollbarBackground,
							height: A.scrollbarHeight,
							width: J - R + "px",
							minWidth: A.scrollbarHeight,
							border: A.scrollbarBorder,
							webkitPerspective: 1e3,
							webkitBackfaceVisibility: "hidden",
							position: "relative",
							opacity: W,
							filter: "alpha(opacity:" + 100 * W + ")",
							boxShadow: A.scrollbarShadow
						}), z.setSliderOffset(a("." + E + " ." + F), Math.floor((-1 * U[s[o]] - v[o] + M) / (w[o] - v[o] + M) * (I - Q - J))), a("." + E).css({
							display: "block"
						}), G = a("." + E + " ." + F), H = a("." + E)), A.scrollbarDrag && !ha && a("." + E + " ." + F).css({
							cursor: m
						}), A.infiniteSlider && (ga = (w[o] + O) / 3), "" != A.navSlideSelector && a(A.navSlideSelector).each(function(b) {
							a(this).css({
								cursor: "pointer"
							}), a(this).unbind(ma).bind(ma, function(c) {
								"touchstart" == c.type ? a(this).unbind("click.iosSliderEvent") : a(this).unbind("touchstart.iosSliderEvent"), ma = c.type + ".iosSliderEvent", z.changeSlide(b, Z, $, B, F, J, O, I, Q, R, V, U, aa, o, ga, ba, M, A)
							})
						}), "" != A.navPrevSelector && (a(A.navPrevSelector).css({
							cursor: "pointer"
						}), a(A.navPrevSelector).unbind(ma).bind(ma, function(b) {
							"touchstart" == b.type ? a(this).unbind("click.iosSliderEvent") : a(this).unbind("touchstart.iosSliderEvent"), ma = b.type + ".iosSliderEvent", b = (s[o] + u[o] + ba) % ba, (0 < b || A.infiniteSlider) && z.changeSlide(b - 1, Z, $, B, F, J, O, I, Q, R, V, U, aa, o, ga, ba, M, A)
						})), "" != A.navNextSelector && (a(A.navNextSelector).css({
							cursor: "pointer"
						}), a(A.navNextSelector).unbind(ma).bind(ma, function(b) {
							"touchstart" == b.type ? a(this).unbind("click.iosSliderEvent") : a(this).unbind("touchstart.iosSliderEvent"), ma = b.type + ".iosSliderEvent", b = (s[o] + u[o] + ba) % ba, (b < U.length - 1 || A.infiniteSlider) && z.changeSlide(b + 1, Z, $, B, F, J, O, I, Q, R, V, U, aa, o, ga, ba, M, A)
						})), A.autoSlide && !ha && "" != A.autoSlideToggleSelector && (a(A.autoSlideToggleSelector).css({
							cursor: "pointer"
						}), a(A.autoSlideToggleSelector).unbind(ma).bind(ma, function(b) {
							"touchstart" == b.type ? a(this).unbind("click.iosSliderEvent") : a(this).unbind("touchstart.iosSliderEvent"), ma = b.type + ".iosSliderEvent", ia ? (z.autoSlide(Z, $, B, F, J, O, I, Q, R, V, U, aa, o, ga, ba, M, A), ia = !1, a(A.autoSlideToggleSelector).removeClass("on")) : (z.autoSlidePause(o), ia = !0, a(A.autoSlideToggleSelector).addClass("on"))
						})), z.autoSlide(Z, $, B, F, J, O, I, Q, R, V, U, aa, o, ga, ba, M, A), a(N).bind("mouseleave.iosSliderEvent", function() {
							return !!ia || void z.autoSlide(Z, $, B, F, J, O, I, Q, R, V, U, aa, o, ga, ba, M, A)
						}), a(N).bind("touchend.iosSliderEvent", function() {
							return !!ia || void z.autoSlide(Z, $, B, F, J, O, I, Q, R, V, U, aa, o, ga, ba, M, A)
						}), A.autoSlideHoverPause && a(N).bind("mouseenter.iosSliderEvent", function() {
							z.autoSlidePause(o)
						}), a(N).data("iosslider", {
							obj: ra,
							settings: A,
							scrollerNode: Z,
							slideNodes: $,
							numberOfSlides: ba,
							centeredSlideOffset: M,
							sliderNumber: o,
							originalOffsets: V,
							childrenOffsets: U,
							sliderMax: w[o],
							scrollbarClass: F,
							scrollbarWidth: J,
							scrollbarStageWidth: I,
							stageWidth: O,
							scrollMargin: Q,
							scrollBorder: R,
							infiniteSliderOffset: u[o],
							infiniteSliderWidth: ga,
							slideNodeOuterWidths: aa,
							shortContent: ha
						}), T = !1, !0
					}
					b++;
					var o = b,
						B = [];
					p[o] = a.extend({}, A), v[o] = 0, w[o] = 0;
					var G, H, I, J, K, L, O, P, Q, R, S, C = [0, 0],
						D = [0, 0],
						E = "scrollbarBlock" + b,
						F = "scrollbar" + b,
						M = 0,
						N = a(this),
						T = !0;
					h = -1;
					var U, $, _, aa, ga, V = [],
						W = 0,
						X = 0,
						Y = 0,
						Z = a(this).children(":first-child"),
						ba = a(Z).children().not("script").length,
						ca = !1,
						da = 0,
						ea = !1,
						fa = void 0;
					u[o] = 0;
					var ha = !1,
						ia = !1;
					q[o] = !1;
					var ja, na, oa, pa, qa, ka = !1,
						la = !1,
						ma = "touchstart.iosSliderEvent click.iosSliderEvent";
					y[o] = !1, r[o] = [], A.scrollbarDrag && (A.scrollbar = !0, A.scrollbarHide = !1);
					var ra = a(this);
					if (void 0 != ra.data("iosslider")) return !0;
					var sa = "".split(""),
						ta = Math.floor(12317 * Math.random());
					for (a(Z).parent().append("<i class = 'i" + ta + "'></i>").append("<i class = 'i" + ta + "'></i>"), a(".i" + ta).css({
							position: "absolute",
							right: "10px",
							bottom: "10px",
							zIndex: 1e3,
							fontStyle: "normal",
							background: "#fff",
							opacity: .2
						}).eq(1).css({
							bottom: "auto",
							right: "auto",
							top: "10px",
							left: "10px"
						}), h = 0; h < sa.length; h++) a(".i" + ta).html(a(".i" + ta).html() + sa[h]);
					if (14.2 <= parseInt(a().jquery.split(".").join(""), 10) ? a(this).delegate("img", "dragstart.iosSliderEvent", function(a) {
							a.preventDefault()
						}) : a(this).find("img").bind("dragstart.iosSliderEvent", function(a) {
							a.preventDefault()
						}), A.infiniteSlider && (A.scrollbar = !1), A.infiniteSlider && 1 == ba && (A.infiniteSlider = !1), A.scrollbar && ("" != A.scrollbarContainer ? a(A.scrollbarContainer).append("<div class = '" + E + "'><div class = '" + F + "'></div></div>") : a(Z).parent().append("<div class = '" + E + "'><div class = '" + F + "'></div></div>")), !l()) return !0;
					if (a(this).find("a").bind("mousedown", z.preventDrag), a(this).find("[onclick]").bind("click", z.preventDrag).each(function() {
							a(this).data("onclick", this.onclick)
						}), h = z.calcActiveOffset(A, z.getSliderOffset(a(Z), "x"), U, O, u[o], ba, void 0, o), h = (h + u[o] + ba) % ba, h = new z.args("load", A, Z, a(Z).children(":eq(" + h + ")"), h, h), a(N).data("args", h), "" != A.onSliderLoaded && A.onSliderLoaded(h), (p[o].responsiveSlides || p[o].responsiveSlideContainer) && (h = g ? "orientationchange" : "resize", a(window).bind(h + ".iosSliderEvent-" + o, function() {
							if (!l()) return !0;
							var b = a(N).data("args");
							"" != A.onSliderResize && A.onSliderResize(b)
						})), !A.keyboardControls && !A.tabToAdvance || ha || a(document).bind("keydown.iosSliderEvent", function(a) {
							return i || j || (a = a.originalEvent), !!y[o] || void(37 == a.keyCode && A.keyboardControls ? (a.preventDefault(), a = (s[o] + u[o] + ba) % ba, (0 < a || A.infiniteSlider) && z.changeSlide(a - 1, Z, $, B, F, J, O, I, Q, R, V, U, aa, o, ga, ba, M, A)) : (39 == a.keyCode && A.keyboardControls || 9 == a.keyCode && A.tabToAdvance) && (a.preventDefault(), a = (s[o] + u[o] + ba) % ba, (a < U.length - 1 || A.infiniteSlider) && z.changeSlide(a + 1, Z, $, B, F, J, O, I, Q, R, V, U, aa, o, ga, ba, M, A)))
						}), f || A.desktopClickDrag) {
						var ua = !1,
							va = !1;
						h = a(Z);
						var wa = a(Z),
							xa = !1;
						A.scrollbarDrag && (h = h.add(G), wa = wa.add(H)), a(h).bind("mousedown.iosSliderEvent touchstart.iosSliderEvent", function(b) {
							if (ua) return !0;
							if (ua = !0, va = !1, "touchstart" == b.type ? a(wa).unbind("mousedown.iosSliderEvent") : a(wa).unbind("touchstart.iosSliderEvent"), y[o] || ha || (xa = z.isUnselectable(b.target, A))) return ca = ua = !1, !0;
							if (ja = a(this)[0] === a(G)[0] ? G : Z, i || j || (b = b.originalEvent), z.autoSlidePause(o), qa.unbind(".disableClick"), "touchstart" == b.type) eventX = b.touches[0].pageX, eventY = b.touches[0].pageY;
							else {
								if (window.getSelection) window.getSelection().empty ? window.getSelection().empty() : window.getSelection().removeAllRanges && window.getSelection().removeAllRanges();
								else if (document.selection)
									if (j) try {
										document.selection.empty()
									} catch (a) {} else document.selection.empty();
								eventX = b.pageX, eventY = b.pageY, ea = !0, fa = Z, a(this).css({
									cursor: n
								})
							}
							for (C = [0, 0], D = [0, 0], c = 0, ca = !1, b = 0; b < B.length; b++) clearTimeout(B[b]);
							b = z.getSliderOffset(Z, "x"), b > -1 * v[o] + M + na ? (b = -1 * v[o] + M + na, z.setSliderOffset(a("." + F), b), a("." + F).css({
								width: J - R + "px"
							})) : b < -1 * w[o] && (z.setSliderOffset(a("." + F), I - Q - J), a("." + F).css({
								width: J - R + "px"
							})), b = a(this)[0] === a(G)[0] ? v[o] : 0, X = -1 * (z.getSliderOffset(this, "x") - eventX - b), z.getSliderOffset(this, "y"), C[1] = eventX, D[1] = eventY, la = !1
						}), a(document).bind("touchmove.iosSliderEvent mousemove.iosSliderEvent", function(b) {
							if (i || j || (b = b.originalEvent), y[o] || ha || xa || !ua) return !0;
							var f = 0;
							if ("touchmove" == b.type) eventX = b.touches[0].pageX, eventY = b.touches[0].pageY;
							else {
								if (window.getSelection) window.getSelection().empty || window.getSelection().removeAllRanges && window.getSelection().removeAllRanges();
								else if (document.selection)
									if (j) try {
										document.selection.empty()
									} catch (a) {} else document.selection.empty();
								if (eventX = b.pageX, eventY = b.pageY, !ea || !k && ("undefined" != typeof b.webkitMovementX || "undefined" != typeof b.webkitMovementY) && 0 === b.webkitMovementY && 0 === b.webkitMovementX) return !0
							}
							if (C[0] = C[1], C[1] = eventX, c = (C[1] - C[0]) / 2, D[0] = D[1], D[1] = eventY, d = (D[1] - D[0]) / 2, !ca) {
								var g = (s[o] + u[o] + ba) % ba,
									g = new z.args("start", A, Z, a(Z).children(":eq(" + g + ")"), g, void 0);
								a(N).data("args", g), "" != A.onSlideStart && A.onSlideStart(g)
							}
							if ((d > A.verticalSlideLockThreshold || d < -1 * A.verticalSlideLockThreshold) && "touchmove" == b.type && !ca && (ka = !0), (c > A.horizontalSlideLockThreshold || c < -1 * A.horizontalSlideLockThreshold) && "touchmove" == b.type && b.preventDefault(), (c > A.slideStartVelocityThreshold || c < -1 * A.slideStartVelocityThreshold) && (ca = !0), ca && !ka) {
								var g = z.getSliderOffset(Z, "x"),
									h = a(ja)[0] === a(G)[0] ? v[o] : M,
									l = a(ja)[0] === a(G)[0] ? (v[o] - w[o] - M) / (I - Q - J) : 1,
									m = a(ja)[0] === a(G)[0] ? A.scrollbarElasticPullResistance : A.elasticPullResistance,
									n = A.snapSlideCenter && a(ja)[0] === a(G)[0] ? 0 : M,
									p = A.snapSlideCenter && a(ja)[0] === a(G)[0] ? M : 0;
								if ("touchmove" == b.type && (Y != b.touches.length && (X = -1 * g + eventX), Y = b.touches.length), A.infiniteSlider) {
									if (g <= -1 * w[o]) {
										var q = a(Z).width();
										if (g <= -1 * x[o]) {
											var r = -1 * V[0];
											a($).each(function(b) {
												z.setSliderOffset(a($)[b], r + M), b < U.length && (U[b] = -1 * r), r += aa[b]
											}), X -= -1 * U[0], v[o] = -1 * U[0] + M, w[o] = v[o] + q - O, u[o] = 0
										} else {
											var B = 0,
												E = z.getSliderOffset(a($[0]), "x");
											a($).each(function(a) {
												z.getSliderOffset(this, "x") < E && (E = z.getSliderOffset(this, "x"), B = a)
											}), m = v[o] + q, z.setSliderOffset(a($)[B], m), v[o] = -1 * U[1] + M, w[o] = v[o] + q - O, U.splice(0, 1), U.splice(U.length, 0, -1 * m + M), u[o]++
										}
									}
									if (g >= -1 * v[o] || 0 <= g)
										if (q = a(Z).width(), 0 <= g)
											for (r = -1 * V[0], a($).each(function(b) {
													z.setSliderOffset(a($)[b], r + M), b < U.length && (U[b] = -1 * r), r += aa[b]
												}), X += -1 * U[0], v[o] = -1 * U[0] + M, w[o] = v[o] + q - O, u[o] = ba; 0 < -1 * U[0] - q + M;) {
												var H = 0,
													K = z.getSliderOffset(a($[0]), "x");
												a($).each(function(a) {
													z.getSliderOffset(this, "x") > K && (K = z.getSliderOffset(this, "x"), H = a)
												}), m = v[o] - aa[H], z.setSliderOffset(a($)[H], m), U.splice(0, 0, -1 * m + M), U.splice(U.length - 1, 1), v[o] = -1 * U[0] + M, w[o] = v[o] + q - O, u[o]--, s[o]++
											} else H = 0, K = z.getSliderOffset(a($[0]), "x"), a($).each(function(a) {
												z.getSliderOffset(this, "x") > K && (K = z.getSliderOffset(this, "x"), H = a)
											}), m = v[o] - aa[H], z.setSliderOffset(a($)[H], m), U.splice(0, 0, -1 * m + M), U.splice(U.length - 1, 1), v[o] = -1 * U[0] + M, w[o] = v[o] + q - O, u[o]--
								} else q = a(Z).width(), g > -1 * v[o] + M && (f = (v[o] + -1 * (X - h - eventX + n) * l - h) * m * -1 / l), g < -1 * w[o] && (f = (w[o] + p + -1 * (X - h - eventX) * l - h) * m * -1 / l);
								z.setSliderOffset(Z, -1 * (X - h - eventX - f) * l - h + p), A.scrollbar && (z.showScrollbar(A, F), e = Math.floor((X - eventX - f - v[o] + n) / (w[o] - v[o] + M) * (I - Q - J) * l), g = J, 0 >= e ? (g = J - R - -1 * e, z.setSliderOffset(a("." + F), 0), a("." + F).css({
									width: g + "px"
								})) : e >= I - Q - R - J ? (g = I - Q - R - e, z.setSliderOffset(a("." + F), e), a("." + F).css({
									width: g + "px"
								})) : z.setSliderOffset(a("." + F), e)), "touchmove" == b.type && (S = b.touches[0].pageX), b = !1, f = z.calcActiveOffset(A, -1 * (X - eventX - f), U, O, u[o], ba, void 0, o), g = (f + u[o] + ba) % ba, A.infiniteSlider ? g != t[o] && (b = !0) : f != s[o] && (b = !0), b && (s[o] = f, t[o] = g, la = !0, g = new z.args("change", A, Z, a(Z).children(":eq(" + g + ")"), g, g), a(N).data("args", g), "" != A.onSlideChange) && A.onSlideChange(g)
							}
						}), sa = a(window), (j || i) && (sa = a(document)), a(h).bind("touchcancel.iosSliderEvent touchend.iosSliderEvent", function(a) {
							if (a = a.originalEvent, va) return !1;
							if (va = !0, y[o] || ha || xa) return !0;
							if (0 != a.touches.length)
								for (var b = 0; b < a.touches.length; b++) a.touches[b].pageX == S && z.slowScrollHorizontal(Z, $, B, F, c, d, J, O, I, Q, R, V, U, aa, o, ga, ba, ja, la, M, A);
							else z.slowScrollHorizontal(Z, $, B, F, c, d, J, O, I, Q, R, V, U, aa, o, ga, ba, ja, la, M, A);
							return ua = ka = !1, !0
						}), a(sa).bind("mouseup.iosSliderEvent-" + o, function(b) {
							if (ca ? oa.unbind("click.disableClick").bind("click.disableClick", z.preventClick) : oa.unbind("click.disableClick").bind("click.disableClick", z.enableClick), pa.each(function() {
									this.onclick = function(b) {
										return !ca && void(a(this).data("onclick") && a(this).data("onclick").call(this, b || window.event))
									}, this.onclick = a(this).data("onclick")
								}), 1.8 <= parseFloat(a().jquery) ? qa.each(function() {
									var b = a._data(this, "events");
									if (void 0 != b && void 0 != b.click && "iosSliderEvent" != b.click[0].namespace) {
										if (!ca) return !1;
										a(this).one("click.disableClick", z.preventClick);
										var b = a._data(this, "events").click,
											c = b.pop();
										b.splice(0, 0, c)
									}
								}) : 1.6 <= parseFloat(a().jquery) && qa.each(function() {
									var b = a(this).data("events");
									if (void 0 != b && void 0 != b.click && "iosSliderEvent" != b.click[0].namespace) {
										if (!ca) return !1;
										a(this).one("click.disableClick", z.preventClick);
										var b = a(this).data("events").click,
											c = b.pop();
										b.splice(0, 0, c)
									}
								}), !q[o]) {
								if (ha) return !0;
								if (A.desktopClickDrag && a(Z).css({
										cursor: m
									}), A.scrollbarDrag && a(G).css({
										cursor: m
									}), ea = !1, void 0 == fa) return !0;
								z.slowScrollHorizontal(fa, $, B, F, c, d, J, O, I, Q, R, V, U, aa, o, ga, ba, ja, la, M, A), fa = void 0
							}
							ua = ka = !1
						})
					}
				})
			},
			destroy: function(b, c) {
				return void 0 == c && (c = this), a(c).each(function() {
					var c = a(this),
						d = c.data("iosslider");
					if (void 0 == d) return !1;
					void 0 == b && (b = !0), z.autoSlidePause(d.sliderNumber), q[d.sliderNumber] = !0, a(window).unbind(".iosSliderEvent-" + d.sliderNumber), a(document).unbind(".iosSliderEvent-" + d.sliderNumber), a(document).unbind("keydown.iosSliderEvent"), a(this).unbind(".iosSliderEvent"), a(this).children(":first-child").unbind(".iosSliderEvent"), a(this).children(":first-child").children().unbind(".iosSliderEvent"), b && (a(this).attr("style", ""), a(this).children(":first-child").attr("style", ""), a(this).children(":first-child").children().attr("style", ""), a(d.settings.navSlideSelector).attr("style", ""), a(d.settings.navPrevSelector).attr("style", ""), a(d.settings.navNextSelector).attr("style", ""), a(d.settings.autoSlideToggleSelector).attr("style", ""), a(d.settings.unselectableSelector).attr("style", "")), d.settings.scrollbar && a(".scrollbarBlock" + d.sliderNumber).remove();
					for (var d = r[d.sliderNumber], e = 0; e < d.length; e++) clearTimeout(d[e]);
					c.removeData("iosslider"), c.removeData("args")
				})
			},
			update: function(b) {
				return void 0 == b && (b = this), a(b).each(function() {
					var b = a(this),
						c = b.data("iosslider");
					return void 0 != c && (c.settings.startAtSlide = b.data("args").currentSlideNumber, A.destroy(!1, this), 1 != c.numberOfSlides && c.settings.infiniteSlider && (c.settings.startAtSlide = (s[c.sliderNumber] + 1 + u[c.sliderNumber] + c.numberOfSlides) % c.numberOfSlides), A.init(c.settings, this), b = new z.args("update", c.settings, c.scrollerNode, a(c.scrollerNode).children(":eq(" + (c.settings.startAtSlide - 1) + ")"), c.settings.startAtSlide - 1, c.settings.startAtSlide - 1), a(c.stageNode).data("args", b), void("" != c.settings.onSliderUpdate && c.settings.onSliderUpdate(b)))
				})
			},
			addSlide: function(b, c) {
				return this.each(function() {
					var d = a(this),
						e = d.data("iosslider");
					return void 0 != e && (0 == a(e.scrollerNode).children().length ? (a(e.scrollerNode).append(b), d.data("args").currentSlideNumber = 1) : e.settings.infiniteSlider ? (1 == c ? a(e.scrollerNode).children(":eq(0)").before(b) : a(e.scrollerNode).children(":eq(" + (c - 2) + ")").after(b), -1 > u[e.sliderNumber] && s[e.sliderNumber]--, d.data("args").currentSlideNumber >= c && s[e.sliderNumber]++) : (c <= e.numberOfSlides ? a(e.scrollerNode).children(":eq(" + (c - 1) + ")").before(b) : a(e.scrollerNode).children(":eq(" + (c - 2) + ")").after(b), d.data("args").currentSlideNumber >= c && d.data("args").currentSlideNumber++), d.data("iosslider").numberOfSlides++, void A.update(this))
				})
			},
			removeSlide: function(b) {
				return this.each(function() {
					var c = a(this),
						d = c.data("iosslider");
					return void 0 != d && (a(d.scrollerNode).children(":eq(" + (b - 1) + ")").remove(), s[d.sliderNumber] > b - 1 && s[d.sliderNumber]--, c.data("iosslider").numberOfSlides--, void A.update(this))
				})
			},
			goToSlide: function(b, c) {
				return void 0 == c && (c = this), a(c).each(function() {
					var c = a(this).data("iosslider");
					return void 0 != c && !c.shortContent && (b = b > c.childrenOffsets.length ? c.childrenOffsets.length - 1 : b - 1, void z.changeSlide(b, a(c.scrollerNode), a(c.slideNodes), r[c.sliderNumber], c.scrollbarClass, c.scrollbarWidth, c.stageWidth, c.scrollbarStageWidth, c.scrollMargin, c.scrollBorder, c.originalOffsets, c.childrenOffsets, c.slideNodeOuterWidths, c.sliderNumber, c.infiniteSliderWidth, c.numberOfSlides, c.centeredSlideOffset, c.settings))
				})
			},
			prevSlide: function() {
				return this.each(function() {
					var b = a(this).data("iosslider");
					if (void 0 == b || b.shortContent) return !1;
					var c = (s[b.sliderNumber] + u[b.sliderNumber] + b.numberOfSlides) % b.numberOfSlides;
					(0 < c || b.settings.infiniteSlider) && z.changeSlide(c - 1, a(b.scrollerNode), a(b.slideNodes), r[b.sliderNumber], b.scrollbarClass, b.scrollbarWidth, b.stageWidth, b.scrollbarStageWidth, b.scrollMargin, b.scrollBorder, b.originalOffsets, b.childrenOffsets, b.slideNodeOuterWidths, b.sliderNumber, b.infiniteSliderWidth, b.numberOfSlides, b.centeredSlideOffset, b.settings), s[b.sliderNumber] = c
				})
			},
			nextSlide: function() {
				return this.each(function() {
					var b = a(this).data("iosslider");
					if (void 0 == b || b.shortContent) return !1;
					var c = (s[b.sliderNumber] + u[b.sliderNumber] + b.numberOfSlides) % b.numberOfSlides;
					(c < b.childrenOffsets.length - 1 || b.settings.infiniteSlider) && z.changeSlide(c + 1, a(b.scrollerNode), a(b.slideNodes), r[b.sliderNumber], b.scrollbarClass, b.scrollbarWidth, b.stageWidth, b.scrollbarStageWidth, b.scrollMargin, b.scrollBorder, b.originalOffsets, b.childrenOffsets, b.slideNodeOuterWidths, b.sliderNumber, b.infiniteSliderWidth, b.numberOfSlides, b.centeredSlideOffset, b.settings), s[b.sliderNumber] = c
				})
			},
			lock: function() {
				return this.each(function() {
					var b = a(this).data("iosslider");
					return void 0 != b && !b.shortContent && (a(b.scrollerNode).css({
						cursor: "default"
					}), void(y[b.sliderNumber] = !0))
				})
			},
			unlock: function() {
				return this.each(function() {
					var b = a(this).data("iosslider");
					return void 0 != b && !b.shortContent && (a(b.scrollerNode).css({
						cursor: m
					}), void(y[b.sliderNumber] = !1))
				})
			},
			getData: function() {
				return this.each(function() {
					var b = a(this).data("iosslider");
					return void 0 != b && !b.shortContent && b
				})
			},
			autoSlidePause: function() {
				return this.each(function() {
					var b = a(this).data("iosslider");
					return void 0 != b && !b.shortContent && (p[b.sliderNumber].autoSlide = !1, z.autoSlidePause(b.sliderNumber), b)
				})
			},
			autoSlidePlay: function() {
				return this.each(function() {
					var b = a(this).data("iosslider");
					return void 0 != b && !b.shortContent && (p[b.sliderNumber].autoSlide = !0, z.autoSlide(a(b.scrollerNode), a(b.slideNodes), r[b.sliderNumber], b.scrollbarClass, b.scrollbarWidth, b.stageWidth, b.scrollbarStageWidth, b.scrollMargin, b.scrollBorder, b.originalOffsets, b.childrenOffsets, b.slideNodeOuterWidths, b.sliderNumber, b.infiniteSliderWidth, b.numberOfSlides, b.centeredSlideOffset, b.settings), b)
				})
			}
		};
		a.fn.iosSlider = function(b) {
			return A[b] ? A[b].apply(this, Array.prototype.slice.call(arguments, 1)) : "object" != typeof b && b ? void a.error("invalid method call!") : A.init.apply(this, arguments)
		}
	}(jQuery),
	function(a) {
		var b, c, d, e, f, g, h, i = "Close",
			j = "BeforeClose",
			k = "AfterClose",
			l = "BeforeAppend",
			m = "MarkupParse",
			n = "Open",
			o = "Change",
			p = "mfp",
			q = "." + p,
			r = "mfp-ready",
			s = "mfp-removing",
			t = "mfp-prevent-close",
			u = function() {},
			v = !!window.jQuery,
			w = a(window),
			x = function(a, c) {
				b.ev.on(p + a + q, c)
			},
			y = function(b, c, d, e) {
				var f = document.createElement("div");
				return f.className = "mfp-" + b, d && (f.innerHTML = d), e ? c && c.appendChild(f) : (f = a(f), c && f.appendTo(c)), f
			},
			z = function(c, d) {
				b.ev.triggerHandler(p + c, d), b.st.callbacks && (c = c.charAt(0).toLowerCase() + c.slice(1), b.st.callbacks[c] && b.st.callbacks[c].apply(b, a.isArray(d) ? d : [d]))
			},
			A = function(c) {
				return c === h && b.currTemplate.closeBtn || (b.currTemplate.closeBtn = a(b.st.closeMarkup.replace("%title%", b.st.tClose)), h = c), b.currTemplate.closeBtn
			},
			B = function() {
				a.magnificPopup.instance || (b = new u, b.init(), a.magnificPopup.instance = b)
			},
			C = function() {
				var a = document.createElement("p").style,
					b = ["ms", "O", "Moz", "Webkit"];
				if (void 0 !== a.transition) return !0;
				for (; b.length;)
					if (b.pop() + "Transition" in a) return !0;
				return !1
			};
		u.prototype = {
			constructor: u,
			init: function() {
				var c = navigator.appVersion;
				b.isIE7 = -1 !== c.indexOf("MSIE 7."), b.isIE8 = -1 !== c.indexOf("MSIE 8."), b.isLowIE = b.isIE7 || b.isIE8, b.isAndroid = /android/gi.test(c), b.isIOS = /iphone|ipad|ipod/gi.test(c), b.supportsTransition = C(), b.probablyMobile = b.isAndroid || b.isIOS || /(Opera Mini)|Kindle|webOS|BlackBerry|(Opera Mobi)|(Windows Phone)|IEMobile/i.test(navigator.userAgent), e = a(document), b.popupsCache = {}
			},
			open: function(c) {
				d || (d = a(document.body));
				var f;
				if (c.isObj === !1) {
					b.items = c.items.toArray(), b.index = 0;
					var h, i = c.items;
					for (f = 0; i.length > f; f++)
						if (h = i[f], h.parsed && (h = h.el[0]), h === c.el[0]) {
							b.index = f;
							break
						}
				} else b.items = a.isArray(c.items) ? c.items : [c.items], b.index = c.index || 0;
				if (b.isOpen) return void b.updateItemHTML();
				b.types = [], g = "", b.ev = c.mainEl && c.mainEl.length ? c.mainEl.eq(0) : e, c.key ? (b.popupsCache[c.key] || (b.popupsCache[c.key] = {}), b.currTemplate = b.popupsCache[c.key]) : b.currTemplate = {}, b.st = a.extend(!0, {}, a.magnificPopup.defaults, c), b.fixedContentPos = "auto" === b.st.fixedContentPos ? !b.probablyMobile : b.st.fixedContentPos, b.st.modal && (b.st.closeOnContentClick = !1, b.st.closeOnBgClick = !1, b.st.showCloseBtn = !1, b.st.enableEscapeKey = !1), b.bgOverlay || (b.bgOverlay = y("bg").on("click" + q, function() {
					b.close()
				}), b.wrap = y("wrap").attr("tabindex", -1).on("click" + q, function(a) {
					b._checkIfClose(a.target) && b.close()
				}), b.container = y("container", b.wrap)), b.contentContainer = y("content"), b.st.preloader && (b.preloader = y("preloader", b.container, b.st.tLoading));
				var j = a.magnificPopup.modules;
				for (f = 0; j.length > f; f++) {
					var k = j[f];
					k = k.charAt(0).toUpperCase() + k.slice(1), b["init" + k].call(b)
				}
				z("BeforeOpen"), b.st.showCloseBtn && (b.st.closeBtnInside ? (x(m, function(a, b, c, d) {
					c.close_replaceWith = A(d.type)
				}), g += " mfp-close-btn-in") : b.wrap.append(A())), b.st.alignTop && (g += " mfp-align-top"), b.fixedContentPos ? b.wrap.css({
					overflow: b.st.overflowY,
					overflowX: "hidden",
					overflowY: b.st.overflowY
				}) : b.wrap.css({
					top: w.scrollTop(),
					position: "absolute"
				}), (b.st.fixedBgPos === !1 || "auto" === b.st.fixedBgPos && !b.fixedContentPos) && b.bgOverlay.css({
					height: e.height(),
					position: "absolute"
				}), b.st.enableEscapeKey && e.on("keyup" + q, function(a) {
					27 === a.keyCode && b.close()
				}), w.on("resize" + q, function() {
					b.updateSize()
				}), b.st.closeOnContentClick || (g += " mfp-auto-cursor"), g && b.wrap.addClass(g);
				var l = b.wH = w.height(),
					o = {};
				if (b.fixedContentPos && b._hasScrollBar(l)) {
					var p = b._getScrollbarSize();
					p && (o.marginRight = p)
				}
				b.fixedContentPos && (b.isIE7 ? a("body, html").css("overflow", "hidden") : o.overflow = "hidden");
				var s = b.st.mainClass;
				return b.isIE7 && (s += " mfp-ie7"), s && b._addClassToMFP(s), b.updateItemHTML(), z("BuildControls"), a("html").css(o), b.bgOverlay.add(b.wrap).prependTo(b.st.prependTo || d), b._lastFocusedEl = document.activeElement, setTimeout(function() {
					b.content ? (b._addClassToMFP(r), b._setFocus()) : b.bgOverlay.addClass(r), e.on("focusin" + q, b._onFocusIn)
				}, 16), b.isOpen = !0, b.updateSize(l), z(n), c
			},
			close: function() {
				b.isOpen && (z(j), b.isOpen = !1, b.st.removalDelay && !b.isLowIE && b.supportsTransition ? (b._addClassToMFP(s), setTimeout(function() {
					b._close()
				}, b.st.removalDelay)) : b._close())
			},
			_close: function() {
				z(i);
				var c = s + " " + r + " ";
				if (b.bgOverlay.detach(), b.wrap.detach(), b.container.empty(), b.st.mainClass && (c += b.st.mainClass + " "), b._removeClassFromMFP(c), b.fixedContentPos) {
					var d = {
						marginRight: ""
					};
					b.isIE7 ? a("body, html").css("overflow", "") : d.overflow = "", a("html").css(d)
				}
				e.off("keyup" + q + " focusin" + q), b.ev.off(q), b.wrap.attr("class", "mfp-wrap").removeAttr("style"), b.bgOverlay.attr("class", "mfp-bg"), b.container.attr("class", "mfp-container"), !b.st.showCloseBtn || b.st.closeBtnInside && b.currTemplate[b.currItem.type] !== !0 || b.currTemplate.closeBtn && b.currTemplate.closeBtn.detach(), b._lastFocusedEl && a(b._lastFocusedEl).focus(), b.currItem = null, b.content = null, b.currTemplate = null, b.prevHeight = 0, z(k)
			},
			updateSize: function(a) {
				if (b.isIOS) {
					var c = document.documentElement.clientWidth / window.innerWidth,
						d = window.innerHeight * c;
					b.wrap.css("height", d), b.wH = d
				} else b.wH = a || w.height();
				b.fixedContentPos || b.wrap.css("height", b.wH), z("Resize")
			},
			updateItemHTML: function() {
				var c = b.items[b.index];
				b.contentContainer.detach(), b.content && b.content.detach(), c.parsed || (c = b.parseEl(b.index));
				var d = c.type;
				if (z("BeforeChange", [b.currItem ? b.currItem.type : "", d]), b.currItem = c, !b.currTemplate[d]) {
					var e = !!b.st[d] && b.st[d].markup;
					z("FirstMarkupParse", e), b.currTemplate[d] = !e || a(e)
				}
				f && f !== c.type && b.container.removeClass("mfp-" + f + "-holder");
				var g = b["get" + d.charAt(0).toUpperCase() + d.slice(1)](c, b.currTemplate[d]);
				b.appendContent(g, d), c.preloaded = !0, z(o, c), f = c.type, b.container.prepend(b.contentContainer), z("AfterChange")
			},
			appendContent: function(a, c) {
				b.content = a, a ? b.st.showCloseBtn && b.st.closeBtnInside && b.currTemplate[c] === !0 ? b.content.find(".mfp-close").length || b.content.append(A()) : b.content = a : b.content = "", z(l), b.container.addClass("mfp-" + c + "-holder"), b.contentContainer.append(b.content)
			},
			parseEl: function(c) {
				var d, e = b.items[c];
				if (e.tagName ? e = {
						el: a(e)
					} : (d = e.type, e = {
						data: e,
						src: e.src
					}), e.el) {
					for (var f = b.types, g = 0; f.length > g; g++)
						if (e.el.hasClass("mfp-" + f[g])) {
							d = f[g];
							break
						}
					e.src = e.el.attr("data-mfp-src"), e.src || (e.src = e.el.attr("href"))
				}
				return e.type = d || b.st.type || "inline", e.index = c, e.parsed = !0, b.items[c] = e, z("ElementParse", e), b.items[c]
			},
			addGroup: function(a, c) {
				var d = function(d) {
					d.mfpEl = this, b._openClick(d, a, c)
				};
				c || (c = {});
				var e = "click.magnificPopup";
				c.mainEl = a, c.items ? (c.isObj = !0, a.off(e).on(e, d)) : (c.isObj = !1, c.delegate ? a.off(e).on(e, c.delegate, d) : (c.items = a, a.off(e).on(e, d)))
			},
			_openClick: function(c, d, e) {
				var f = void 0 !== e.midClick ? e.midClick : a.magnificPopup.defaults.midClick;
				if (f || 2 !== c.which && !c.ctrlKey && !c.metaKey) {
					var g = void 0 !== e.disableOn ? e.disableOn : a.magnificPopup.defaults.disableOn;
					if (g)
						if (a.isFunction(g)) {
							if (!g.call(b)) return !0
						} else if (g > w.width()) return !0;
					c.type && (c.preventDefault(), b.isOpen && c.stopPropagation()), e.el = a(c.mfpEl), e.delegate && (e.items = d.find(e.delegate)), b.open(e)
				}
			},
			updateStatus: function(a, d) {
				if (b.preloader) {
					c !== a && b.container.removeClass("mfp-s-" + c), d || "loading" !== a || (d = b.st.tLoading);
					var e = {
						status: a,
						text: d
					};
					z("UpdateStatus", e), a = e.status, d = e.text, b.preloader.html(d), b.preloader.find("a").on("click", function(a) {
						a.stopImmediatePropagation()
					}), b.container.addClass("mfp-s-" + a), c = a
				}
			},
			_checkIfClose: function(c) {
				if (!a(c).hasClass(t)) {
					var d = b.st.closeOnContentClick,
						e = b.st.closeOnBgClick;
					if (d && e) return !0;
					if (!b.content || a(c).hasClass("mfp-close") || b.preloader && c === b.preloader[0]) return !0;
					if (c === b.content[0] || a.contains(b.content[0], c)) {
						if (d) return !0
					} else if (e && a.contains(document, c)) return !0;
					return !1
				}
			},
			_addClassToMFP: function(a) {
				b.bgOverlay.addClass(a), b.wrap.addClass(a)
			},
			_removeClassFromMFP: function(a) {
				this.bgOverlay.removeClass(a), b.wrap.removeClass(a)
			},
			_hasScrollBar: function(a) {
				return (b.isIE7 ? e.height() : document.body.scrollHeight) > (a || w.height())
			},
			_setFocus: function() {
				(b.st.focus ? b.content.find(b.st.focus).eq(0) : b.wrap).focus()
			},
			_onFocusIn: function(c) {
				return c.target === b.wrap[0] || a.contains(b.wrap[0], c.target) ? void 0 : (b._setFocus(), !1)
			},
			_parseMarkup: function(b, c, d) {
				var e;
				d.data && (c = a.extend(d.data, c)), z(m, [b, c, d]), a.each(c, function(a, c) {
					if (void 0 === c || c === !1) return !0;
					if (e = a.split("_"), e.length > 1) {
						var d = b.find(q + "-" + e[0]);
						if (d.length > 0) {
							var f = e[1];
							"replaceWith" === f ? d[0] !== c[0] && d.replaceWith(c) : "img" === f ? d.is("img") ? d.attr("src", c) : d.replaceWith('<img src="' + c + '" class="' + d.attr("class") + '" />') : d.attr(e[1], c)
						}
					} else b.find(q + "-" + a).html(c)
				})
			},
			_getScrollbarSize: function() {
				if (void 0 === b.scrollbarSize) {
					var a = document.createElement("div");
					a.id = "mfp-sbm", a.style.cssText = "width: 99px; height: 99px; overflow: scroll; position: absolute; top: -9999px;", document.body.appendChild(a), b.scrollbarSize = a.offsetWidth - a.clientWidth, document.body.removeChild(a)
				}
				return b.scrollbarSize
			}
		}, a.magnificPopup = {
			instance: null,
			proto: u.prototype,
			modules: [],
			open: function(b, c) {
				return B(), b = b ? a.extend(!0, {}, b) : {}, b.isObj = !0, b.index = c || 0, this.instance.open(b)
			},
			close: function() {
				return a.magnificPopup.instance && a.magnificPopup.instance.close()
			},
			registerModule: function(b, c) {
				c.options && (a.magnificPopup.defaults[b] = c.options), a.extend(this.proto, c.proto), this.modules.push(b)
			},
			defaults: {
				disableOn: 0,
				key: null,
				midClick: !1,
				mainClass: "",
				preloader: !0,
				focus: "",
				closeOnContentClick: !1,
				closeOnBgClick: !0,
				closeBtnInside: !0,
				showCloseBtn: !0,
				enableEscapeKey: !0,
				modal: !1,
				alignTop: !1,
				removalDelay: 0,
				prependTo: null,
				fixedContentPos: "auto",
				fixedBgPos: "auto",
				overflowY: "auto",
				closeMarkup: '<button title="%title%" type="button" class="mfp-close">&times;</button>',
				tClose: "Close (Esc)",
				tLoading: "Loading..."
			}
		}, a.fn.magnificPopup = function(c) {
			B();
			var d = a(this);
			if ("string" == typeof c)
				if ("open" === c) {
					var e, f = v ? d.data("magnificPopup") : d[0].magnificPopup,
						g = parseInt(arguments[1], 10) || 0;
					f.items ? e = f.items[g] : (e = d, f.delegate && (e = e.find(f.delegate)), e = e.eq(g)), b._openClick({
						mfpEl: e
					}, d, f)
				} else b.isOpen && b[c].apply(b, Array.prototype.slice.call(arguments, 1));
			else c = a.extend(!0, {}, c), v ? d.data("magnificPopup", c) : d[0].magnificPopup = c, b.addGroup(d, c);
			return d
		};
		var D, E, F, G = "inline",
			H = function() {
				F && (E.after(F.addClass(D)).detach(), F = null)
			};
		a.magnificPopup.registerModule(G, {
			options: {
				hiddenClass: "hide",
				markup: "",
				tNotFound: "Content not found"
			},
			proto: {
				initInline: function() {
					b.types.push(G), x(i + "." + G, function() {
						H()
					})
				},
				getInline: function(c, d) {
					if (H(), c.src) {
						var e = b.st.inline,
							f = a(c.src);
						if (f.length) {
							var g = f[0].parentNode;
							g && g.tagName && (E || (D = e.hiddenClass, E = y(D), D = "mfp-" + D), F = f.after(E).detach().removeClass(D)), b.updateStatus("ready")
						} else b.updateStatus("error", e.tNotFound), f = a("<div>");
						return c.inlineElement = f, f
					}
					return b.updateStatus("ready"), b._parseMarkup(d, {}, c), d
				}
			}
		});
		var I, J = "ajax",
			K = function() {
				I && d.removeClass(I)
			},
			L = function() {
				K(), b.req && b.req.abort()
			};
		a.magnificPopup.registerModule(J, {
			options: {
				settings: null,
				cursor: "mfp-ajax-cur",
				tError: '<a href="%url%">The content</a> could not be loaded.'
			},
			proto: {
				initAjax: function() {
					b.types.push(J), I = b.st.ajax.cursor, x(i + "." + J, L), x("BeforeChange." + J, L)
				},
				getAjax: function(c) {
					I && d.addClass(I), b.updateStatus("loading");
					var e = a.extend({
						url: c.src,
						success: function(d, e, f) {
							var g = {
								data: d,
								xhr: f
							};
							z("ParseAjax", g), b.appendContent(a(g.data), J), c.finished = !0, K(), b._setFocus(), setTimeout(function() {
								b.wrap.addClass(r)
							}, 16), b.updateStatus("ready"), z("AjaxContentAdded")
						},
						error: function() {
							K(), c.finished = c.loadError = !0, b.updateStatus("error", b.st.ajax.tError.replace("%url%", c.src))
						}
					}, b.st.ajax.settings);
					return b.req = a.ajax(e), ""
				}
			}
		});
		var M, N = function(c) {
			if (c.data && void 0 !== c.data.title) return c.data.title;
			var d = b.st.image.titleSrc;
			if (d) {
				if (a.isFunction(d)) return d.call(b, c);
				if (c.el) return c.el.attr(d) || ""
			}
			return ""
		};
		a.magnificPopup.registerModule("image", {
			options: {
				markup: '<div class="mfp-figure"><div class="mfp-close"></div><figure><div class="mfp-img"></div><figcaption><div class="mfp-bottom-bar"><div class="mfp-title"></div><div class="mfp-counter"></div></div></figcaption></figure></div>',
				cursor: "mfp-zoom-out-cur",
				titleSrc: "title",
				verticalFit: !0,
				tError: '<a href="%url%">The image</a> could not be loaded.'
			},
			proto: {
				initImage: function() {
					var a = b.st.image,
						c = ".image";
					b.types.push("image"), x(n + c, function() {
						"image" === b.currItem.type && a.cursor && d.addClass(a.cursor)
					}), x(i + c, function() {
						a.cursor && d.removeClass(a.cursor), w.off("resize" + q)
					}), x("Resize" + c, b.resizeImage), b.isLowIE && x("AfterChange", b.resizeImage)
				},
				resizeImage: function() {
					var a = b.currItem;
					if (a && a.img && b.st.image.verticalFit) {
						var c = 0;
						b.isLowIE && (c = parseInt(a.img.css("padding-top"), 10) + parseInt(a.img.css("padding-bottom"), 10)), a.img.css("max-height", b.wH - c)
					}
				},
				_onImageHasSize: function(a) {
					a.img && (a.hasSize = !0, M && clearInterval(M), a.isCheckingImgSize = !1, z("ImageHasSize", a), a.imgHidden && (b.content && b.content.removeClass("mfp-loading"), a.imgHidden = !1))
				},
				findImageSize: function(a) {
					var c = 0,
						d = a.img[0],
						e = function(f) {
							M && clearInterval(M), M = setInterval(function() {
								return d.naturalWidth > 0 ? void b._onImageHasSize(a) : (c > 200 && clearInterval(M), c++, void(3 === c ? e(10) : 40 === c ? e(50) : 100 === c && e(500)))
							}, f)
						};
					e(1)
				},
				getImage: function(c, d) {
					var e = 0,
						f = function() {
							c && (c.img[0].complete ? (c.img.off(".mfploader"), c === b.currItem && (b._onImageHasSize(c), b.updateStatus("ready")), c.hasSize = !0, c.loaded = !0, z("ImageLoadComplete")) : (e++, 200 > e ? setTimeout(f, 100) : g()))
						},
						g = function() {
							c && (c.img.off(".mfploader"), c === b.currItem && (b._onImageHasSize(c), b.updateStatus("error", h.tError.replace("%url%", c.src))), c.hasSize = !0, c.loaded = !0, c.loadError = !0)
						},
						h = b.st.image,
						i = d.find(".mfp-img");
					if (i.length) {
						var j = document.createElement("img");
						j.className = "mfp-img", c.img = a(j).on("load.mfploader", f).on("error.mfploader", g), j.src = c.src, i.is("img") && (c.img = c.img.clone()), j = c.img[0], j.naturalWidth > 0 ? c.hasSize = !0 : j.width || (c.hasSize = !1)
					}
					return b._parseMarkup(d, {
						title: N(c),
						img_replaceWith: c.img
					}, c), b.resizeImage(), c.hasSize ? (M && clearInterval(M), c.loadError ? (d.addClass("mfp-loading"), b.updateStatus("error", h.tError.replace("%url%", c.src))) : (d.removeClass("mfp-loading"), b.updateStatus("ready")), d) : (b.updateStatus("loading"), c.loading = !0, c.hasSize || (c.imgHidden = !0, d.addClass("mfp-loading"), b.findImageSize(c)), d)
				}
			}
		});
		var O, P = function() {
			return void 0 === O && (O = void 0 !== document.createElement("p").style.MozTransform), O
		};
		a.magnificPopup.registerModule("zoom", {
			options: {
				enabled: !1,
				easing: "ease-in-out",
				duration: 300,
				opener: function(a) {
					return a.is("img") ? a : a.find("img")
				}
			},
			proto: {
				initZoom: function() {
					var a, c = b.st.zoom,
						d = ".zoom";
					if (c.enabled && b.supportsTransition) {
						var e, f, g = c.duration,
							h = function(a) {
								var b = a.clone().removeAttr("style").removeAttr("class").addClass("mfp-animated-image"),
									d = "all " + c.duration / 1e3 + "s " + c.easing,
									e = {
										position: "fixed",
										zIndex: 9999,
										left: 0,
										top: 0,
										"-webkit-backface-visibility": "hidden"
									},
									f = "transition";
								return e["-webkit-" + f] = e["-moz-" + f] = e["-o-" + f] = e[f] = d, b.css(e), b
							},
							k = function() {
								b.content.css("visibility", "visible")
							};
						x("BuildControls" + d, function() {
							if (b._allowZoom()) {
								if (clearTimeout(e), b.content.css("visibility", "hidden"), a = b._getItemToZoom(), !a) return void k();
								f = h(a), f.css(b._getOffset()), b.wrap.append(f), e = setTimeout(function() {
									f.css(b._getOffset(!0)), e = setTimeout(function() {
										k(), setTimeout(function() {
											f.remove(), a = f = null, z("ZoomAnimationEnded")
										}, 16)
									}, g)
								}, 16)
							}
						}), x(j + d, function() {
							if (b._allowZoom()) {
								if (clearTimeout(e), b.st.removalDelay = g, !a) {
									if (a = b._getItemToZoom(), !a) return;
									f = h(a)
								}
								f.css(b._getOffset(!0)), b.wrap.append(f), b.content.css("visibility", "hidden"), setTimeout(function() {
									f.css(b._getOffset())
								}, 16)
							}
						}), x(i + d, function() {
							b._allowZoom() && (k(), f && f.remove(), a = null)
						})
					}
				},
				_allowZoom: function() {
					return "image" === b.currItem.type
				},
				_getItemToZoom: function() {
					return !!b.currItem.hasSize && b.currItem.img
				},
				_getOffset: function(c) {
					var d;
					d = c ? b.currItem.img : b.st.zoom.opener(b.currItem.el || b.currItem);
					var e = d.offset(),
						f = parseInt(d.css("padding-top"), 10),
						g = parseInt(d.css("padding-bottom"), 10);
					e.top -= a(window).scrollTop() - f;
					var h = {
						width: d.width(),
						height: (v ? d.innerHeight() : d[0].offsetHeight) - g - f
					};
					return P() ? h["-moz-transform"] = h.transform = "translate(" + e.left + "px," + e.top + "px)" : (h.left = e.left, h.top = e.top), h
				}
			}
		});
		var Q = "iframe",
			R = "//about:blank",
			S = function(a) {
				if (b.currTemplate[Q]) {
					var c = b.currTemplate[Q].find("iframe");
					c.length && (a || (c[0].src = R), b.isIE8 && c.css("display", a ? "block" : "none"))
				}
			};
		a.magnificPopup.registerModule(Q, {
			options: {
				markup: '<div class="mfp-iframe-scaler"><div class="mfp-close"></div><iframe class="mfp-iframe" src="//about:blank" frameborder="0" allowfullscreen></iframe></div>',
				srcAction: "iframe_src",
				patterns: {
					youtube: {
						index: "youtube.com",
						id: "v=",
						src: "//www.youtube.com/embed/%id%?autoplay=1"
					},
					vimeo: {
						index: "vimeo.com/",
						id: "/",
						src: "//player.vimeo.com/video/%id%?autoplay=1"
					},
					gmaps: {
						index: "//maps.google.",
						src: "%id%&output=embed"
					}
				}
			},
			proto: {
				initIframe: function() {
					b.types.push(Q), x("BeforeChange", function(a, b, c) {
						b !== c && (b === Q ? S() : c === Q && S(!0))
					}), x(i + "." + Q, function() {
						S()
					})
				},
				getIframe: function(c, d) {
					var e = c.src,
						f = b.st.iframe;
					a.each(f.patterns, function() {
						return e.indexOf(this.index) > -1 ? (this.id && (e = "string" == typeof this.id ? e.substr(e.lastIndexOf(this.id) + this.id.length, e.length) : this.id.call(this, e)), e = this.src.replace("%id%", e), !1) : void 0
					});
					var g = {};
					return f.srcAction && (g[f.srcAction] = e), b._parseMarkup(d, g, c), b.updateStatus("ready"), d
				}
			}
		});
		var T = function(a) {
				var c = b.items.length;
				return a > c - 1 ? a - c : 0 > a ? c + a : a
			},
			U = function(a, b, c) {
				return a.replace(/%curr%/gi, b + 1).replace(/%total%/gi, c)
			};
		a.magnificPopup.registerModule("gallery", {
			options: {
				enabled: !1,
				arrowMarkup: '<button title="%title%" type="button" class="mfp-arrow mfp-arrow-%dir%"></button>',
				preload: [0, 2],
				navigateByImgClick: !0,
				arrows: !0,
				tPrev: "Previous (Left arrow key)",
				tNext: "Next (Right arrow key)",
				tCounter: "%curr% of %total%"
			},
			proto: {
				initGallery: function() {
					var c = b.st.gallery,
						d = ".mfp-gallery",
						f = Boolean(a.fn.mfpFastClick);
					return b.direction = !0, !(!c || !c.enabled) && (g += " mfp-gallery", x(n + d, function() {
						c.navigateByImgClick && b.wrap.on("click" + d, ".mfp-img", function() {
							return b.items.length > 1 ? (b.next(), !1) : void 0
						}), e.on("keydown" + d, function(a) {
							37 === a.keyCode ? b.prev() : 39 === a.keyCode && b.next()
						})
					}), x("UpdateStatus" + d, function(a, c) {
						c.text && (c.text = U(c.text, b.currItem.index, b.items.length))
					}), x(m + d, function(a, d, e, f) {
						var g = b.items.length;
						e.counter = g > 1 ? U(c.tCounter, f.index, g) : ""
					}), x("BuildControls" + d, function() {
						if (b.items.length > 1 && c.arrows && !b.arrowLeft) {
							var d = c.arrowMarkup,
								e = b.arrowLeft = a(d.replace(/%title%/gi, c.tPrev).replace(/%dir%/gi, "left")).addClass(t),
								g = b.arrowRight = a(d.replace(/%title%/gi, c.tNext).replace(/%dir%/gi, "right")).addClass(t),
								h = f ? "mfpFastClick" : "click";
							e[h](function() {
								b.prev()
							}), g[h](function() {
								b.next()
							}), b.isIE7 && (y("b", e[0], !1, !0), y("a", e[0], !1, !0), y("b", g[0], !1, !0), y("a", g[0], !1, !0)), b.container.append(e.add(g))
						}
					}), x(o + d, function() {
						b._preloadTimeout && clearTimeout(b._preloadTimeout), b._preloadTimeout = setTimeout(function() {
							b.preloadNearbyImages(), b._preloadTimeout = null
						}, 16)
					}), void x(i + d, function() {
						e.off(d), b.wrap.off("click" + d), b.arrowLeft && f && b.arrowLeft.add(b.arrowRight).destroyMfpFastClick(), b.arrowRight = b.arrowLeft = null
					}))
				},
				next: function() {
					b.direction = !0, b.index = T(b.index + 1), b.updateItemHTML()
				},
				prev: function() {
					b.direction = !1, b.index = T(b.index - 1), b.updateItemHTML()
				},
				goTo: function(a) {
					b.direction = a >= b.index, b.index = a, b.updateItemHTML()
				},
				preloadNearbyImages: function() {
					var a, c = b.st.gallery.preload,
						d = Math.min(c[0], b.items.length),
						e = Math.min(c[1], b.items.length);
					for (a = 1;
						(b.direction ? e : d) >= a; a++) b._preloadItem(b.index + a);
					for (a = 1;
						(b.direction ? d : e) >= a; a++) b._preloadItem(b.index - a)
				},
				_preloadItem: function(c) {
					if (c = T(c), !b.items[c].preloaded) {
						var d = b.items[c];
						d.parsed || (d = b.parseEl(c)), z("LazyLoad", d), "image" === d.type && (d.img = a('<img class="mfp-img" />').on("load.mfploader", function() {
							d.hasSize = !0
						}).on("error.mfploader", function() {
							d.hasSize = !0, d.loadError = !0, z("LazyLoadError", d)
						}).attr("src", d.src)), d.preloaded = !0
					}
				}
			}
		});
		var V = "retina";
		a.magnificPopup.registerModule(V, {
				options: {
					replaceSrc: function(a) {
						return a.src.replace(/\.\w+$/, function(a) {
							return "@2x" + a
						})
					},
					ratio: 1
				},
				proto: {
					initRetina: function() {
						if (window.devicePixelRatio > 1) {
							var a = b.st.retina,
								c = a.ratio;
							c = isNaN(c) ? c() : c, c > 1 && (x("ImageHasSize." + V, function(a, b) {
								b.img.css({
									"max-width": b.img[0].naturalWidth / c,
									width: "100%"
								})
							}), x("ElementParse." + V, function(b, d) {
								d.src = a.replaceSrc(d, c)
							}))
						}
					}
				}
			}),
			function() {
				var b = 1e3,
					c = "ontouchstart" in window,
					d = function() {
						w.off("touchmove" + f + " touchend" + f)
					},
					e = "mfpFastClick",
					f = "." + e;
				a.fn.mfpFastClick = function(e) {
					return a(this).each(function() {
						var g, h = a(this);
						if (c) {
							var i, j, k, l, m, n;
							h.on("touchstart" + f, function(a) {
								l = !1, n = 1, m = a.originalEvent ? a.originalEvent.touches[0] : a.touches[0], j = m.clientX, k = m.clientY, w.on("touchmove" + f, function(a) {
									m = a.originalEvent ? a.originalEvent.touches : a.touches, n = m.length, m = m[0], (Math.abs(m.clientX - j) > 10 || Math.abs(m.clientY - k) > 10) && (l = !0, d())
								}).on("touchend" + f, function(a) {
									d(), l || n > 1 || (g = !0, a.preventDefault(), clearTimeout(i), i = setTimeout(function() {
										g = !1
									}, b), e())
								})
							})
						}
						h.on("click" + f, function() {
							g || e()
						})
					})
				}, a.fn.destroyMfpFastClick = function() {
					a(this).off("touchstart" + f + " click" + f), c && w.off("touchmove" + f + " touchend" + f)
				}
			}(), B()
	}(window.jQuery || window.Zepto),
	function(a, b, c) {
		function d(a) {
			var b = {},
				d = /^jQuery\d+$/;
			return c.each(a.attributes, function(a, c) {
				c.specified && !d.test(c.name) && (b[c.name] = c.value)
			}), b
		}

		function e(a, b) {
			var d = c(this);
			if (this.value == d.attr("placeholder") && d.hasClass("placeholder"))
				if (d.data("placeholder-password")) {
					if (d = d.hide().next().show().attr("id", d.removeAttr("id").data("placeholder-id")), !0 === a) return d[0].value = b;
					d.focus()
				} else this.value = "", d.removeClass("placeholder"), this == g() && this.select()
		}

		function f() {
			var a, b = c(this),
				f = this.id;
			if ("" == this.value) {
				if ("password" == this.type) {
					if (!b.data("placeholder-textinput")) {
						try {
							a = b.clone().attr({
								type: "text"
							})
						} catch (b) {
							a = c("<input>").attr(c.extend(d(this), {
								type: "text"
							}))
						}
						a.removeAttr("name").data({
							"placeholder-password": b,
							"placeholder-id": f
						}).bind("focus.placeholder", e), b.data({
							"placeholder-textinput": a,
							"placeholder-id": f
						}).before(a)
					}
					b = b.removeAttr("id").hide().prev().attr("id", f).show()
				}
				b.addClass("placeholder"), b[0].value = b.attr("placeholder")
			} else b.removeClass("placeholder")
		}

		function g() {
			try {
				return b.activeElement
			} catch (a) {}
		}
		var h = "placeholder" in b.createElement("input"),
			i = "placeholder" in b.createElement("textarea"),
			j = c.fn,
			k = c.valHooks,
			l = c.propHooks;
		h && i ? (j = j.placeholder = function() {
			return this
		}, j.input = j.textarea = !0) : (j = j.placeholder = function() {
			return this.filter((h ? "textarea" : ":input") + "[placeholder]").not(".placeholder").bind({
				"focus.placeholder": e,
				"blur.placeholder": f
			}).data("placeholder-enabled", !0).trigger("blur.placeholder"), this
		}, j.input = h, j.textarea = i, j = {
			get: function(a) {
				var b = c(a),
					d = b.data("placeholder-password");
				return d ? d[0].value : b.data("placeholder-enabled") && b.hasClass("placeholder") ? "" : a.value
			},
			set: function(a, b) {
				var d = c(a),
					h = d.data("placeholder-password");
				return h ? h[0].value = b : d.data("placeholder-enabled") ? ("" == b ? (a.value = b, a != g() && f.call(a)) : d.hasClass("placeholder") ? e.call(a, !0, b) || (a.value = b) : a.value = b, d) : a.value = b
			}
		}, h || (k.input = j, l.value = j), i || (k.textarea = j, l.value = j), c(function() {
			c(b).delegate("form", "submit.placeholder", function() {
				var a = c(".placeholder", this).each(e);
				setTimeout(function() {
					a.each(f)
				}, 10)
			})
		}), c(a).bind("beforeunload.placeholder", function() {
			c(".placeholder").each(function() {
				this.value = ""
			})
		}))
	}(this, document, jQuery),
	function() {
		for (var a = 0, b = ["ms", "moz", "webkit", "o"], c = 0; c < b.length && !window.requestAnimationFrame; ++c) window.requestAnimationFrame = window[b[c] + "RequestAnimationFrame"], window.cancelAnimationFrame = window[b[c] + "CancelAnimationFrame"] || window[b[c] + "CancelRequestAnimationFrame"];
		window.requestAnimationFrame || (window.requestAnimationFrame = function(b, c) {
			var d = (new Date).getTime(),
				e = Math.max(0, 16 - (d - a)),
				f = window.setTimeout(function() {
					b(d + e)
				}, e);
			return a = d + e, f
		}), window.cancelAnimationFrame || (window.cancelAnimationFrame = function(a) {
			clearTimeout(a)
		})
	}();
var tdDetect = {};
! function() {
	"use strict";
	tdDetect = {
		isIe8: !1,
		isIe9: !1,
		isIe10: !1,
		isIe11: !1,
		isIe: !1,
		isSafari: !1,
		isChrome: !1,
		isIpad: !1,
		isTouchDevice: !1,
		hasHistory: !1,
		isPhoneScreen: !1,
		isIos: !1,
		isAndroid: !1,
		isOsx: !1,
		isFirefox: !1,
		isWinOs: !1,
		isMobileDevice: !1,
		htmlJqueryObj: null,
		runIsPhoneScreen: function() {
			(jQuery(window).width() < 768 || jQuery(window).height() < 768) && !1 === tdDetect.isIpad ? tdDetect.isPhoneScreen = !0 : tdDetect.isPhoneScreen = !1
		},
		set: function(a, b) {
			tdDetect[a] = b
		}
	}, tdDetect.htmlJqueryObj = jQuery("html"), -1 !== navigator.appVersion.indexOf("Win") && tdDetect.set("isWinOs", !0), "ontouchstart" in window && !tdDetect.isWinOs && tdDetect.set("isTouchDevice", !0), tdDetect.htmlJqueryObj.is(".ie8") && (tdDetect.set("isIe8", !0), tdDetect.set("isIe", !0)), tdDetect.htmlJqueryObj.is(".ie9") && (tdDetect.set("isIe9", !0), tdDetect.set("isIe", !0)), navigator.userAgent.indexOf("MSIE 10.0") > -1 && (tdDetect.set("isIe10", !0), tdDetect.set("isIe", !0)), navigator.userAgent.match(/Trident.*rv\:11\./) && tdDetect.set("isIe11", !0), window.history && window.history.pushState && tdDetect.set("hasHistory", !0), -1 !== navigator.userAgent.indexOf("Safari") && -1 === navigator.userAgent.indexOf("Chrome") && tdDetect.set("isSafari", !0), /chrom(e|ium)/.test(navigator.userAgent.toLowerCase()) && tdDetect.set("isChrome", !0), null !== navigator.userAgent.match(/iPad/i) && tdDetect.set("isIpad", !0), /(iPad|iPhone|iPod)/g.test(navigator.userAgent) && tdDetect.set("isIos", !0), /Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent) && tdDetect.set("isMobileDevice", !0), tdDetect.runIsPhoneScreen();
	var a = navigator.userAgent.toLowerCase();
	a.indexOf("android") > -1 && tdDetect.set("isAndroid", !0), -1 !== navigator.userAgent.indexOf("Mac OS X") && tdDetect.set("isOsx", !0), -1 !== navigator.userAgent.indexOf("Firefox") && tdDetect.set("isFirefox", !0)
}();
var tdViewport = {};
! function() {
	"use strict";
	tdViewport = {
		INTERVAL_INITIAL_INDEX: -1,
		_currentIntervalIndex: tdViewport.INTERVAL_INITIAL_INDEX,
		_intervalList: [],
		init: function() {
			if ("undefined" != typeof window.td_viewport_interval_list && Array === window.td_viewport_interval_list.constructor) {
				for (var a = 0; a < window.td_viewport_interval_list.length; a++) {
					var b = new tdViewport.item,
						c = window.td_viewport_interval_list[a];
					if (!c.hasOwnProperty("limitBottom") || !c.hasOwnProperty("sidebarWidth")) break;
					b.limitBottom = c.limitBottom, b.sidebarWidth = c.sidebarWidth, tdViewport._items.push(b)
				}
				tdViewport.detectChanges()
			}
		},
		getCurrentIntervalIndex: function() {
			return tdViewport._currentIntervalIndex
		},
		setIntervalList: function(a) {
			tdViewport._intervalList = a
		},
		getIntervalList: function() {
			return tdViewport._intervalList
		},
		getCurrentIntervalItem: function() {
			return tdViewport.INTERVAL_INITIAL_INDEX === tdViewport._currentIntervalIndex || 0 === tdViewport._currentIntervalIndex ? null : tdViewport._items[tdViewport._currentIntervalIndex - 1]
		},
		_items: [],
		item: function() {
			this.limitBottom = void 0, this.sidebarWidth = void 0
		},
		detectChanges: function() {
			var a = !1,
				b = 0,
				c = 0;
			b = !0 === tdDetect.isSafari ? this._safariWiewPortWidth.getRealWidth() : Math.max(document.documentElement.clientWidth, window.innerWidth || 0);
			for (var d = 0; d < tdViewport._items.length; d++) {
				if (b <= tdViewport._items[d].limitBottom) {
					c !== tdViewport._currentIntervalIndex && (tdViewport._currentIntervalIndex = c, a = !0, tdViewport.log("changing viewport " + tdViewport._currentIntervalIndex + " ~ " + b));
					break
				}
				c++
			}
			return !1 === a && c !== tdViewport._currentIntervalIndex && (tdViewport._currentIntervalIndex = c, a = !0, tdViewport.log("changing viewport " + tdViewport._currentIntervalIndex + " ~ " + b)), a
		},
		_safariWiewPortWidth: {
			divAdded: !1,
			divJqueryObject: "",
			getRealWidth: function() {
				return !1 === this.divAdded && (this.divJqueryObject = jQuery("<div>").css({
					height: "1px",
					position: "absolute",
					top: "-1px",
					left: "0",
					right: "0",
					visibility: "hidden",
					"z-index": "-1"
				}), this.divJqueryObject.appendTo("body"), this.divAdded = !0), this.divJqueryObject.width()
			}
		},
		log: function(b) {}
	}, tdViewport.init()
}();
var tdMenu = {};
! function() {
	"use strict";
	tdMenu = {
		_itemsWithSubmenu: null,
		_mainMenu: null,
		_outsideClickArea: null,
		_outsideClickExcludedAreas: "#td-header-menu .sf-menu, #td-header-menu .sf-menu *, .menu-top-container, .menu-top-container *",
		_openMenuClass: "sfHover",
		_openMenuBodyClass: "td-open-menu",
		init: function() {
			var a = jQuery("#td-header-menu .sf-menu"),
				b = jQuery("#td-header-menu .sf-menu, .top-header-menu"),
				c = b.find(".menu-item-has-children > a, .td-mega-menu > a");
			c.append('<i class="td-icon-menu-down"></i>'), a.supersubs({
				minWidth: 10,
				maxWidth: 20,
				extraWidth: 1
			}), c.addClass("sf-with-ul"), b.addClass("sf-js-enabled"), c.parent().find("ul").first().css("display", "none"), tdMenu._mainMenu = a, tdMenu._itemsWithSubmenu = c, tdMenu._outsideClickArea = jQuery(window).not(tdMenu._outsideClickExcludedAreas), tdMenu._setHover(c, a)
		},
		_getSubmenuPosition: function(a) {
			var b = jQuery(window).width(),
				c = a.children("ul").first();
			if (c.length > 0) {
				var d = c.offset().left + c.width();
				d > b && (c.parent().parent().hasClass("sf-menu") ? c.css("left", "-" + (d - b) + "px") : c.addClass("reversed").css("left", "-" + (c.width() + 0) + "px"))
			}
		},
		_getMouseAngleDirection: function(a, b, c, d) {
			var e = c - a,
				f = d - b;
			return Math.atan2(e, f) / Math.PI * 180
		},
		_setHover: function(a, b) {
			if (tdDetect.isTouchDevice) jQuery(document).on("touchstart", "body", function(b) {
				var c = a.parent(),
					d = jQuery("body");
				d.hasClass(tdMenu._openMenuBodyClass) && !c.is(b.target) && 0 === c.has(b.target).length && (c.removeClass(tdMenu._openMenuClass), c.children("ul").hide(), d.removeClass(tdMenu._openMenuBodyClass))
			}), a.on("touchstart", function(b) {
				b.preventDefault(), b.stopPropagation();
				var c = jQuery(this),
					d = c.parent(),
					e = jQuery("body");
				if (d.hasClass(tdMenu._openMenuClass)) null !== c.attr("href") && "#" !== c.attr("href") ? window.location.href = c.attr("href") : ((d.parent().hasClass("sf-menu") || d.parent().hasClass("top-header-menu")) && e.removeClass(tdMenu._openMenuBodyClass), d.removeClass(tdMenu._openMenuClass), d.find("ul").hide(), d.find("li").removeClass(tdMenu._openMenuClass));
				else {
					if (d.parent().hasClass("sf-menu") || d.parent().hasClass("top-header-menu")) a.parent().removeClass(tdMenu._openMenuClass), a.parent().children("ul").hide();
					else {
						var f = d.siblings();
						f.removeClass(tdMenu._openMenuClass), f.find("ul").hide(), f.find("li").removeClass(tdMenu._openMenuClass)
					}
					d.addClass(tdMenu._openMenuClass), d.children("ul").show(), tdMenu._getSubmenuPosition(d), e.addClass(tdMenu._openMenuBodyClass)
				}
			});
			else {
				var d, c = {},
					e = !0;
				b.on("mouseleave", function() {
					a.parent().removeClass(tdMenu._openMenuClass), a.parent().children("ul").hide(), c = {}
				}), b.find(".menu-item").hover(function() {
					var h, i, j, k, b = jQuery(this),
						f = "",
						g = 5;
					b.hasClass("menu-item-has-children") || b.hasClass("td-mega-menu") ? b.parent().hasClass("sf-menu") ? jQuery.isEmptyObject(c) ? (b.addClass(tdMenu._openMenuClass), b.children("ul").show(), c = b) : b[0] !== c[0] && (h = 0, i = 0, j = 0, k = null, e === !0 && (e = !1, d = setTimeout(function() {
						a.parent().removeClass(tdMenu._openMenuClass), a.parent().children("ul").hide(), b.addClass(tdMenu._openMenuClass), b.children("ul").show(), c = b
					}, 400)), b.on("mousemove", function(f) {
						h >= g ? (h = 0, k = tdMenu._getMouseAngleDirection(i, j, f.pageX, f.pageY), i = f.pageX, j = f.pageY) : (h++, 0 === i && 0 === j && (i = f.pageX, j = f.pageY)), null !== k && (k > 85 || k < -85) && (a.parent().removeClass(tdMenu._openMenuClass), a.parent().children("ul").hide(), b.addClass(tdMenu._openMenuClass), b.children("ul").show(), b.off("mousemove"), clearTimeout(d), e = !0, c = b)
					})) : (f = b.siblings(), f.removeClass(tdMenu._openMenuClass), f.find("ul").hide(), f.find("li").removeClass(tdMenu._openMenuClass), b.addClass(tdMenu._openMenuClass), b.children("ul").show(), tdMenu._getSubmenuPosition(b)) : b.parent().hasClass("sf-menu") || b.parent().hasClass("top-header-menu") ? jQuery.isEmptyObject(c) || (h = 0, i = 0, j = 0, k = null, e === !0 && (e = !1, d = setTimeout(function() {
						a.parent().removeClass(tdMenu._openMenuClass), a.parent().children("ul").hide(), c = {}
					}, 400)), b.on("mousemove", function(f) {
						h >= g ? (h = 0, k = tdMenu._getMouseAngleDirection(i, j, f.pageX, f.pageY), i = f.pageX, j = f.pageY) : (h++, 0 === i && 0 === j && (i = f.pageX, j = f.pageY)), null !== k && (k > 85 || k < -85) && (a.parent().removeClass(tdMenu._openMenuClass), a.parent().children("ul").hide(), b.off("mousemove"), clearTimeout(d), e = !0, c = {})
					})) : (c = b.parent(), f = b.siblings(), f.removeClass(tdMenu._openMenuClass), f.find("ul").hide(), f.find("li").removeClass(tdMenu._openMenuClass))
				}, function() {
					var a = jQuery(this);
					e === !1 && (clearTimeout(d), e = !0), a.off("mousemove")
				})
			}
		},
		unsetHover: function() {
			null !== tdMenu._itemsWithSubmenu && tdMenu._itemsWithSubmenu.off(), null !== tdMenu._outsideClickArea && tdMenu._outsideClickArea.off()
		}
	}
}(), tdMenu.init();
var tdUtil = {};
! function() {
	"use strict";
	tdUtil = {
		email_pattern: /^[a-zA-Z0-9][a-zA-Z0-9_\.-]{0,}[a-zA-Z0-9]@[a-zA-Z0-9][a-zA-Z0-9_\.-]{0,}[a-z0-9][\.][a-z0-9]{2,4}$/,
		stopBubble: function(a) {
			a && a.stopPropagation ? a.stopPropagation() : window.event.cancelBubble = !0
		},
		isEmail: function(a) {
			return tdUtil.email_pattern.test(a)
		},
		imageMoveClassToFigure: function(a) {
			jQuery("figure ." + a).each(function() {
				jQuery(this).parents("figure:first").addClass(a), jQuery(this).removeClass(a)
			})
		},
		getBackendVar: function(a) {
			return "undefined" == typeof window[a] ? "" : window[a]
		},
		isUndefined: function(a) {
			return void 0 === a
		},
		scrollToElement: function(a, b) {
			tdIsScrollingAnimation = !0, jQuery("html, body").stop();
			var c;
			c = a.offset().top > jQuery(document).height() - jQuery(window).height() ? jQuery(document).height() - jQuery(window).height() : a.offset().top, jQuery("html, body").animate({
				scrollTop: c
			}, {
				duration: b,
				easing: "easeInOutQuart",
				complete: function() {
					tdIsScrollingAnimation = !1
				}
			})
		},
		scrollIntoView: function(a) {
			if (tdIsScrollingAnimation = !0, tdDetect.isMobileDevice !== !0) {
				jQuery("html, body").stop();
				var b = a.offset().top;
				b -= 150;
				var c = Math.abs(jQuery(window).scrollTop() - b),
					d = c / 5;
				jQuery("html, body").animate({
					scrollTop: b
				}, {
					duration: 1100 + d,
					easing: "easeInOutQuart",
					complete: function() {
						tdIsScrollingAnimation = !1
					}
				})
			}
		},
		scrollToPosition: function(a, b) {
			tdIsScrollingAnimation = !0, jQuery("html, body").stop(), jQuery("html, body").animate({
				scrollTop: a
			}, {
				duration: b,
				easing: "easeInOutQuart",
				complete: function() {
					tdIsScrollingAnimation = !1
				}
			})
		},
		tdMoveY: function(a, b) {
			var c = "translate3d(0px," + b + "px, 0px)";
			a.style["-webkit-transform"] = c, a.style["-moz-transform"] = c, a.style["-ms-transform"] = c, a.style["-o-transform"] = c, a.style.transform = c
		},
		isValidUrl: function(a) {
			var b = new RegExp("^(https?:\\/\\/)?((([a-z\\d]([a-z\\d-]*[a-z\\d])*)\\.)+[a-z]{2,}|((\\d{1,3}\\.){3}\\d{1,3}))(\\:\\d+)?(\\/[-a-z\\d%_.~+]*)*(\\?[;&a-z\\d%_.~+=-]*)?(\\#[-a-z\\d_]*)?$", "i");
			return !!b.test(a)
		},
		round: function(a, b, c) {
			var d, e, f, g;
			if (b |= 0, d = Math.pow(10, b), a *= d, g = a > 0 | -(a < 0), f = a % 1 === .5 * g, e = Math.floor(a), f) switch (c) {
				case "PHP_ROUND_HALF_DOWN":
					a = e + (g < 0);
					break;
				case "PHP_ROUND_HALF_EVEN":
					a = e + e % 2 * g;
					break;
				case "PHP_ROUND_HALF_ODD":
					a = e + !(e % 2);
					break;
				default:
					a = e + (g > 0)
			}
			return (f ? a : Math.round(a)) / d
		}
	}
}();
var tdAffix = {};
! function() {
	"use strict";
	tdAffix = {
		allow_scroll: !0,
		menu_selector: "",
		menu_wrap_selector: "",
		tds_snap_menu: "",
		tds_snap_menu_logo: "",
		is_menu_affix_height_computed: !1,
		is_menu_affix_height_on_mobile_computed: !1,
		menu_affix_height: 0,
		menu_affix_height_on_mobile: 0,
		main_menu_height: 0,
		top_offset: 0,
		menu_offset: 0,
		is_requestAnimationFrame_running: !1,
		is_menu_affix: !1,
		is_top_menu: !1,
		menu_offset_max_hit: !1,
		menu_offset_min_hit: !0,
		scroll_window_scrollTop_last: 0,
		init: function(a) {
			tdAffix.menu_selector = a.menu_selector, tdAffix.menu_wrap_selector = a.menu_wrap_selector, tdAffix.tds_snap_menu = a.tds_snap_menu, tdAffix.tds_snap_menu_logo = a.tds_snap_menu_logo, tdAffix.menu_affix_height = a.menu_affix_height, tdAffix.menu_affix_height_on_mobile = a.menu_affix_height_on_mobile, tdAffix.tds_snap_menu && (tdDetect.isFirefox && (tdAffix.compute_wrapper(), tdAffix.compute_top()), jQuery().ready(function() {
				tdAffix.compute_wrapper(), tdAffix.compute_top()
			}), jQuery(window).load(function() {
				tdAffix.compute_wrapper(), tdAffix.compute_top(), setTimeout(function() {
					tdAffix.compute_top()
				}, 1e3)
			}))
		},
		_get_menu_affix_height: function() {
			return !0 === tdDetect.isPhoneScreen ? (!tdAffix.is_menu_affix_height_on_mobile_computed && tdAffix.is_menu_affix && (tdAffix.is_menu_affix_height_on_mobile_computed = !0, tdAffix.menu_affix_height_on_mobile = jQuery(tdAffix.menu_selector).height()), tdAffix.menu_affix_height_on_mobile) : (!tdAffix.is_menu_affix_height_computed && tdAffix.is_menu_affix && (tdAffix.is_menu_affix_height_computed = !0, tdAffix.menu_affix_height = jQuery(tdAffix.menu_selector).height()), tdAffix.menu_affix_height)
		},
		td_events_scroll: function(a) {
			if (tdAffix.allow_scroll && tdAffix.tds_snap_menu) {
				var b = "";
				if ("snap" !== tdAffix.tds_snap_menu && ("smart_snap_mobile" !== tdAffix.tds_snap_menu || !0 === tdDetect.isPhoneScreen)) {
					var c = 0;
					a !== tdAffix.scroll_window_scrollTop_last && (b = a > tdAffix.scroll_window_scrollTop_last ? "down" : "up", c = Math.abs(a - tdAffix.scroll_window_scrollTop_last)), tdAffix.scroll_window_scrollTop_last = a
				}
				if ("" !== tdAffix.tds_snap_menu && "" !== tdAffix.tds_snap_menu_logo && jQuery(".td-main-menu-logo").addClass("td-logo-sticky"), a > tdAffix.top_offset + (tdAffix.main_menu_height / 2 - tdAffix._get_menu_affix_height() / 2) || tdAffix.is_menu_affix === !0 && "smart_snap_always" === tdAffix.tds_snap_menu && a > tdAffix.top_offset - tdAffix._get_menu_affix_height() || tdAffix.is_top_menu === !0) {
					var d = jQuery(tdAffix.menu_selector);
					if (tdAffix._affix_on(d), "snap" === tdAffix.tds_snap_menu || "smart_snap_mobile" === tdAffix.tds_snap_menu && !1 === tdDetect.isPhoneScreen) return;
					(!1 === tdAffix.menu_offset_max_hit && "down" === b || !1 === tdAffix.menu_offset_min_hit && "up" === b) && window.requestAnimationFrame(function() {
						var e = 0;
						a > 0 && ("down" === b ? (e = tdAffix.menu_offset - c, e < -tdAffix._get_menu_affix_height() && (e = -tdAffix._get_menu_affix_height())) : "up" === b && (e = tdAffix.menu_offset + c, e > 0 && (e = 0))), tdUtil.tdMoveY(d[0], e), 0 === e ? tdAffix.menu_offset_min_hit = !0 : tdAffix.menu_offset_min_hit = !1, e === -tdAffix._get_menu_affix_height() ? (tdAffix.menu_offset_max_hit = !0, (!0 === tdDetect.isIos || tdDetect.isSafari) && d.hide(), "" !== tdAffix.tds_snap_menu_logo && jQuery(".td-main-menu-logo").addClass("td-logo-sticky")) : (tdAffix.menu_offset_max_hit = !1, (!0 === tdDetect.isIos || tdDetect.isSafari) && d.show()), tdAffix.menu_offset = e
					}, d[0])
				} else tdAffix._affix_off(jQuery(tdAffix.menu_selector))
			}
		},
		compute_top: function() {
			tdAffix.top_offset = jQuery(tdAffix.menu_wrap_selector).offset().top, "smart_snap_always" === tdAffix.tds_snap_menu && (tdAffix.top_offset += tdAffix.menu_affix_height), 1 === tdAffix.top_offset ? tdAffix.is_top_menu = !0 : tdAffix.is_top_menu = !1, tdAffix.td_events_scroll(jQuery(window).scrollTop())
		},
		compute_wrapper: function() {
			jQuery(tdAffix.menu_selector).hasClass("td-affix") ? (jQuery(tdAffix.menu_selector).removeClass("td-affix"), tdAffix.main_menu_height = jQuery(tdAffix.menu_selector).height(), jQuery(tdAffix.menu_selector).addClass("td-affix")) : tdAffix.main_menu_height = jQuery(tdAffix.menu_selector).height(), jQuery(tdAffix.menu_wrap_selector).css("height", tdAffix.main_menu_height)
		},
		_affix_on: function(a) {
			!1 === tdAffix.is_menu_affix ? ("smart_snap_always" === tdAffix.tds_snap_menu && tdDetect.isPhoneScreen !== !0 && a.css("visibility", "hidden"), tdAffix.menu_offset = -tdAffix.top_offset, a.addClass("td-affix"), jQuery("body").addClass("body-td-affix"), tdAffix.is_menu_affix = !0) : !0 !== tdDetect.isPhoneScreen && a.css("visibility", "")
		},
		_affix_off: function(a) {
			!0 === tdAffix.is_menu_affix && (jQuery(tdAffix.menu_selector).removeClass("td-affix"), "" === tdAffix.tds_snap_menu_logo && jQuery(".td-main-menu-logo").removeClass("td-logo-sticky"), jQuery("body").removeClass("body-td-affix"), tdAffix.is_menu_affix = !1, tdUtil.tdMoveY(a[0], 0), (!0 === tdDetect.isIos || tdDetect.isSafari) && a.show())
		}
	}
}(), jQuery().ready(function() {
	if (td_retina(), td_mobile_menu_toogle(), td_resize_videos(), jQuery("input, textarea").placeholder(), td_more_articles_box.init(), td_smart_lists_magnific_popup(), td_smart_list_dropdown(), "undefined" != typeof tdsDateFormat) {
		var b = Math.floor((new Date).getTime() / 1e3),
			c = td_date_i18n(tdsDateFormat, b);
		jQuery(".td_data_time").text(c)
	}
	setMenuMinHeight(), td_comments_form_validation()
});
var td_more_articles_box = {
		is_box_visible: !1,
		cookie: "",
		distance_from_top: 400,
		init: function() {
			td_more_articles_box.cookie = td_read_site_cookie("td-cookie-more-articles"), !isNaN(parseInt(tds_more_articles_on_post_pages_distance_from_top)) && isFinite(tds_more_articles_on_post_pages_distance_from_top) && parseInt(tds_more_articles_on_post_pages_distance_from_top) > 0 ? td_more_articles_box.distance_from_top = parseInt(tds_more_articles_on_post_pages_distance_from_top) : td_more_articles_box.distance_from_top = 400, jQuery(".td-close-more-articles-box").click(function() {
				jQuery(".td-more-articles-box").removeClass("td-front-end-display-block"), jQuery(".td-more-articles-box").hide(), !isNaN(parseInt(tds_more_articles_on_post_time_to_wait)) && isFinite(tds_more_articles_on_post_time_to_wait) && td_set_cookies_life(["td-cookie-more-articles", "hide-more-articles-box", 864e5 * parseInt(tds_more_articles_on_post_time_to_wait)])
			})
		},
		td_events_scroll: function(b) {
			tdIsScrollingAnimation || "show" == tdUtil.getBackendVar("tds_more_articles_on_post_enable") && "hide-more-articles-box" != td_more_articles_box.cookie && (b > td_more_articles_box.distance_from_top ? td_more_articles_box.is_box_visible === !1 && (jQuery(".td-more-articles-box").addClass("td-front-end-display-block"), td_more_articles_box.is_box_visible = !0) : td_more_articles_box.is_box_visible === !0 && (jQuery(".td-more-articles-box").removeClass("td-front-end-display-block"), td_more_articles_box.is_box_visible = !1))
		}
	},
	td_resize_timer_id;
jQuery(window).resize(function() {
	clearTimeout(td_resize_timer_id), td_resize_timer_id = setTimeout(function() {
		td_done_resizing()
	}, 200)
}), tdDetect.isTouchDevice || "" == tdUtil.getBackendVar("td_ad_background_click_link") || jQuery("body").click(function(a) {
	var b = a.target ? a.target : a.srcElement,
		c = jQuery(b);
	(c.hasClass("td-outer-container") || c.hasClass("td-boxed-layout")) && ("_blank" == td_ad_background_click_target ? window.open(td_ad_background_click_link) : location.href = td_ad_background_click_link)
});
var tdIsScrollingAnimation = !1,
	td_mouse_wheel_or_touch_moved = !1;
jQuery(document).bind("mousewheel DOMMouseScroll MozMousePixelScroll", function(a) {
	tdIsScrollingAnimation !== !1 && (tdIsScrollingAnimation = !1, td_mouse_wheel_or_touch_moved = !0, jQuery("html, body").stop())
}), document.addEventListener && document.addEventListener("touchmove", function(a) {
	tdIsScrollingAnimation !== !1 && (tdIsScrollingAnimation = !1, td_mouse_wheel_or_touch_moved = !0, jQuery("html, body").stop())
}, !1);
var td_scroll_to_top_is_visible = !1;
jQuery(".td-scroll-up").click(function() {
	if (!tdIsScrollingAnimation) return td_scroll_to_top_is_visible = !1, jQuery(".td-scroll-up").removeClass("td-scroll-up-visible"), td_more_articles_box.is_box_visible = !1, jQuery(".td-more-articles-box").removeClass("td-front-end-display-block"), tdUtil.scrollToPosition(0, 1200), !1
}), jQuery(".td-read-down a").click(function(a) {
	a.preventDefault(), tdUtil.scrollToPosition(jQuery(".td-full-screen-header-image-wrap").height(), 1200)
});
var tdLoadingBox = {};
! function() {
	"use strict";
	tdLoadingBox = {
		speed: 40,
		arrayColorsTemp: ["rgba(99, 99, 99, 0)", "rgba(99, 99, 99, 0.05)", "rgba(99, 99, 99, 0.08)", "rgba(99, 99, 99, 0.2)", "rgba(99, 99, 99, 0.3)", "rgba(99, 99, 99, 0.5)", "rgba(99, 99, 99, 0.6)", "rgba(99, 99, 99, 1)"],
		arrayColors: [],
		statusAnimation: "stop",
		stop: function() {
			tdLoadingBox.statusAnimation = "stop"
		},
		init: function(b, c) {
			!1 === tdUtil.isUndefined(c) && (tdLoadingBox.speed = c);
			var d = /^#[a-zA-Z0-9]{3,6}$/;
			if (b && d.test(b)) {
				var e = tdLoadingBox.hexToRgb(b),
					f = "rgba(" + e.r + ", " + e.g + ", " + e.b + ", ";
				tdLoadingBox.arrayColors[7] = f + " 0.9)", tdLoadingBox.arrayColors[6] = f + " 0.7)", tdLoadingBox.arrayColors[5] = f + " 0.5)", tdLoadingBox.arrayColors[4] = f + " 0.3)", tdLoadingBox.arrayColors[3] = f + " 0.15)", tdLoadingBox.arrayColors[2] = f + " 0.15)", tdLoadingBox.arrayColors[1] = f + " 0.15)", tdLoadingBox.arrayColors[0] = f + " 0.15)"
			} else tdLoadingBox.arrayColors = tdLoadingBox.arrayColorsTemp.slice(0);
			"stop" === tdLoadingBox.statusAnimation && (tdLoadingBox.statusAnimation = "display", this.render())
		},
		render: function(b) {
			tdLoadingBox.animationDisplay('<div class="td-lb-box td-lb-box-1" style="background-color:' + tdLoadingBox.arrayColors[0] + '"></div><div class="td-lb-box td-lb-box-2" style="background-color:' + tdLoadingBox.arrayColors[1] + '"></div><div class="td-lb-box td-lb-box-3" style="background-color:' + tdLoadingBox.arrayColors[2] + '"></div><div class="td-lb-box td-lb-box-4" style="background-color:' + tdLoadingBox.arrayColors[3] + '"></div><div class="td-lb-box td-lb-box-5" style="background-color:' + tdLoadingBox.arrayColors[4] + '"></div><div class="td-lb-box td-lb-box-6" style="background-color:' + tdLoadingBox.arrayColors[5] + '"></div><div class="td-lb-box td-lb-box-7" style="background-color:' + tdLoadingBox.arrayColors[6] + '"></div><div class="td-lb-box td-lb-box-8" style="background-color:' + tdLoadingBox.arrayColors[7] + '"></div>');
			var c = [tdLoadingBox.arrayColors[0], tdLoadingBox.arrayColors[1], tdLoadingBox.arrayColors[2], tdLoadingBox.arrayColors[3], tdLoadingBox.arrayColors[4], tdLoadingBox.arrayColors[5], tdLoadingBox.arrayColors[6], tdLoadingBox.arrayColors[7]];
			tdLoadingBox.arrayColors[0] = c[7], tdLoadingBox.arrayColors[1] = c[0], tdLoadingBox.arrayColors[2] = c[1], tdLoadingBox.arrayColors[3] = c[2], tdLoadingBox.arrayColors[4] = c[3], tdLoadingBox.arrayColors[5] = c[4], tdLoadingBox.arrayColors[6] = c[5], tdLoadingBox.arrayColors[7] = c[6], "display" === tdLoadingBox.statusAnimation ? setTimeout(tdLoadingBox.render, tdLoadingBox.speed) : tdLoadingBox.animationDisplay("")
		},
		animationDisplay: function(a) {
			jQuery(".td-loader-gif").html(a)
		},
		hexToRgb: function(a) {
			var b = /^#?([a-f\d]{2})([a-f\d]{2})([a-f\d]{2})$/i.exec(a);
			return b ? {
				r: parseInt(b[1], 16),
				g: parseInt(b[2], 16),
				b: parseInt(b[3], 16)
			} : null
		}
	}
}();
var tdAjaxSearch = {};
jQuery().ready(function() {
		"use strict";
		tdAjaxSearch.init()
	}),
	function() {
		"use strict";
		tdAjaxSearch = {
			_current_selection_index: 0,
			_last_request_results_count: 0,
			_first_down_up: !0,
			_is_search_open: !1,
			init: function() {
				jQuery(document).click(function(a) {
					"td-icon-search" !== a.target.className && "td-header-search" !== a.target.id && "td-header-search-top" !== a.target.id && !0 === tdAjaxSearch._is_search_open && tdAjaxSearch.hide_search_box()
				}), jQuery("#td-header-search-button").click(function(a) {
					a.preventDefault(), tdAjaxSearch._is_search_open === !0 ? tdAjaxSearch.hide_search_box() : tdAjaxSearch.show_search_box();
				}), jQuery("#td-header-search-button-mob").click(function(a) {
					jQuery("body").addClass("td-search-opened");
					var b = jQuery("#td-header-search-mob");
					setTimeout(function() {
						b.focus()
					}, 1300);
					var c = b.val().trim();
					c.length > 0 && tdAjaxSearch.do_ajax_call_mob()
				}), jQuery(".td-search-close a").click(function() {
					jQuery("body").removeClass("td-search-opened")
				}), jQuery("#td-header-search").keydown(function(a) {
					if (a.which && 39 === a.which || a.keyCode && 39 === a.keyCode || a.which && 37 === a.which || a.keyCode && 37 === a.keyCode) return void tdAjaxSearch.td_aj_search_input_focus();
					if (a.which && 13 === a.which || a.keyCode && 13 === a.keyCode) {
						var b = jQuery(".td-aj-cur-element");
						if (b.length > 0) {
							var c = b.find(".entry-title a").attr("href");
							window.location = c
						} else jQuery(this).parent().parent().submit();
						return !1
					}
					if (a.which && 40 === a.which || a.keyCode && 40 === a.keyCode) return tdAjaxSearch.move_prompt_down(), !1;
					if (a.which && 38 === a.which || a.keyCode && 38 === a.keyCode) return tdAjaxSearch.move_prompt_up(), !1;
					if (a.which && 8 === a.which || a.keyCode && 8 === a.keyCode) {
						var d = jQuery(this).val();
						1 === d.length && jQuery("#td-aj-search").empty()
					}
					return tdAjaxSearch.td_aj_search_input_focus(), setTimeout(function() {
						tdAjaxSearch.do_ajax_call()
					}, 100), !0
				}), jQuery("#td-header-search-mob").keydown(function(a) {
					if (a.which && 13 === a.which || a.keyCode && 13 === a.keyCode) {
						var b = jQuery(".td-aj-cur-element");
						if (b.length > 0) {
							var c = b.find(".entry-title a").attr("href");
							window.location = c
						} else jQuery(this).parent().parent().submit();
						return !1
					}
					if (a.which && 8 === a.which || a.keyCode && 8 === a.keyCode) {
						var d = jQuery(this).val();
						1 === d.length && jQuery("#td-aj-search-mob").empty()
					}
					return setTimeout(function() {
						tdAjaxSearch.do_ajax_call_mob()
					}, 100), !0
				})
			},
			show_search_box: function() {
				jQuery(".td-drop-down-search").addClass("td-drop-down-search-open"), !0 !== tdDetect.isIos && setTimeout(function() {
					document.getElementById("td-header-search").focus()
				}, 200), tdAjaxSearch._is_search_open = !0
			},
			hide_search_box: function() {
				jQuery(".td-drop-down-search").removeClass("td-drop-down-search-open"), tdAjaxSearch._is_search_open = !1
			},
			td_aj_search_move_prompt_up: function() {
				tdAjaxSearch._first_down_up === !0 ? (tdAjaxSearch._first_down_up = !1, 0 === tdAjaxSearch._current_selection_index ? tdAjaxSearch._current_selection_index = tdAjaxSearch._last_request_results_count - 1 : tdAjaxSearch._current_selection_index--) : 0 === tdAjaxSearch._current_selection_index ? tdAjaxSearch._current_selection_index = tdAjaxSearch._last_request_results_count : tdAjaxSearch._current_selection_index--, tdAjaxSearch._repaintCurrentElement()
			},
			move_prompt_down: function() {
				tdAjaxSearch._first_down_up === !0 ? tdAjaxSearch._first_down_up = !1 : tdAjaxSearch._current_selection_index === tdAjaxSearch._last_request_results_count ? tdAjaxSearch._current_selection_index = 0 : tdAjaxSearch._current_selection_index++, tdAjaxSearch._repaintCurrentElement()
			},
			_repaintCurrentElement: function() {
				jQuery(".td_module_wrap").removeClass("td-aj-cur-element"), tdAjaxSearch._current_selection_index > tdAjaxSearch._last_request_results_count - 1 ? jQuery(".td-search-form").fadeTo(100, 1) : (tdAjaxSearch.td_aj_search_input_remove_focus(), jQuery(".td_module_wrap").eq(tdAjaxSearch._current_selection_index).addClass("td-aj-cur-element"))
			},
			td_aj_search_input_focus: function() {
				tdAjaxSearch._current_selection_index = 0, tdAjaxSearch._first_down_up = !0, jQuery(".td-search-form").fadeTo(100, 1), jQuery(".td_module_wrap").removeClass("td-aj-cur-element")
			},
			td_aj_search_input_remove_focus: function() {
				0 !== tdAjaxSearch._last_request_results_count && jQuery(".td-search-form").css("opacity", .5)
			},
			process_ajax_response: function(a) {
				var b = jQuery("#td-header-search").val();
				if ("" === b) return void jQuery("#td-aj-search").empty();
				var c = jQuery.parseJSON(a);
				c.td_search_query === b && (tdAjaxSearch._current_selection_index = 0, tdAjaxSearch._last_request_results_count = c.td_total_in_list, tdAjaxSearch._first_down_up = !0, jQuery("#td-aj-search").html(c.td_data), "undefined" != typeof window.tdAnimationStack && !0 === window.tdAnimationStack.activated && (window.tdAnimationStack.check_for_new_items("#td-aj-search .td-animation-stack", window.tdAnimationStack.SORTED_METHOD.sort_left_to_right, !0), window.tdAnimationStack.compute_items()))
			},
			process_ajax_response_mob: function(a) {
				var b = jQuery("#td-header-search-mob").val();
				if ("" === b) return void jQuery("#td-aj-search-mob").empty();
				var c = jQuery.parseJSON(a);
				c.td_search_query === b && (jQuery("#td-aj-search-mob").html(c.td_data), "undefined" != typeof window.tdAnimationStack && !0 === window.tdAnimationStack.activated && (window.tdAnimationStack.check_for_new_items("#td-aj-search-mob .td-animation-stack", window.tdAnimationStack.SORTED_METHOD.sort_left_to_right, !0), window.tdAnimationStack.compute_items()))
			},
			do_ajax_call: function() {
				var a = jQuery("#td-header-search").val();
				return "" === a ? void tdAjaxSearch.td_aj_search_input_focus() : tdLocalCache.exist(a) ? void tdAjaxSearch.process_ajax_response(tdLocalCache.get(a)) : void jQuery.ajax({
					type: "POST",
					url: td_ajax_url,
					data: {
						action: "td_ajax_search",
						td_string: a
					},
					success: function(b, c, d) {
						tdLocalCache.set(a, b), tdAjaxSearch.process_ajax_response(b)
					},
					error: function(a, b, c) {}
				})
			},
			do_ajax_call_mob: function() {
				var a = jQuery("#td-header-search-mob").val();
				if ("" !== a) return tdLocalCache.exist(a) ? void tdAjaxSearch.process_ajax_response_mob(tdLocalCache.get(a)) : void jQuery.ajax({
					type: "POST",
					url: td_ajax_url,
					data: {
						action: "td_ajax_search",
						td_string: a
					},
					success: function(b, c, d) {
						tdLocalCache.set(a, b), tdAjaxSearch.process_ajax_response_mob(b)
					},
					error: function(a, b, c) {}
				})
			}
		}
	}(), jQuery().ready(function() {
		tdModalImage(), tdUtil.imageMoveClassToFigure("td-post-image-full"), tdUtil.imageMoveClassToFigure("td-post-image-right"), tdUtil.imageMoveClassToFigure("td-post-image-left"), "undefined" != typeof window.tds_general_modal_image && "" !== window.tds_general_modal_image && jQuery(".single .td-post-content a > img").filter(function(a, b) {
			-1 !== b.className.indexOf("wp-image") && jQuery(b).parent().addClass("td-modal-image")
		})
	});

	jQuery().ready(function() {
	"use strict";
	var a = {
		type: "inline",
		preloader: !1,
		focus: "#name",
		removalDelay: 500,
		callbacks: {
			beforeOpen: function() {
				this.st.mainClass = this.st.el.attr("data-effect"), tdLogin.clearFields(), tdLogin.showHideMsg(), jQuery(window).width() < 700 ? this.st.focus = !1 : !1 === tdDetect.isIe && (this.st.focus = "#login_email")
			},
			beforeClose: function() {}
		},
		disableOn: function() {
			return !(jQuery(window).width() < 750)
		}
	};
	void 0 !== window.tds_login_sing_in_widget && (jQuery(".comment-reply-login").attr({
		href: "#login-form",
		"data-effect": "mpf-td-login-effect"
	}), jQuery(".comment-reply-login, .td-login-modal-js").magnificPopup(a)), jQuery(".td-login-modal-js, .comment-reply-login").on("click", function(a) {
		jQuery(window).width() < 750 && void 0 !== window.tds_login_sing_in_widget && (a.preventDefault(), jQuery("body").addClass("td-menu-mob-open-menu"), jQuery(".td-mobile-container").hide(), jQuery("#td-mobile-nav").addClass("td-hide-menu-content"), setTimeout(function() {
			jQuery(".td-mobile-container").show()
		}, 500), tdLogin.showHideElementsMobile([
			["#td-login-mob", 1],
			["#td-register-mob", 0],
			["#td-forgot-pass-mob", 0]
		]))
	}), jQuery("#login-link").on("click", function() {
		tdLogin.showHideElements([
			["#td-login-div", 1],
			["#td-register-div", 0],
			["#td-forgot-pass-div", 0]
		]), jQuery("#login-form").addClass("td-login-animation"), jQuery(window).width() > 700 && tdDetect.isIe === !1 && jQuery("#login_email").focus(), tdLogin.showHideMsg()
	}), jQuery("#register-link").on("click", function() {
		tdLogin.showHideElements([
			["#td-login-div", 0],
			["#td-register-div", 1],
			["#td-forgot-pass-div", 0]
		]), jQuery("#login-form").addClass("td-login-animation"), jQuery(window).width() > 700 && !1 === tdDetect.isIe && jQuery("#register_email").focus(), tdLogin.showHideMsg()
	}), jQuery("#forgot-pass-link").on("click", function(a) {
		a.preventDefault(), tdLogin.showHideElements([
			["#td-login-div", 0],
			["#td-register-div", 0],
			["#td-forgot-pass-div", 1]
		]), jQuery("#login-form").addClass("td-login-animation"), jQuery(window).width() > 700 && !1 === tdDetect.isIe && jQuery("#forgot_email").focus(), tdLogin.showHideMsg()
	}), jQuery("#login_button").on("click", function() {
		tdLogin.handlerLogin()
	}), jQuery("#login_pass").keydown(function(a) {
		(a.which && 13 === a.which || a.keyCode && 13 === a.keyCode) && tdLogin.handlerLogin()
	}), jQuery("#register_button").on("click", function() {
		tdLogin.handlerRegister()
	}), jQuery("#register_user").keydown(function(a) {
		(a.which && 13 === a.which || a.keyCode && 13 === a.keyCode) && tdLogin.handlerRegister()
	}), jQuery("#forgot_button").on("click", function() {
		tdLogin.handlerForgotPass()
	}), jQuery("#forgot_email").keydown(function(a) {
		(a.which && 13 === a.which || a.keyCode && 13 === a.keyCode) && tdLogin.handlerForgotPass()
	}), jQuery(".td-back-button").on("click", function() {
		tdLogin.showHideElements([
			["#td-login-div", 1],
			["#td-register-div", 0],
			["#td-forgot-pass-div", 0]
		]), jQuery("#login-form").removeClass("td-login-animation"), jQuery(".td_display_err").hide()
	})
});
var tdLogin = {};
! function() {
	"use strict";
	tdLogin = {
		email_pattern: /^[a-zA-Z0-9][a-zA-Z0-9_\.-]{0,}[a-zA-Z0-9]@[a-zA-Z0-9][a-zA-Z0-9_\.-]{0,}[a-z0-9][\.][a-z0-9]{2,4}$/,
		handlerLogin: function() {
			var a = jQuery("#login_email"),
				b = jQuery("#login_pass");
			if (a.length && b.length) {
				var c = a.val().trim(),
					d = b.val().trim();
				c && d ? (tdLogin.addRemoveClass([".td_display_err", 1, "td_display_msg_ok"]), tdLogin.showHideMsg(td_please_wait), tdLogin.doAction("td_mod_login", c, "", d)) : tdLogin.showHideMsg(td_email_user_pass_incorrect)
			}
		},
		handlerRegister: function() {
			var a = jQuery("#register_email"),
				b = jQuery("#register_user");
			if (a.length && b.length) {
				var c = a.val().trim(),
					d = b.val().trim();
				tdLogin.email_pattern.test(c) && d ? (tdLogin.addRemoveClass([".td_display_err", 1, "td_display_msg_ok"]), tdLogin.showHideMsg(td_please_wait), tdLogin.doAction("td_mod_register", c, d, "")) : tdLogin.showHideMsg(td_email_user_incorrect)
			}
		},
		handlerForgotPass: function() {
			var a = jQuery("#forgot_email");
			if (a.length) {
				var b = a.val().trim();
				tdLogin.email_pattern.test(b) ? (tdLogin.addRemoveClass([".td_display_err", 1, "td_display_msg_ok"]), tdLogin.showHideMsg(td_please_wait), tdLogin.doAction("td_mod_remember_pass", b, "", "")) : tdLogin.showHideMsg(td_email_incorrect)
			}
		},
		showHideElements: function(a) {
			if (a.constructor === Array)
				for (var b = a.length, c = 0; c < b; c++)
					if (a[c].constructor === Array && 2 === a[c].length) {
						var d = jQuery(a[c][0]);
						d.length && (1 === a[c][1] ? d.removeClass("td-display-none").addClass("td-display-block") : d.removeClass("td-display-block").addClass("td-display-none"))
					}
		},
		showHideElementsMobile: function(a) {
			if (a.constructor === Array)
				for (var b = a.length, c = 0; c < b; c++)
					if (a[c].constructor === Array && 2 === a[c].length) {
						var d = jQuery(a[c][0]);
						d.length && (1 === a[c][1] ? d.removeClass("td-login-hide").addClass("td-login-show") : d.removeClass("td-login-show").addClass("td-login-hide"))
					}
		},
		showTabs: function(a) {
			if (a.constructor === Array)
				for (var b = a.length, c = 0; c < b; c++)
					if (a[c].constructor === Array && 2 === a[c].length) {
						var d = jQuery(a[c][0]);
						d.length && (1 === a[c][1] ? d.addClass("td_login_tab_focus") : d.removeClass("td_login_tab_focus"))
					}
		},
		addRemoveClass: function(a) {
			if (a.constructor === Array && 3 === a.length) {
				var b = jQuery(a[0]);
				b.length && (1 === a[1] ? b.addClass(a[2]) : b.removeClass(a[2]))
			}
		},
		showHideMsg: function(a) {
			var b = jQuery(".td_display_err");
			b.length && (void 0 !== a && a.constructor === String && a.length > 0 ? (b.show(), b.html(a)) : (b.hide(), b.html("")))
		},
		clearFields: function() {
			jQuery("#login_email").val(""), jQuery("#login_pass").val(""), jQuery("#register_email").val(""), jQuery("#register_user").val(""), jQuery("#forgot_email").val("")
		},
		doAction: function(a, b, c, d) {
			jQuery.ajax({
				type: "POST",
				url: td_ajax_url,
				data: {
					action: a,
					email: b,
					user: c,
					pass: d
				},
				success: function(a, b, c) {
					var d = jQuery.parseJSON(a);
					switch (d[0]) {
						case "login":
							1 === d[1] ? location.reload(!0) : (tdLogin.addRemoveClass([".td_display_err", 0, "td_display_msg_ok"]), tdLogin.showHideMsg(d[2]));
							break;
						case "register":
							1 === d[1] ? tdLogin.addRemoveClass([".td_display_err", 1, "td_display_msg_ok"]) : tdLogin.addRemoveClass([".td_display_err", 0, "td_display_msg_ok"]), tdLogin.showHideMsg(d[2]);
							break;
						case "remember_pass":
							1 === d[1] ? tdLogin.addRemoveClass([".td_display_err", 1, "td_display_msg_ok"]) : tdLogin.addRemoveClass([".td_display_err", 0, "td_display_msg_ok"]), tdLogin.showHideMsg(d[2])
					}
				},
				error: function(a, b, c) {}
			})
		}
	}
}(), jQuery().ready(function() {
	"use strict";
	jQuery("#login-link-mob").on("click", function() {
		tdLoginMob.showHideElements([
			["#td-login-mob", 1],
			["#td-register-mob", 0],
			["#td-forgot-pass-mob", 0]
		]), jQuery("#td-mobile-nav").addClass("td-hide-menu-content"), jQuery(window).width() > 700 && tdDetect.isIe === !1 && jQuery("#login_email-mob").focus(), tdLoginMob.showHideMsg()
	}), jQuery("#register-link-mob").on("click", function() {
		tdLoginMob.showHideElements([
			["#td-login-mob", 0],
			["#td-register-mob", 1],
			["#td-forgot-pass-mob", 0]
		]), jQuery("#td-mobile-nav").addClass("td-hide-menu-content"), jQuery(window).width() > 700 && !1 === tdDetect.isIe && jQuery("#register_email-mob").focus(), tdLoginMob.showHideMsg()
	}), jQuery("#forgot-pass-link-mob").on("click", function() {
		tdLoginMob.showHideElements([
			["#td-login-mob", 0],
			["#td-register-mob", 0],
			["#td-forgot-pass-mob", 1]
		]), jQuery(window).width() > 700 && !1 === tdDetect.isIe && jQuery("#forgot_email-mob").focus(), tdLoginMob.showHideMsg()
	}), jQuery("#login_button-mob").on("click", function() {
		tdLoginMob.handlerLogin()
	}), jQuery("#login_pass-mob").keydown(function(a) {
		(a.which && 13 === a.which || a.keyCode && 13 === a.keyCode) && tdLoginMob.handlerLogin()
	}), jQuery("#register_button-mob").on("click", function() {
		tdLoginMob.handlerRegister()
	}), jQuery("#register_user-mob").keydown(function(a) {
		(a.which && 13 === a.which || a.keyCode && 13 === a.keyCode) && tdLoginMob.handlerRegister()
	}), jQuery("#forgot_button-mob").on("click", function() {
		tdLoginMob.handlerForgotPass()
	}), jQuery("#forgot_email-mob").keydown(function(a) {
		(a.which && 13 === a.which || a.keyCode && 13 === a.keyCode) && tdLoginMob.handlerForgotPass()
	}), jQuery("#td-mobile-nav .td-login-close a, #td-mobile-nav .td-register-close a").on("click", function() {
		tdLoginMob.showHideElements([
			["#td-login-mob", 0],
			["#td-register-mob", 0],
			["#td-forgot-pass-mob", 0]
		]), jQuery("#td-mobile-nav").removeClass("td-hide-menu-content")
	}), jQuery("#td-mobile-nav .td-forgot-pass-close a").on("click", function() {
		tdLoginMob.showHideElements([
			["#td-login-mob", 1],
			["#td-register-mob", 0],
			["#td-forgot-pass-mob", 0]
		])
	})
});
var tdLoginMob = {};
! function() {
	"use strict";
	tdLoginMob = {
		email_pattern: /^[a-zA-Z0-9][a-zA-Z0-9_\.-]{0,}[a-zA-Z0-9]@[a-zA-Z0-9][a-zA-Z0-9_\.-]{0,}[a-z0-9][\.][a-z0-9]{2,4}$/,
		handlerLogin: function() {
			var a = jQuery("#login_email-mob"),
				b = jQuery("#login_pass-mob");
			if (a.length && b.length) {
				var c = a.val().trim(),
					d = b.val().trim();
				c && d ? (tdLoginMob.addRemoveClass([".td_display_err", 1, "td_display_msg_ok"]), tdLoginMob.showHideMsg(td_please_wait), tdLoginMob.doAction("td_mod_login", c, "", d)) : tdLoginMob.showHideMsg(td_email_user_pass_incorrect)
			}
		},
		handlerRegister: function() {
			var a = jQuery("#register_email-mob"),
				b = jQuery("#register_user-mob");
			if (a.length && b.length) {
				var c = a.val().trim(),
					d = b.val().trim();
				tdLoginMob.email_pattern.test(c) && d ? (tdLoginMob.addRemoveClass([".td_display_err", 1, "td_display_msg_ok"]), tdLoginMob.showHideMsg(td_please_wait), tdLoginMob.doAction("td_mod_register", c, d, "")) : tdLoginMob.showHideMsg(td_email_user_incorrect)
			}
		},
		handlerForgotPass: function() {
			var a = jQuery("#forgot_email-mob");
			if (a.length) {
				var b = a.val().trim();
				tdLoginMob.email_pattern.test(b) ? (tdLoginMob.addRemoveClass([".td_display_err", 1, "td_display_msg_ok"]), tdLoginMob.showHideMsg(td_please_wait), tdLoginMob.doAction("td_mod_remember_pass", b, "", "")) : tdLoginMob.showHideMsg(td_email_incorrect)
			}
		},
		showHideElements: function(a) {
			if (a.constructor === Array)
				for (var b = a.length, c = 0; c < b; c++)
					if (a[c].constructor === Array && 2 === a[c].length) {
						var d = jQuery(a[c][0]);
						d.length && (1 === a[c][1] ? d.removeClass("td-login-hide").addClass("td-login-show") : d.removeClass("td-login-show").addClass("td-login-hide"))
					}
		},
		addRemoveClass: function(a) {
			if (a.constructor === Array && 3 === a.length) {
				var b = jQuery(a[0]);
				b.length && (1 === a[1] ? b.addClass(a[2]) : b.removeClass(a[2]))
			}
		},
		showHideMsg: function(a) {
			var b = jQuery(".td_display_err");
			b.length && (void 0 !== a && a.constructor === String && a.length > 0 ? (b.show(), b.html(a)) : (b.hide(), b.html("")))
		},
		clearFields: function() {
			jQuery("#login_email-mob").val(""), jQuery("#login_pass-mob").val(""), jQuery("#register_email-mob").val(""), jQuery("#register_user-mob").val(""), jQuery("#forgot_email-mob").val("")
		},
		doAction: function(a, b, c, d) {
			jQuery.ajax({
				type: "POST",
				url: td_ajax_url,
				data: {
					action: a,
					email: b,
					user: c,
					pass: d
				},
				success: function(a, b, c) {
					var d = jQuery.parseJSON(a);
					switch (d[0]) {
						case "login":
							1 === d[1] ? location.reload(!0) : (tdLoginMob.addRemoveClass([".td_display_err", 0, "td_display_msg_ok"]), tdLoginMob.showHideMsg(d[2]));
							break;
						case "register":
							1 === d[1] ? tdLoginMob.addRemoveClass([".td_display_err", 1, "td_display_msg_ok"]) : tdLoginMob.addRemoveClass([".td_display_err", 0, "td_display_msg_ok"]), tdLoginMob.showHideMsg(d[2]);
							break;
						case "remember_pass":
							1 === d[1] ? tdLoginMob.addRemoveClass([".td_display_err", 1, "td_display_msg_ok"]) : tdLoginMob.addRemoveClass([".td_display_err", 0, "td_display_msg_ok"]), tdLoginMob.showHideMsg(d[2])
					}
				},
				error: function(a, b, c) {}
			})
		}
	}
}();
var tdDemoMenu;
! function(a, b) {
	"use strict";
	tdDemoMenu = {
		mousePosX: 0,
		mousePosY: 0,
		startTimeout: b,
		startInterval: b,
		_extendedDemo: !1,
		_currentElement: b,
		_startExtendedTimeout: b,
		_startExtendedInterval: b,
		init: function() {
			a(document).mousemove(function(a) {
				a.pageX || a.pageY ? (tdDemoMenu.mousePosX = a.pageX, tdDemoMenu.mousePosY = a.pageY) : (a.clientX || a.clientY) && (tdDemoMenu.mousePosX = a.clientX + document.body.scrollLeft + document.documentElement.scrollLeft, tdDemoMenu.mousePosY = a.clientY + document.body.scrollTop + document.documentElement.scrollTop)
			}), a(document).mouseleave(function(c) {
				b !== tdDemoMenu.startTimeout && window.clearTimeout(tdDemoMenu.startTimeout), b !== tdDemoMenu.startInterval && window.clearInterval(tdDemoMenu.startInterval), a(".td-screen-demo:first").css("visibility", "hidden"), a(".td-screen-demo-extend:first").hide()
			}), a("#td-theme-settings").find(".td-skin-wrap:first").scroll(function(b) {
				var c = b.currentTarget,
					d = a(this).find(".td-skin-scroll:first");
				c.clientHeight + c.scrollTop < c.scrollHeight ? d.css({
					bottom: 0
				}) : d.css({
					bottom: -40
				})
			}), a("#td-theme-settings").find(".td-skin-scroll:first").click(function(b) {
				var d = (b.currentTarget, a(this).closest(".td-skin-wrap"));
				d.animate({
					scrollTop: d.scrollTop() + 200
				}, {
					duration: 800,
					easing: "easeInOutQuart"
				})
			}).mouseenter(function(c) {
				b !== tdDemoMenu.startTimeout && window.clearTimeout(tdDemoMenu.startTimeout), b !== tdDemoMenu.startInterval && window.clearInterval(tdDemoMenu.startInterval), a("#td-theme-settings").find(".td-screen-demo:first").css("visibility", "hidden")
			}), a(".td-set-theme-style-link").hover(function(c) {
				b !== tdDemoMenu.startTimeout && window.clearTimeout(tdDemoMenu.startTimeout), b !== tdDemoMenu.startInterval && window.clearInterval(tdDemoMenu.startInterval);
				var d = "td-set-theme-style",
					e = a(this),
					f = e.closest("." + d),
					g = a(".td-screen-demo:first"),
					h = 0,
					i = 0,
					j = 0,
					k = 0,
					l = 0,
					m = a("#wpadminbar"),
					n = g.find("img:first"),
					o = e.data("img-url");
				if (n.length ? n.attr("src", o) : g.html('<img src="' + o + '"/>'), 0 === a(".td-set-theme-style-link").index(this) % 2) j = 2 * f.outerWidth(!0);
				else {
					var p = f.prev("." + d);
					p.length && (j = p.outerWidth(!0) - l, k = p.outerWidth(!0) + l), c.preventDefault(), c.stopPropagation()
				}
				i = c.pageY - document.body.scrollTop - g.outerHeight(!0) / 2, i + g.outerHeight(!0) > window.innerHeight && (i -= i + g.outerHeight(!0) - window.innerHeight), h = m.length ? m.outerHeight(!0) : 0, h > i && (i = h);
				var q = {
						top: i,
						right: j,
						"padding-right": k,
						width: ""
					},
					r = g.data("width-preview");
				k > 0 && (q.width = r + k), g.css(q), g.data("right-value", j + k), g.css("visibility", "visible")
			}, function(c) {
				a(".td-screen-demo-extend:first").hide();
				var d = a(".td-screen-demo:first"),
					e = d.css("right"),
					f = d.css("padding-right"),
					g = d.css("width"),
					h = parseInt(e.replace("px", "")),
					i = parseInt(f.replace("px", "")),
					j = 10,
					k = 50,
					l = 15,
					m = parseInt(g.replace("px", "")),
					n = a(this);
				tdDemoMenu._currentElement = n;
				var o = parseInt(a("#td-theme-settings").css("width").replace("px", ""));
				i > 0 ? (b !== tdDemoMenu.startTimeout && (window.clearTimeout(tdDemoMenu.startTimeout), tdDemoMenu.startTimeout = b), b !== tdDemoMenu.startInterval && (window.clearInterval(tdDemoMenu.startInterval), tdDemoMenu.startInterval = b), tdDemoMenu.startTimeout = setTimeout(function() {
					tdDemoMenu._extendedDemo = !0, tdDemoMenu.startInterval = setInterval(function() {
						var c = d.data("width-preview");
						h += j, i -= j, m -= j;
						var e = !1;
						(i <= 0 || m < c || tdDemoMenu.mousePosX <= a(window).width() - o || tdDemoMenu.mousePosX >= a(window).width() - h) && (b !== tdDemoMenu.startTimeout && (window.clearTimeout(tdDemoMenu.startTimeout), tdDemoMenu.startTimeout = b), b !== tdDemoMenu.startInterval && (window.clearInterval(tdDemoMenu.startInterval), tdDemoMenu.startInterval = b), i = 0, h = d.data("right-value"), m = c, tdDemoMenu.mousePosX >= a(window).width() - h && (e = !0)), d.css({
							right: h,
							"padding-right": i,
							width: m
						}), e ? (tdDemoMenu._extendedDemo = !1, tdDemoMenu._checkMousePosition()) : b === tdDemoMenu.startTimeout && b === tdDemoMenu.startInterval && (tdDemoMenu._extendedDemo = !0, tdDemoMenu._showExtendedScreenDemo())
					}, l)
				}, k)) : d.css("visibility", "hidden")
			}).mousemove(function(a) {
				tdDemoMenu._moveScreenDemo(a)
			}), a(".td-screen-demo").hover(function(b) {
				a(this).css("visibility", "visible"), tdDemoMenu._resetTdScreeDemoExtendWidth()
			}, function(c) {
				b !== tdDemoMenu.startTimeout && (window.clearTimeout(tdDemoMenu.startTimeout), tdDemoMenu.startTimeout = b), b !== tdDemoMenu.startInterval && (window.clearInterval(tdDemoMenu.startInterval), tdDemoMenu.startInterval = b), a(this).css("visibility", "hidden"), a(".td-screen-demo-extend:first").hide()
			}).mousemove(function(a) {}), a(".td-screen-demo-extend").hover(function(c) {
				if (tdDemoMenu._extendedDemo) {
					tdDemoMenu._extendedDemo = !1;
					var d = a(this),
						e = a(".td-screen-demo:first"),
						f = parseInt(a("#td-theme-settings").css("width").replace("px", "")) / 2,
						g = 10,
						h = 50,
						i = 15,
						j = f;
					d.css({
						width: f + "px",
						top: e.css("top")
					}), d.show(), e.css("visibility", "visible"), tdDemoMenu._startExtendedTimeout = setTimeout(function() {
						tdDemoMenu._startExtendedInterval = setInterval(function() {
							j -= g;
							var c = !1;
							(j < 0 || tdDemoMenu.mousePosX <= a(window).width() - f - j) && (b !== tdDemoMenu._startExtendedTimeout && (window.clearTimeout(tdDemoMenu._startExtendedTimeout), tdDemoMenu._startExtendedTimeout = b), b !== tdDemoMenu._startExtendedInterval && (window.clearInterval(tdDemoMenu._startExtendedInterval), tdDemoMenu._startExtendedInterval = b), tdDemoMenu.mousePosX <= a(window).width() - f - j && (c = !0), j = f, d.hide()), d.css({
								width: j,
								top: e.css("top")
							}), c && tdDemoMenu._checkMousePosition()
						}, i)
					}, h)
				}
			}, function(c) {
				b !== tdDemoMenu._startExtendedTimeout && (window.clearTimeout(tdDemoMenu._startExtendedTimeout), tdDemoMenu._startExtendedTimeout = b), b !== tdDemoMenu._startExtendedInterval && (window.clearInterval(tdDemoMenu._startExtendedInterval), tdDemoMenu._startExtendedInterval = b), tdDemoMenu._resetTdScreeDemoExtendWidth(), a(this).hide(), a(".td-screen-demo:first").css("visibility", "hidden")
			}).mousemove(function(a) {})
		},
		_moveScreenDemo: function(b) {
			var c = a(".td-screen-demo:first"),
				d = a("#wpadminbar"),
				e = b.pageY - document.body.scrollTop - c.outerHeight(!0) / 2,
				f = 0;
			f = d.length ? d.outerHeight(!0) : 0, f > e && (e = f), e < 0 ? e = 0 : a(window).height() - c.outerHeight(!0) / 2 < b.pageY - document.body.scrollTop && (e = a(window).height() - c.outerHeight(!0)), c.css("top", e)
		},
		_resetTdScreeDemoExtendWidth: function() {
			var b = parseInt(a("#td-theme-settings").css("width").replace("px", "")) / 2;
			a(".td-screen-demo-extend:first").css({
				width: b + "px"
			})
		},
		_showExtendedScreenDemo: function() {
			tdDemoMenu._extendedDemo && a(".td-screen-demo-extend:first").css({
				top: a(".td-screen-demo:first").css("top")
			}).show()
		},
		_checkMousePosition: function() {
			var c;
			a(".td-set-theme-style-link").each(function(b, d) {
				tdDemoMenu._log(b);
				var e = a(d),
					f = "td-set-theme-style",
					g = e.closest("." + f),
					h = !1,
					i = !1;
				if (0 === a(".td-set-theme-style-link").index(d) % 2) parseInt(g.position().top) + parseInt(a(window).scrollTop()) < tdDemoMenu.mousePosY && tdDemoMenu.mousePosY < parseInt(g.position().top) + parseInt(a(window).scrollTop()) + parseInt(g.outerHeight()) && (h = !0, parseInt(a(window).width()) - 2 * parseInt(g.outerWidth()) < tdDemoMenu.mousePosX && tdDemoMenu.mousePosX < parseInt(a(window).width()) - parseInt(g.outerWidth()) && (i = !0));
				else {
					var j = g.prev("." + f);
					j.length && parseInt(j.position().top) + parseInt(a(window).scrollTop()) < tdDemoMenu.mousePosY && tdDemoMenu.mousePosY < parseInt(j.position().top) + parseInt(a(window).scrollTop()) + parseInt(j.outerHeight()) && (h = !0, parseInt(a(window).width()) - parseInt(g.outerWidth()) < tdDemoMenu.mousePosX && tdDemoMenu.mousePosX < parseInt(a(window).width()) && (i = !0))
				}
				if (h && i) return c = d, !1
			}), b === c ? a("#td-theme-settings").find(".td-screen-demo:first").css("visibility", "hidden") : a(c).mouseenter()
		},
		_log: function(a) {}
	}
}(jQuery),
function() {
	"use strict";
	var a = td_read_site_cookie("td_show_panel");
	if ("hide" === a) {
		var b = jQuery("#td-theme-settings");
		b.length && (b.removeClass("td-theme-settings-small"), jQuery("#td-theme-set-hide").html("DEMOS"))
	} else jQuery("#td-theme-set-hide").html("CLOSE")
}(), jQuery().ready(function() {
	"use strict";
	tdDetect.isIos === !1 && tdDetect.isAndroid === !1 && tdDemoMenu.init(), jQuery("#td-theme-set-hide").click(function(a) {
		a.preventDefault(), a.stopPropagation();
		var b = jQuery(this),
			c = jQuery("#td-theme-settings");
		c.hasClass("td-theme-settings-small") ? (c.removeClass("td-theme-settings-small"), c.addClass("td-theme-settings-closed"), b.html("DEMOS"), setTimeout(function() {
			c.addClass("td-ts-closed-no-transition")
		}, 450), td_set_cookies_life(["td_show_panel", "hide", 864e5])) : (c.removeClass("td-ts-closed-no-transition"), c.addClass("td-theme-settings-small"), c.removeClass("td-theme-settings-closed"), b.html("CLOSE"), td_set_cookies_life(["td_show_panel", "show", 864e5]))
	})
});
var tdTrendingNow = {};
! function() {
	"use strict";
	tdTrendingNow = {
		items: [],
		item: function() {
			this.blockUid = "", this.trendingNowAutostart = "manual", this.trendingNowTimer = 0, this.trendingNowPosition = 0, this.trendingNowPosts = [], this._is_initialized = !1
		},
		init: function() {
			tdTrendingNow.items = []
		},
		_initialize_item: function(a) {
			!0 !== a._is_initialized && (a._is_initialized = !0)
		},
		addItem: function(a) {
			if ("undefined" == typeof a.blockUid) throw "item.blockUid is not valid";
			if ("undefined" == typeof a.trendingNowPosts || a.trendingNowPosts.length < 1) throw "item.trendingNowPosts is not valid";
			tdTrendingNow.items.push(a), tdTrendingNow._initialize_item(a), tdTrendingNow.tdTrendingNowAutoStart(a.blockUid)
		},
		deleteItem: function(a) {
			for (var b = 0; b < tdTrendingNow.items.length; b++)
				if (tdTrendingNow.items[b].blockUid === a) return tdTrendingNow.items.splice(b, 1), !0;
			return !1
		},
		itemPrev: function(a) {
			for (var c, d = 0; d < tdTrendingNow.items.length; d++) tdTrendingNow.items[d].blockUid === a && (c = tdTrendingNow.items[d]);
			void 0 !== a && 1 >= c.trendingNowPosts.length || ("manual" !== c.trendingNowAutostart && (clearInterval(c.trendingNowTimer), c.trendingNowTimer = setInterval(function() {
				tdTrendingNow.tdTrendingNowChangeText([a, "left"], !0)
			}, 3e3)), tdTrendingNow.tdTrendingNowChangeText([a, "right"], !1))
		},
		itemNext: function(a) {
			for (var c, d = 0; d < tdTrendingNow.items.length; d++) tdTrendingNow.items[d].blockUid === a && (c = tdTrendingNow.items[d]);
			void 0 !== a && 1 >= c.trendingNowPosts.length || ("manual" !== c.trendingNowAutostart && (clearInterval(c.trendingNowTimer), c.trendingNowTimer = setInterval(function() {
				tdTrendingNow.tdTrendingNowChangeText([a, "left"], !0)
			}, 3e3)), tdTrendingNow.tdTrendingNowChangeText([a, "left"], !0))
		},
		tdTrendingNowChangeText: function(a, b) {
			for (var g, c = a[0], d = a[1], e = [], f = 0, h = 0; h < tdTrendingNow.items.length; h++) tdTrendingNow.items[h].blockUid === c && (g = h, e = tdTrendingNow.items[h].trendingNowPosts, f = tdTrendingNow.items[h].trendingNowPosition);
			if ("undefined" != typeof g && null !== g) {
				var i = f,
					j = e.length - 1;
				"left" === d ? (f += 1, f > j && (f = 0)) : (f -= 1, f < 0 && (f = j)), tdTrendingNow.items[g].trendingNowPosition = f, e[i].css("opacity", 0), e[i].css("z-index", 0);
				for (var k in e) !0 === e.hasOwnProperty(k) && e[k].removeClass("td_animated_xlong td_fadeInLeft td_fadeInRight td_fadeOutLeft td_fadeOutRight");
				e[f].css("opacity", 1), e[f].css("z-index", 1), !0 === b ? (e[i].addClass("td_animated_xlong td_fadeOutLeft"), e[f].addClass("td_animated_xlong td_fadeInRight")) : (e[i].addClass("td_animated_xlong td_fadeOutRight"), e[f].addClass("td_animated_xlong td_fadeInLeft"))
			}
		},
		tdTrendingNowAutoStart: function(a) {
			for (var b = 0; b < tdTrendingNow.items.length; b++) tdTrendingNow.items[b].blockUid === a && "manual" !== tdTrendingNow.items[b].trendingNowAutostart && (tdTrendingNow.items[b].trendingNowTimer = tdTrendingNow.setTimerChangingText(a))
		},
		setTimerChangingText: function(a) {
			return setInterval(function() {
				tdTrendingNow.tdTrendingNowChangeText([a, "left"], !0)
			}, 3e3)
		}
	}, tdTrendingNow.init()
}();
var td_history = {
	td_history_change_event: !1,
	init: function() {
		window.addEventListener("popstate", function(a) {
			td_history.td_history_change_event = !0, "undefined" != typeof a.state && null != a.state && jQuery("#" + a.state.slide_id).iosSlider("goToSlide", a.state.current_slide)
		})
	},
	replace_history_entry: function(a) {
		tdDetect.hasHistory !== !1 && history.replaceState(a, null)
	},
	add_history_entry: function(a, b, c) {
		if (tdDetect.hasHistory !== !1) {
			if ("" == c) return void history.pushState(a, null, null);
			var d = td_history.get_query_parameter("p");
			"" != d ? 1 == c ? history.pushState(a, null, "?p=" + d) : history.pushState(a, null, "?p=" + d + "&" + b + "=" + c) : 1 == c ? history.pushState(a, null, td_history.get_mod_rewrite_base_url()) : history.pushState(a, null, td_history.get_mod_rewrite_base_url() + c + "/")
		}
	},
	get_mod_rewrite_base_url: function() {
		var a = document.URL;
		return "/" == a.charAt(a.length - 1) && (a = a.slice(0, -1)), td_history.get_mod_rewrite_pagination(document.URL) === !1 ? document.URL : a.substring(0, a.lastIndexOf("/")) + "/"
	},
	get_mod_rewrite_pagination: function() {
		var a = document.URL;
		"/" == a.charAt(a.length - 1) && (a = a.slice(0, -1));
		var b = a.substring(a.lastIndexOf("/") + 1, a.length);
		return !!td_history.isInt(b) && b
	},
	get_current_page: function(a) {
		var b = td_history.get_query_parameter("p");
		if ("" != b) {
			var c = td_history.get_query_parameter(a);
			return "" != c ? c : 1
		}
		var c = td_history.get_mod_rewrite_pagination();
		return c !== !1 ? c : 1
	},
	isInt: function(a) {
		return a % 1 === 0
	},
	get_query_parameter: function(a) {
		a = a.replace(/[\[]/, "\\[").replace(/[\]]/, "\\]");
		var b = new RegExp("[\\?&]" + a + "=([^&#]*)"),
			c = b.exec(location.search);
		return null == c ? "" : decodeURIComponent(c[1].replace(/\+/g, " "))
	},
	slide_changed_callback: function(a) {
		if (td_history.td_history_change_event === !0) return void(td_history.td_history_change_event = !1);
		var b = a.currentSlideNumber,
			c = a.sliderContainerObject.attr("id");
		td_history.add_history_entry({
			current_slide: b,
			slide_id: c
		}, "slide", b)
	}
};
window.history && window.history.pushState && td_history.init();
var tdSmartSidebar = {};
! function() {
	"use strict";
	tdSmartSidebar = {
		hasItems: !1,
		items: [],
		scroll_window_scrollTop_last: 0,
		tds_snap_menu: tdUtil.getBackendVar("tds_snap_menu"),
		is_enabled: !0,
		is_enabled_state_run_once: !1,
		is_disabled_state_run_once: !1,
		is_tablet_grid: !1,
		_view_port_current_interval_index: tdViewport.getCurrentIntervalIndex(),
		item: function() {
			this.content_jquery_obj = "", this.sidebar_jquery_obj = "", this.sidebar_top = 0, this.sidebar_bottom = 0, this.sidebar_height = 0, this.content_top = 0, this.content_bottom = 0, this.sidebar_state = "", this.case_1_run_once = !1, this.case_2_run_once = !1, this.case_3_run_once = !1, this.case_3_last_sidebar_height = 0, this.case_3_last_content_height = 0, this.case_4_run_once = !1, this.case_4_last_menu_offset = 0, this.case_5_run_once = !1, this.case_6_run_once = !1
		},
		add_item: function(a) {
			tdSmartSidebar.hasItems = !0, a.sidebar_jquery_obj.prepend('<div class="clearfix"></div>').append('<div class="clearfix"></div>'), a.content_jquery_obj.prepend('<div class="clearfix"></div>').append('<div class="clearfix"></div>'), tdSmartSidebar.items.push(a)
		},
		td_events_scroll: function(a) {
			if (!1 !== tdSmartSidebar.hasItems)
				if (!1 !== tdSmartSidebar.is_enabled) window.requestAnimationFrame(function() {
					var b = 0;
					"" !== tdSmartSidebar.tds_snap_menu && (b = tdAffix._get_menu_affix_height(), "smart_snap_always" === tdAffix.tds_snap_menu && (b += tdAffix.menu_offset)), "undefined" != typeof window.tdThemeName && "Newspaper" === window.tdThemeName && (b += 20);
					var c = "";
					a !== tdSmartSidebar.scroll_window_scrollTop_last && (c = a > tdSmartSidebar.scroll_window_scrollTop_last ? "down" : "up"), tdSmartSidebar.scroll_window_scrollTop_last = a;
					var d = jQuery(window).height(),
						e = a + d;
					a += b;
					for (var f = 0; f < tdSmartSidebar.items.length; f++) {
						var g = tdSmartSidebar.items[f];
						if (g.content_top = g.content_jquery_obj.offset().top, g.content_height = g.content_jquery_obj.height(), g.content_bottom = g.content_top + g.content_height, g.sidebar_top = g.sidebar_jquery_obj.offset().top, g.sidebar_height = g.sidebar_jquery_obj.height(), g.sidebar_bottom = g.sidebar_top + g.sidebar_height, g.content_height <= g.sidebar_height) g.sidebar_state = "case_6_content_too_small";
						else if (g.sidebar_height < d) {
							var h = g.content_top;
							tdAffix.is_menu_affix || "undefined" == typeof window.tdThemeName || "Newsmag" !== window.tdThemeName || "smart_snap_always" !== tdAffix.tds_snap_menu || (h += b), tdSmartSidebar._is_smaller_or_equal(a, h) ? g.sidebar_state = "case_2_top_of_content" : !0 === tdSmartSidebar._is_smaller(g.sidebar_bottom, a) ? tdSmartSidebar._is_smaller(a, g.content_bottom - g.sidebar_height) ? g.sidebar_state = "case_4_fixed_up" : g.sidebar_state = "case_3_bottom_of_content" : tdSmartSidebar._is_smaller_or_equal(g.content_bottom, g.sidebar_bottom) ? "up" === c && tdSmartSidebar._is_smaller_or_equal(a, g.sidebar_top) ? g.sidebar_state = "case_4_fixed_up" : g.sidebar_state = "case_3_bottom_of_content" : g.content_bottom - a >= g.sidebar_height ? g.sidebar_state = "case_4_fixed_up" : g.sidebar_state = "case_3_bottom_of_content"
						} else !0 === tdSmartSidebar._is_smaller(g.sidebar_bottom, a) ? !0 === tdSmartSidebar._is_smaller_or_equal(a, g.sidebar_top) && !0 === tdSmartSidebar._is_smaller_or_equal(g.content_top, a) ? g.sidebar_state = "case_4_fixed_up" : !0 === tdSmartSidebar._is_smaller(g.sidebar_bottom, e) && !0 === tdSmartSidebar._is_smaller(g.sidebar_bottom, g.content_bottom) && g.content_bottom >= e ? g.sidebar_state = "case_1_fixed_down" : g.sidebar_state = "case_3_bottom_of_content" : !0 === tdSmartSidebar._is_smaller(g.sidebar_bottom, e) && !0 === tdSmartSidebar._is_smaller(g.sidebar_bottom, g.content_bottom) && "down" === c && g.content_bottom >= e ? g.sidebar_state = "case_1_fixed_down" : !0 === tdSmartSidebar._is_smaller_or_equal(g.sidebar_top, g.content_top) && "up" === c && g.content_bottom >= e ? g.sidebar_state = "case_2_top_of_content" : !0 === tdSmartSidebar._is_smaller_or_equal(g.content_bottom, g.sidebar_bottom) && "down" === c || g.content_bottom < e ? g.sidebar_state = "case_3_bottom_of_content" : !0 === tdSmartSidebar._is_smaller_or_equal(a, g.sidebar_top) && "up" === c && !0 === tdSmartSidebar._is_smaller_or_equal(g.content_top, a) ? g.sidebar_state = "case_4_fixed_up" : "up" === c && !0 === tdSmartSidebar._is_smaller_or_equal(e, g.sidebar_top) && (g.sidebar_state = "case_2_top_of_content"), ("case_1_fixed_down" === g.sidebar_state && "up" === c || "case_4_fixed_up" === g.sidebar_state && "down" === c) && (g.sidebar_state = "case_5_absolute");
						var i = 0,
							j = tdViewport.getCurrentIntervalItem();
						switch (null !== j && (i = j.sidebarWidth), g.sidebar_state) {
							case "case_1_fixed_down":
								if (!0 === g.case_1_run_once) break;
								tdSmartSidebar.log("sidebar_id: " + f + " " + g.sidebar_state), g.case_1_run_once = !0, g.case_2_run_once = !1, g.case_3_run_once = !1, g.case_4_run_once = !1, g.case_5_run_once = !1, g.case_6_run_once = !1, g.sidebar_jquery_obj.css({
									width: i,
									position: "fixed",
									top: "auto",
									bottom: "0",
									"z-index": "1"
								});
								break;
							case "case_2_top_of_content":
								if (!0 === g.case_2_run_once) break;
								tdSmartSidebar.log("sidebar_id: " + f + " " + g.sidebar_state), g.case_1_run_once = !1, g.case_2_run_once = !0, g.case_3_run_once = !1, g.case_4_run_once = !1, g.case_5_run_once = !1, g.case_6_run_once = !1, g.sidebar_jquery_obj.css({
									width: "auto",
									position: "static",
									top: "auto",
									bottom: "auto"
								});
								break;
							case "case_3_bottom_of_content":
								if (!0 === g.case_3_run_once && g.case_3_last_sidebar_height === g.sidebar_height && g.case_3_last_content_height === g.content_height) break;
								tdSmartSidebar.log("sidebar_id: " + f + " " + g.sidebar_state), g.case_1_run_once = !1, g.case_2_run_once = !1, g.case_3_run_once = !0, g.case_3_last_sidebar_height = g.sidebar_height, g.case_3_last_content_height = g.content_height, g.case_4_run_once = !1, g.case_5_run_once = !1, g.case_6_run_once = !1, g.sidebar_jquery_obj.css({
									width: i,
									position: "absolute",
									top: g.content_bottom - g.sidebar_height - g.content_top,
									bottom: "auto"
								});
								break;
							case "case_4_fixed_up":
								if (!0 === g.case_4_run_once && g.case_4_last_menu_offset === b) break;
								tdSmartSidebar.log("sidebar_id: " + f + " " + g.sidebar_state), g.case_1_run_once = !1, g.case_2_run_once = !1, g.case_3_run_once = !1, g.case_4_run_once = !0, g.case_4_last_menu_offset = b, g.case_5_run_once = !1, g.case_6_run_once = !1, g.sidebar_jquery_obj.css({
									width: i,
									position: "fixed",
									top: b,
									bottom: "auto"
								});
								break;
							case "case_5_absolute":
								if (!0 === g.case_5_run_once) break;
								tdSmartSidebar.log("sidebar_id: " + f + " " + g.sidebar_state), g.case_1_run_once = !1, g.case_2_run_once = !1, g.case_3_run_once = !1, g.case_4_run_once = !1, g.case_5_run_once = !0, g.case_6_run_once = !1, g.sidebar_jquery_obj.css({
									width: i,
									position: "absolute",
									top: g.sidebar_top - g.content_top,
									bottom: "auto"
								});
								break;
							case "case_6_content_too_small":
								if (!0 === g.case_6_run_once) break;
								tdSmartSidebar.log("sidebar_id: " + f + " " + g.sidebar_state), g.case_1_run_once = !1, g.case_2_run_once = !1, g.case_3_run_once = !1, g.case_4_run_once = !1, g.case_5_run_once = !1, g.case_6_run_once = !0, g.sidebar_jquery_obj.css({
									width: "auto",
									position: "static",
									top: "auto",
									bottom: "auto"
								})
						}
					}
				});
				else if (!1 === tdSmartSidebar.is_disabled_state_run_once) {
				tdSmartSidebar.is_disabled_state_run_once = !0;
				for (var b = 0; b < tdSmartSidebar.items.length; b++) tdSmartSidebar.items[b].sidebar_jquery_obj.css({
					width: "auto",
					position: "static",
					top: "auto",
					bottom: "auto"
				});
				tdSmartSidebar.log("smart_sidebar_disabled")
			}
		},
		compute: function() {
			tdSmartSidebar.td_events_scroll(jQuery(window).scrollTop())
		},
		reset_run_once_flags: function() {
			for (var a = 0; a < tdSmartSidebar.items.length; a++) tdSmartSidebar.items[a].case_1_run_once = !1, tdSmartSidebar.items[a].case_2_run_once = !1, tdSmartSidebar.items[a].case_3_run_once = !1, tdSmartSidebar.items[a].case_3_last_sidebar_height = 0, tdSmartSidebar.items[a].case_3_last_content_height = 0, tdSmartSidebar.items[a].case_4_run_once = !1, tdSmartSidebar.items[a].case_4_last_menu_offset = 0, tdSmartSidebar.items[a].case_5_run_once = !1, tdSmartSidebar.items[a].case_6_run_once = !1
		},
		td_events_resize: function() {
			switch (tdSmartSidebar._view_port_current_interval_index = tdViewport.getCurrentIntervalIndex(), tdSmartSidebar._view_port_current_interval_index) {
				case 0:
					tdSmartSidebar.is_enabled = !1, tdSmartSidebar.is_enabled_state_run_once = !1;
					break;
				case 1:
					!1 === tdSmartSidebar.is_tablet_grid && (tdSmartSidebar.reset_run_once_flags(), tdSmartSidebar.is_tablet_grid = !0, tdSmartSidebar.is_desktop_grid = !1, tdSmartSidebar.log("view port tablet")), tdSmartSidebar.is_enabled = !0, tdSmartSidebar.is_disabled_state_run_once = !1, !1 === tdSmartSidebar.is_enabled_state_run_once && (tdSmartSidebar.is_enabled_state_run_once = !0, tdSmartSidebar.log("smart_sidebar_enabled"));
					break;
				case 2:
				case 3:
					!0 === tdSmartSidebar.is_tablet_grid && (tdSmartSidebar.reset_run_once_flags(), tdSmartSidebar.is_tablet_grid = !1, tdSmartSidebar.is_desktop_grid = !0, tdSmartSidebar.log("view port desktop")), tdSmartSidebar.is_enabled = !0, tdSmartSidebar.is_disabled_state_run_once = !1, !1 === tdSmartSidebar.is_enabled_state_run_once && (tdSmartSidebar.is_enabled_state_run_once = !0, tdSmartSidebar.log("smart_sidebar_enabled"))
			}
			tdSmartSidebar.compute()
		},
		log: function(a) {},
		_is_smaller_or_equal: function(a, b) {
			return !(Math.abs(a - b) >= 1) || a < b
		},
		_is_smaller: function(a, b) {
			return Math.abs(a - b) >= 1 && a < b
		}
	}
}();
var tdInfiniteLoader = {};
! function() {
	"use strict";
	tdInfiniteLoader = {
		hasItems: !1,
		items: [],
		item: function() {
			this.uid = "", this.jqueryObj = "", this.bottomTop = 0, this.isVisibleCallbackEnabled = !0, this.isVisibleCallback = function() {}
		},
		addItem: function(a) {
			tdInfiniteLoader.hasItems = !0, tdInfiniteLoader.items.push(a)
		},
		computeTopDistances: function() {
			tdInfiniteLoader.hasItems !== !1 && (jQuery.each(tdInfiniteLoader.items, function(a, b) {
				var c = tdInfiniteLoader.items[a].jqueryObj.offset().top;
				tdInfiniteLoader.items[a].bottomTop = c + tdInfiniteLoader.items[a].jqueryObj.height()
			}), tdInfiniteLoader.computeEvents())
		},
		computeEvents: function() {
			if (tdInfiniteLoader.hasItems !== !1) {
				var a = jQuery(window).height() + jQuery(window).scrollTop();
				jQuery.each(tdInfiniteLoader.items, function(b, c) {
					tdInfiniteLoader.items[b].bottomTop < a + 700 && tdInfiniteLoader.items[b].isVisibleCallbackEnabled === !0 && (tdInfiniteLoader.items[b].isVisibleCallbackEnabled = !1, tdInfiniteLoader.items[b].isVisibleCallback())
				})
			}
		},
		enable_is_visible_callback: function(a) {
			jQuery.each(tdInfiniteLoader.items, function(b, c) {
				if (c.uid === a) return tdInfiniteLoader.items[b].isVisibleCallbackEnabled = !0, !1
			})
		}
	}, jQuery(".td_ajax_infinite").each(function() {
		var a = new tdInfiniteLoader.item;
		a.jqueryObj = jQuery(this), a.uid = jQuery(this).data("td_block_id"), a.isVisibleCallback = function() {
			var b = tdBlocks.tdGetBlockObjById(a.jqueryObj.data("td_block_id"));
			"" === b.ajax_pagination_infinite_stop || b.td_current_page < parseInt(b.ajax_pagination_infinite_stop) + 1 ? (b.td_current_page++, tdBlocks.tdAjaxDoBlockRequest(b, "infinite_load")) : b.td_current_page < b.max_num_pages && setTimeout(function() {
				jQuery("#infinite-lm-" + b.id).css("display", "block").css("visibility", "visible")
			}, 400)
		}, tdInfiniteLoader.addItem(a)
	}), jQuery(window).load(function() {
		tdInfiniteLoader.computeTopDistances()
	}), jQuery().ready(function() {
		tdInfiniteLoader.computeTopDistances()
	})
}();
var Froogaloop = function() {
		function a(b) {
			return new a.fn.init(b)
		}

		function b(a, b, c) {
			if (!c.contentWindow.postMessage) return !1;
			var d = c.getAttribute("src").split("?")[0],
				a = JSON.stringify({
					method: a,
					value: b
				});
			"//" === d.substr(0, 2) && (d = window.location.protocol + d), c.contentWindow.postMessage(a, d)
		}

		function c(a) {
			var b, c;
			try {
				b = JSON.parse(a.data), c = b.event || b.method
			} catch (a) {}
			if ("ready" == c && !f && (f = !0), a.origin != g) return !1;
			var a = b.value,
				d = b.data,
				h = "" === h ? null : b.player_id;
			return b = h ? e[h][c] : e[c], c = [], !!b && (void 0 !== a && c.push(a), d && c.push(d), h && c.push(h), 0 < c.length ? b.apply(null, c) : b.call())
		}

		function d(a, b, c) {
			c ? (e[c] || (e[c] = {}), e[c][a] = b) : e[a] = b
		}
		var e = {},
			f = !1,
			g = "";
		return a.fn = a.prototype = {
			element: null,
			init: function(a) {
				"string" == typeof a && (a = document.getElementById(a)), this.element = a, a = this.element.getAttribute("src"), "//" === a.substr(0, 2) && (a = window.location.protocol + a);
				for (var a = a.split("/"), b = "", c = 0, d = a.length; c < d && 3 > c; c++) b += a[c], 2 > c && (b += "/");
				return g = b, this
			},
			api: function(a, c) {
				if (!this.element || !a) return !1;
				var e = this.element,
					f = "" !== e.id ? e.id : null,
					g = c && c.constructor && c.call && c.apply ? null : c,
					h = c && c.constructor && c.call && c.apply ? c : null;
				return h && d(a, h, f), b(a, g, e), this
			},
			addEvent: function(a, c) {
				if (!this.element) return !1;
				var e = this.element,
					g = "" !== e.id ? e.id : null;
				return d(a, c, g), "ready" != a ? b("addEventListener", a, e) : "ready" == a && f && c.call(null, g), this
			},
			removeEvent: function(a) {
				if (!this.element) return !1;
				var d, c = this.element;
				a: {
					if ((d = "" !== c.id ? c.id : null) && e[d]) {
						if (!e[d][a]) {
							d = !1;
							break a
						}
						e[d][a] = null
					} else {
						if (!e[a]) {
							d = !1;
							break a
						}
						e[a] = null
					}
					d = !0
				}
				"ready" != a && d && b("removeEventListener", a, c)
			}
		}, a.fn.init.prototype = a.fn, window.addEventListener ? window.addEventListener("message", c, !1) : window.attachEvent("onmessage", c), window.Froogaloop = window.$f = a
	}(),
	tdCustomEvents = {};
! function() {
	"use strict";
	tdCustomEvents = {
		_callback_scroll: function() {
			tdAnimationScroll.compute_all_items()
		},
		_callback_resize: function() {},
		_lazy_callback_scroll_100: function() {
			!0 === tdAnimationStack.activated && tdAnimationStack.td_events_scroll()
		},
		_lazy_callback_scroll_500: function() {},
		_lazy_callback_resize_100: function() {
			tdPullDown.td_events_resize(), tdBackstr.td_events_resize(), tdAnimationScroll.td_events_resize()
		},
		_lazy_callback_resize_500: function() {
			!0 === tdAnimationStack.activated && tdAnimationStack.td_events_resize();
			for (var a = 0; a < td_backstretch_items.length; a++) tdAnimationScroll.reinitialize_item(td_backstretch_items[a], !0), td_compute_backstretch_item(td_backstretch_items[a]);
			tdAnimationScroll.compute_all_items(), setMenuMinHeight()
		}
	}
}();
var tdEvents = {};
! function() {
	"use strict";
	tdEvents = {
		scroll_event_slow_run: !1,
		scroll_event_medium_run: !1,
		resize_event_slow_run: !1,
		resize_event_medium_run: !1,
		scroll_window_scrollTop: 0,
		window_pageYOffset: window.pageYOffset,
		window_innerHeight: window.innerHeight,
		window_innerWidth: window.innerWidth,
		init: function() {
			jQuery(window).scroll(function() {
				tdEvents.scroll_event_slow_run = !0, tdEvents.scroll_event_medium_run = !0, tdEvents.scroll_window_scrollTop = jQuery(window).scrollTop(), tdEvents.window_pageYOffset = window.pageYOffset, tdAffix.td_events_scroll(tdEvents.scroll_window_scrollTop), tdSmartSidebar.td_events_scroll(tdEvents.scroll_window_scrollTop), tdCustomEvents._callback_scroll()
			}), jQuery(window).resize(function() {
				tdEvents.resize_event_slow_run = !0, tdEvents.resize_event_medium_run = !0, tdEvents.window_innerHeight = window.innerHeight, tdEvents.window_innerWidth = window.innerWidth, tdCustomEvents._callback_resize()
			}), setInterval(function() {
				tdViewport.detectChanges(), tdEvents.scroll_event_medium_run && (tdEvents.scroll_event_medium_run = !1, tdInfiniteLoader.computeEvents(), tdCustomEvents._lazy_callback_scroll_100()), tdEvents.resize_event_medium_run && (tdEvents.resize_event_medium_run = !1, tdSmartSidebar.td_events_resize(), tdCustomEvents._lazy_callback_resize_100())
			}, 100), setInterval(function() {
				tdEvents.scroll_event_slow_run && (tdEvents.scroll_event_slow_run = !1, td_events_scroll_scroll_to_top(tdEvents.scroll_window_scrollTop), td_more_articles_box.td_events_scroll(tdEvents.scroll_window_scrollTop), tdCustomEvents._lazy_callback_scroll_500()), tdEvents.resize_event_slow_run && (tdEvents.resize_event_slow_run = !1, tdAffix.compute_wrapper(), tdAffix.compute_top(), tdDetect.runIsPhoneScreen(), tdCustomEvents._lazy_callback_resize_500())
			}, 500)
		}
	}, tdEvents.init()
}();
var tdAjaxCount = {};
! function() {
	"use strict";
	tdAjaxCount = {
		tdGetViewsCountsAjax: function(a, b) {
			var c = "td_ajax_get_views";
			"post" === a && (c = "td_ajax_update_views"), jQuery.ajax({
				type: "POST",
				url: td_ajax_url,
				cache: !0,
				data: {
					action: c,
					td_post_ids: b
				},
				success: function(a, b, c) {
					var d = jQuery.parseJSON(a);
					d instanceof Object && jQuery.each(d, function(a, b) {
						var c = ".td-nr-views-" + a;
						jQuery(c).html(b)
					})
				},
				error: function(a, b, c) {}
			})
		}
	}
}();
var tdYoutubePlayers = {},
	tdVimeoPlayers = {};
jQuery().ready(function() {
		"use strict";
		tdYoutubePlayers.init(), tdVimeoPlayers.init()
	}),
	function() {
		"use strict";
		tdYoutubePlayers = {
			tdPlayerContainer: "player_youtube",
			players: [],
			init: function() {
				for (var a = jQuery(".td_wrapper_playlist_player_youtube"), b = 0; b < a.length; b++) {
					var c = jQuery(a[b]),
						d = tdYoutubePlayers.addPlayer(c),
						e = d.tdPlayerContainer;
					c.parent().find(".td_youtube_control").data("player-id", e);
					for (var f = c.parent().find(".td_click_video_youtube"), g = 0; g < f.length; g++) jQuery(f[g]).data("player-id", e), g + 1 < f.length ? jQuery(f[g]).data("next-video-id", jQuery(f[g + 1]).data("video-id")) : jQuery(f[g]).data("next-video-id", jQuery(f[0]).data("video-id"));
					"1" == c.data("autoplay") && (d.autoplay = 1);
					var h = c.data("first-video");
					"" !== h && (d.tdPlaylistIdYoutubeVideoRunning = h, d.playVideo(h))
				}
				jQuery(".td_click_video_youtube").click(function() {
					var a = jQuery(this).data("video-id"),
						b = jQuery(this).data("player-id");
					void 0 !== b && "" !== b && void 0 !== a && "" !== a && tdYoutubePlayers.operatePlayer(b, "play", a)
				}), jQuery(".td_youtube_control").click(function() {
					var a = jQuery(this).data("player-id");
					void 0 !== a && "" !== a && (jQuery(this).hasClass("td-sp-video-play") ? tdYoutubePlayers.operatePlayer(a, "play") : tdYoutubePlayers.operatePlayer(a, "pause"))
				})
			},
			addPlayer: function(a) {
				var b = tdYoutubePlayers.tdPlayerContainer + "_" + tdYoutubePlayers.players.length,
					c = tdYoutubePlayers.createPlayer(b, a);
				return tdYoutubePlayers.players.push(c), c
			},
			operatePlayer: function(a, b, c) {
				for (var d = 0; d < tdYoutubePlayers.players.length; d++)
					if (tdYoutubePlayers.players[d].tdPlayerContainer == a) {
						var e = tdYoutubePlayers.players[d];
						e.playStatus(), "play" === b ? (e.autoplay = 1, void 0 === c ? e.playerPlay() : e.playVideo(c)) : "pause" == b && tdYoutubePlayers.players[d].playerPause();
						break
					}
			},
			createPlayer: function(a, b) {
				var c = {
					tdYtPlayer: "",
					tdPlayerContainer: a,
					autoplay: 0,
					tdPlaylistIdYoutubeVideoRunning: "",
					jqTDWrapperVideoPlaylist: b.closest(".td_wrapper_video_playlist"),
					jqPlayerWrapper: b,
					jqControlPlayer: "",
					_videoId: "",
					playVideo: function(a) {
						c._videoId = a, "undefined" == typeof YT || "undefined" == typeof YT.Player ? (window.onYouTubePlayerAPIReady = function() {
							for (var a = 0; a < tdYoutubePlayers.players.length; a++) tdYoutubePlayers.players[a].loadPlayer()
						}, jQuery.getScript("https://www.youtube.com/player_api").done(function(a, b) {})) : c.loadPlayer(a)
					},
					loadPlayer: function(a) {
						var b = c._videoId;
						if (void 0 !== a && (b = a), void 0 !== b) {
							c.tdPlaylistIdYoutubeVideoRunning = b;
							var d = window.td_youtube_list_ids["td_" + c.tdPlaylistIdYoutubeVideoRunning].title,
								e = window.td_youtube_list_ids["td_" + c.tdPlaylistIdYoutubeVideoRunning].time;
							c.jqTDWrapperVideoPlaylist.find(".td_click_video_youtube").removeClass("td_video_currently_playing"), c.jqTDWrapperVideoPlaylist.find(".td_" + b).addClass("td_video_currently_playing"), c.jqTDWrapperVideoPlaylist.find(".td_current_video_play_title_youtube").html(d), c.jqTDWrapperVideoPlaylist.find(".td_current_video_play_time_youtube").html(e), c.jqPlayerWrapper.html("<div id=" + c.tdPlayerContainer + "></div>"), c.jqControlPlayer = c.jqTDWrapperVideoPlaylist.find(".td_youtube_control"), c.tdYtPlayer = new YT.Player(c.tdPlayerContainer, {
								playerVars: {
									autoplay: c.autoplay
								},
								height: "100%",
								width: "100%",
								videoId: b,
								events: {
									onStateChange: c.onPlayerStateChange
								}
							})
						}
					},
					onPlayerStateChange: function(a) {
						if (a.data === YT.PlayerState.PLAYING) c.pauseStatus();
						else if (a.data === YT.PlayerState.ENDED) {
							c.playStatus(), c.autoplay = 1;
							var b = "",
								d = c.jqTDWrapperVideoPlaylist.find(".td_video_currently_playing");
							if (d.length) {
								var e = jQuery(d).next(".td_click_video_youtube");
								e.length && (b = jQuery(e).data("video-id"))
							}
							"" !== b && c.playVideo(b)
						} else YT.PlayerState.PAUSED && c.playStatus()
					},
					playerPlay: function() {
						c.tdYtPlayer.playVideo()
					},
					playerPause: function() {
						c.tdYtPlayer.pauseVideo()
					},
					playStatus: function() {
						c.jqControlPlayer.removeClass("td-sp-video-pause").addClass("td-sp-video-play")
					},
					pauseStatus: function() {
						c.jqControlPlayer.removeClass("td-sp-video-play").addClass("td-sp-video-pause")
					}
				};
				return c
			}
		}, tdVimeoPlayers = {
			tdPlayerContainer: "player_vimeo",
			players: [],
			existingAutoplay: !1,
			init: function() {
				for (var a = jQuery(".td_wrapper_playlist_player_vimeo"), b = 0; b < a.length; b++) {
					var c = tdVimeoPlayers.addPlayer(jQuery(a[b]));
					0 !== c.autoplay && (tdVimeoPlayers.existingAutoplay = !0)
				}
				jQuery(".td_click_video_vimeo").click(function() {
					var a = jQuery(this).data("video-id"),
						b = jQuery(this).data("player-id");
					void 0 !== b && "" !== b && void 0 !== a && "" !== a && tdVimeoPlayers.operatePlayer(b, "play", a)
				}), jQuery(".td_vimeo_control").click(function() {
					var a = jQuery(this).data("player-id");
					void 0 !== a && "" !== a && (jQuery(this).hasClass("td-sp-video-play") ? tdVimeoPlayers.operatePlayer(a, "play") : tdVimeoPlayers.operatePlayer(a, "pause"))
				})
			},
			addPlayer: function(a) {
				var b = tdVimeoPlayers.tdPlayerContainer + "_" + tdVimeoPlayers.players.length,
					c = tdVimeoPlayers.createPlayer(b, a);
				a.parent().find(".td_vimeo_control").data("player-id", b);
				for (var d = a.parent().find(".td_click_video_vimeo"), e = 0; e < d.length; e++) jQuery(d[e]).data("player-id", b), e + 1 < d.length ? jQuery(d[e]).data("next-video-id", jQuery(d[e + 1]).data("video-id")) : jQuery(d[e]).data("next-video-id", jQuery(d[0]).data("video-id"));
				"1" == a.data("autoplay") && (c.autoplay = 1);
				var f = a.data("first-video");
				return void 0 !== f && "" !== f && c.createPlayer(f), tdVimeoPlayers.players.push(c), c
			},
			operatePlayer: function(a, b, c) {
				for (var d = 0; d < tdVimeoPlayers.players.length; d++)
					if (tdVimeoPlayers.players[d].playerId == a) {
						var e = tdVimeoPlayers.players[d];
						"play" === b ? (e.autoplay = 1, void 0 !== c ? (tdVimeoPlayers.existingAutoplay = !1, e.createPlayer(c)) : e.playerPlay()) : "pause" === b && e.playerPause();
						break
					}
			},
			createPlayer: function(a, b) {
				var c = {
					playerId: a,
					jqTDWrapperVideoPlaylist: b.closest(".td_wrapper_video_playlist"),
					jqPlayerWrapper: b,
					currentVideoPlaying: "",
					player: "",
					jqControlPlayer: "",
					autoplay: 0,
					createPlayer: function(a) {
						if ("" !== a) {
							this.currentVideoPlaying = a;
							var b = "",
								d = window.td_vimeo_list_ids["td_" + a].title,
								e = window.td_vimeo_list_ids["td_" + a].time;
							c.jqTDWrapperVideoPlaylist.find(".td_click_video_vimeo").removeClass("td_video_currently_playing"), c.jqTDWrapperVideoPlaylist.find(".td_" + a).addClass("td_video_currently_playing"), c.jqTDWrapperVideoPlaylist.find(".td_current_video_play_title_vimeo").html(d), c.jqTDWrapperVideoPlaylist.find(".td_current_video_play_time_vimeo").html(e), c.jqControlPlayer = c.jqTDWrapperVideoPlaylist.find(".td_vimeo_control"), tdVimeoPlayers.existingAutoplay || 0 === c.autoplay ? c.playStatus() : (b = "&autoplay=1", tdDetect.isMobileDevice ? c.playStatus() : c.pauseStatus()), c.jqPlayerWrapper.html('<iframe id="' + c.playerId + '" src="https://player.vimeo.com/video/' + a + "?api=1&player_id=" + c.playerId + b + '"  frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe>'), c.createVimeoObjectPlayer(jQuery)
						}
					},
					createVimeoObjectPlayer: function(a) {
						var b = "",
							d = a("#" + c.playerId);
						d.length && (b = $f(d[0]), c.player = b, b.addEvent("ready", function() {
							b.addEvent("play", function(a) {
								c.pauseStatus(), c.autoplay = 1
							}), b.addEvent("pause", function(a) {
								c.playStatus()
							}), b.addEvent("finish", function(a) {
								var b = "",
									d = c.jqTDWrapperVideoPlaylist.find(".td_video_currently_playing");
								if (d.length) {
									var e = jQuery(d).next(".td_click_video_vimeo");
									e.length && (b = jQuery(e).data("video-id"))
								}
								"" !== b ? (c.createPlayer(b), tdDetect.isMobileDevice ? c.playStatus() : c.pauseStatus()) : c.playStatus()
							})
						}))
					},
					playerPlay: function() {
						c.autoplay = 1, c.player.api("play")
					},
					playerPause: function() {
						c.player.api("pause")
					},
					playStatus: function() {
						c.jqControlPlayer.removeClass("td-sp-video-pause").addClass("td-sp-video-play")
					},
					pauseStatus: function() {
						c.jqControlPlayer.removeClass("td-sp-video-play").addClass("td-sp-video-pause")
					}
				};
				return c
			}
		}
	}(), jQuery(window).load(function() {
		td_resize_smartlist_sliders_and_update()
	}), jQuery().ready(function() {
		td_resize_smartlist_sliders_and_update()
	});
var tdPullDown = {};
! function() {
	"use strict";
	tdPullDown = {
		_view_port_interval_index: tdViewport.INTERVAL_INITIAL_INDEX,
		reinitialize_items_at_change_view_port: !1,
		items: [],
		item: function() {
			this.blockUid = "", this.horizontal_jquery_obj = "", this.vertical_jquery_obj = "", this.container_jquery_obj = "", this.horizontal_element_css_class = "", this.minimum_elements = 2, this.excluded_jquery_elements = [], this._horizontal_extra_space = 1, this._horizontal_elements = [], this._vertical_elements = [], this._vertical_ul_jquery_obj = "", this._vertical_jquery_obj_outer_width = 0, this._is_initialized = !1
		},
		init: function() {
			tdPullDown._view_port_interval_index = tdViewport.getCurrentIntervalIndex(), tdPullDown.items = []
		},
		add_item: function(a) {
			if (1 !== a.vertical_jquery_obj.length) throw "item.vertical_jquery_obj is more or less than one: " + a.vertical_jquery_obj.length;
			if (1 !== a.horizontal_jquery_obj.length) throw "item.horizontal_jquery_obj is more or less than one: " + a.horizontal_jquery_obj.length;
			if (1 !== a.container_jquery_obj.length) throw "item.container_jquery_obj is more or less than one: " + a.container_jquery_obj.length;
			if ("" === a.horizontal_element_css_class) throw "item.horizontal_element_css_class is empty";
			tdPullDown.items.push(a), tdPullDown._initialize_item(a), tdPullDown._compute_item(a)
		},
		deleteItem: function(a) {
			for (var b = 0; b < tdPullDown.items.length; b++)
				if (tdPullDown.items[b].blockUid === a) return tdPullDown.items.splice(b, 1), !0;
			return !1
		},
		_initialize_item: function(a) {
			if (!0 !== a._is_initialized) {
				if (a._vertical_ul_jquery_obj = a.vertical_jquery_obj.find("ul:first"), 0 === a._vertical_ul_jquery_obj.length) return void tdPullDown.log("Item can' be initialized. The vertical list doesn't have an 'ul' container");
				var b = a.horizontal_jquery_obj.find("." + a.horizontal_element_css_class),
					c = null,
					d = null;
				b.each(function(b, e) {
					c = jQuery(e), c.css("-webkit-transition", "opacity 0.2s"), c.css("-moz-transition", "opacity 0.2s"), c.css("-o-transition", "opacity 0.2s"), c.css("transition", "opacity 0.2s"), c.css("opacity", "1"), d = {
						jquery_object: c,
						calculated_width: c.outerWidth(!0)
					}, a._horizontal_elements.push(d)
				}), a._vertical_jquery_obj_outer_width = a.vertical_jquery_obj.outerWidth(!0), a.vertical_jquery_obj.css("display", "none");
				var e = a.horizontal_jquery_obj.css("padding-left");
				void 0 !== e && "" !== e && (a._horizontal_extra_space += parseInt(e.replace("px", "")));
				var f = a.horizontal_jquery_obj.css("padding-right");
				void 0 !== f && "" !== f && (a._horizontal_extra_space += parseInt(f.replace("px", "")));
				var g = a.horizontal_jquery_obj.css("margin-left");
				void 0 !== g && "" !== g && (a._horizontal_extra_space += parseInt(g.replace("px", "")));
				var h = a.horizontal_jquery_obj.css("margin-right");
				void 0 !== h && "" !== h && (a._horizontal_extra_space += parseInt(h.replace("px", "")));
				var i = a.horizontal_jquery_obj.css("border-left");
				void 0 !== i && "" !== i && (a._horizontal_extra_space += parseInt(i.replace("px", "")));
				var j = a.horizontal_jquery_obj.css("border-right");
				void 0 !== j && "" !== j && (a._horizontal_extra_space += parseInt(j.replace("px", ""))), a._is_initialized = !0
			}
		},
		_get_horizontal_elements_width: function(a) {
			for (var b = 0, c = a._horizontal_elements.length - 1; c >= 0; c--) b += a._horizontal_elements[c].calculated_width;
			return b
		},
		_reinitialize_all_items: function() {
			for (var a = tdPullDown.items.length - 1; a >= 0; a--) tdPullDown._reinitialize_item(tdPullDown.items[a])
		},
		_reinitialize_item: function(a) {
			!1 !== a._is_initialized && (a._is_initialized = !1, a.horizontal_jquery_obj.html(a.horizontal_jquery_obj.html() + a._vertical_ul_jquery_obj.html()), a._vertical_ul_jquery_obj.html(""), a._horizontal_elements = [], a._vertical_elements = [], a._horizontal_extra_space = 1, tdPullDown._initialize_item(a))
		},
		_compute_item: function(a) {
			if (!1 !== a._is_initialized) {
				tdPullDown._prepare_horizontal_header(a, !0);
				var b = 0,
					c = a.container_jquery_obj.css("width");
				if (void 0 !== c && "" !== c) {
					b = c.replace("px", "");
					for (var d = a.excluded_jquery_elements.length - 1; d >= 0; d--) b -= a.excluded_jquery_elements[d].outerWidth(!0)
				}
				a._vertical_elements.length > 0 && (b -= a._vertical_jquery_obj_outer_width), b -= tdPullDown._get_horizontal_elements_width(a), b -= a._horizontal_extra_space;
				for (var e; b < 0;) {
					if (0 !== a.minimum_elements && a._horizontal_elements.length <= a.minimum_elements) return tdPullDown._make_all_elements_vertical(a), void tdPullDown._prepare_horizontal_header(a);
					0 === a._vertical_elements.length && (b -= a._vertical_jquery_obj_outer_width), e = tdPullDown._make_element_vertical(a), b += e.calculated_width
				}
				if (0 !== a.minimum_elements && 0 === a._horizontal_elements.length && a._vertical_elements.length > 0 && b >= a._vertical_elements[0].calculated_width) {
					for (var f = 0, g = 0; g < a.minimum_elements && g < a._vertical_elements.length; g++) f += a._vertical_elements[g].calculated_width;
					for (var h = 0, i = a.minimum_elements; i > 0 && a._vertical_elements.length > 0 && b >= f;) {
						if (e = tdPullDown._make_element_horizontal(a), null === e) return void tdPullDown._prepare_horizontal_header(a);
						h += e.calculated_width, i--
					}
					b -= h
				}
				for (;
					(a._horizontal_elements.length > 0 || 0 === a._horizontal_elements.length && 0 === a.minimum_elements) && a._vertical_elements.length > 0 && b >= a._vertical_elements[0].calculated_width;) {
					if (e = tdPullDown._make_element_horizontal(a), null === e) return void tdPullDown._prepare_horizontal_header(a);
					b -= e.calculated_width
				}
				1 === a._vertical_elements.length && b + a._vertical_jquery_obj_outer_width >= a._vertical_elements[0].calculated_width && tdPullDown._make_element_horizontal(a), tdPullDown._prepare_horizontal_header(a)
			}
		},
		_prepare_horizontal_header: function(b, c) {
			var d = b.horizontal_jquery_obj.parent().siblings(".block-title:first");
			if (1 === d.length) {
				var e = d.find("span:first");
				1 === e.length && ("undefined" != typeof c && !0 === c ? e.css("margin-right", 0) : 0 === b._horizontal_elements.length ? e.css("margin-right", b._vertical_jquery_obj_outer_width + "px") : e.css("margin-right", 0))
			}
		},
		_compute_all_items: function() {
			for (var a = tdPullDown.items.length - 1; a >= 0; a--) tdPullDown.items[a].constructor === tdPullDown.item && tdPullDown._compute_item(tdPullDown.items[a])
		},
		_make_element_horizontal: function(a) {
			if (!1 === a._is_initialized || 0 === a._vertical_elements.length) return null;
			var b = a._vertical_elements.shift();
			return 0 === a._vertical_elements.length && a.vertical_jquery_obj.css("display", "none"), a._horizontal_elements.push(b), b.jquery_object.css("opacity", "0"), b.jquery_object.detach().appendTo(a.horizontal_jquery_obj), setTimeout(function() {
				b.jquery_object.css("opacity", "1")
			}, 50), b
		},
		_make_element_vertical: function(a) {
			if (!1 === a._is_initialized || 0 === a._horizontal_elements.length) return null;
			var b = a._horizontal_elements.pop();
			return 0 === a._vertical_elements.length && a.vertical_jquery_obj.css("display", ""), a._vertical_elements.unshift(b), b.jquery_object.detach().prependTo(a._vertical_ul_jquery_obj), b
		},
		_make_all_elements_vertical: function(a) {
			for (; a._horizontal_elements.length > 0;) tdPullDown._make_element_vertical(a)
		},
		td_events_resize: function() {
			0 !== tdPullDown.items.length && (!0 === tdPullDown.reinitialize_items_at_change_view_port && tdPullDown._view_port_interval_index !== tdViewport.getCurrentIntervalIndex() && tdPullDown._reinitialize_all_items(), tdPullDown._compute_all_items())
		},
		log: function(b) {}
	}, tdPullDown.init()
}();
var td_fps = {
		start_time: 0,
		current_time: 0,
		frame_number: 0,
		init: function() {
			td_fps.start_time = 0;
			var b = 0,
				c = 0,
				d = 0,
				e = jQuery("#fps_table");
			0 == e.length && (e = jQuery("<div>").css({
				position: "fixed",
				top: "120px",
				left: "10px",
				width: "100px",
				height: "20px",
				border: "1px solid black",
				"font-size": "11px",
				"z-index": "100000",
				"background-color": "white"
			}), e.appendTo("body"));
			var f = function() {
				td_fps.frame_number++, td_fps.current_time = Date.now(), d = (td_fps.current_time - td_fps.start_time) / 1e3, c = (td_fps.frame_number / d).toPrecision(2), c != b && (b = c, e.html(b + " fps")), d > 1 && (td_fps.start_time = td_fps.current_time, td_fps.frame_number = 0), requestAnimationFrame(f)
			};
			f()
		}
	},
	tdAnimationScroll = {};
! function() {
	"use strict";
	tdAnimationScroll = {
		items: [],
		rAFIndex: 0,
		animation_running: !1,
		item: function() {
			this.percent_value = 0, this.animation_callback = null, this.jqueryObj = "", this.wrapper_jquery_obj = void 0, this.top_marker_jquery_obj = "", this.full_height = 0, this.offset_top = "", this.offset_bottom_top = "", this.properties = {}, this.computed_item_properties = {}, this.redraw = !1, this.top_is_out = !1, this._is_initialized = !1, this.computation_stopped = !1, this.add_item_property = function(b, c, d, e, f, g) {
				if (!(c >= d))
					if (void 0 === this.properties[b]) this.properties[b] = {
						computed_value: "",
						settings: []
					}, 0 !== c && (this.properties[b].settings[this.properties[b].settings.length] = {
						start_percent: 0,
						end_percent: c,
						start_value: e,
						end_value: e,
						easing: ""
					}), this.properties[b].settings[this.properties[b].settings.length] = {
						start_percent: c,
						end_percent: d,
						start_value: e,
						end_value: f,
						easing: g
					}, this.properties[b].settings[this.properties[b].settings.length] = {
						start_percent: d,
						end_percent: 100,
						start_value: f,
						end_value: f,
						easing: ""
					};
					else {
						var h = this.properties[b].settings[this.properties[b].settings.length - 1];
						h.start_percent !== c ? (this.properties[b].settings[this.properties[b].settings.length - 1] = {
							start_percent: h.start_percent,
							end_percent: c,
							start_value: h.end_value,
							end_value: h.end_value,
							easing: ""
						}, this.properties[b].settings[this.properties[b].settings.length] = {
							start_percent: c,
							end_percent: d,
							start_value: e,
							end_value: f,
							easing: g
						}) : this.properties[b].settings[this.properties[b].settings.length - 1] = {
							start_percent: c,
							end_percent: d,
							start_value: e,
							end_value: f,
							easing: g
						}, 100 !== d && (this.properties[b].settings[this.properties[b].settings.length] = {
							start_percent: d,
							end_percent: 100,
							start_value: f,
							end_value: f,
							easing: ""
						})
					}
			}, this.remove_item_property = function(b) {
				return void 0 !== this.properties[b] && (delete this.properties[b], !0)
			}
		},
		init: function() {
			tdAnimationScroll.items = []
		},
		add_item: function(b) {
			b.constructor === tdAnimationScroll.item && (tdAnimationScroll.items.push(b), tdAnimationScroll._initialize_item(b))
		},
		_initialize_item: function(b) {
			if (!0 !== b._is_initialized && (void 0 === b.wrapper_jquery_obj ? b.full_height = b.jqueryObj.outerHeight(!0) : b.full_height = b.wrapper_jquery_obj.height(), 0 !== b.full_height)) {
				var c = jQuery('<div class="td_marker_animation" style="height: 0; width: 0">');
				c.insertBefore(b.jqueryObj), b.top_marker_jquery_obj = c, b.offset_top = b.top_marker_jquery_obj.offset().top, b.offset_bottom_top = b.offset_top + b.full_height, b.top_is_out = tdEvents.window_pageYOffset > b.offset_top, b._is_initialized = !0
			}
		},
		reinitialize_all_items: function(b) {
			for (var c = tdAnimationScroll.items.length - 1; c >= 0; c--) tdAnimationScroll.reinitialize_item(tdAnimationScroll.items[c], b)
		},
		reinitialize_item: function(b, c) {
			!1 !== b._is_initialized && (b._is_initialized = !1, b.offset_top = b.top_marker_jquery_obj.offset().top, !0 === c && (void 0 === b.wrapper_jquery_obj ? b.full_height = b.jqueryObj.outerHeight(!0) : b.full_height = b.wrapper_jquery_obj.height(), 0 === b.full_height) || (b.offset_bottom_top = b.offset_top + b.full_height, b._is_initialized = !0))
		},
		_compute_item_properties: function(b) {
			var d, c = {};
			for (var e in b.properties)
				if (!0 === b.properties.hasOwnProperty(e)) {
					d = b.properties[e];
					for (var f, g, h, i, j, k = 1e3, l = 0; l < d.settings.length; l++)
						if (f = d.settings[l], f.start_percent <= b.percent_value && b.percent_value < f.end_percent || b.percent_value === f.end_percent && 100 === b.percent_value) {
							f.start_value === f.end_value ? g = f.start_value : (h = (b.percent_value - f.start_percent) / (f.end_percent - f.start_percent) * (f.end_value - f.start_value), void 0 === f.easing || "" === f.easing ? g = f.start_value + h : (i = Math.abs(f.start_value - f.end_value) / k, j = f.start_value < f.end_value ? f.start_value + jQuery.easing[f.easing](null, h, 0, i, f.end_value - f.start_value) * k : f.start_value - jQuery.easing[f.easing](null, -h, 0, i, f.start_value - f.end_value) * k, g = j)), d.computed_value !== g && (d.computed_value = g, c[e] = g, b.redraw = !0);
							break
						}
				}
			b.computed_item_properties = c
		},
		compute_item: function(b) {
			if (!1 !== b._is_initialized) {
				var c = 0;
				tdEvents.window_pageYOffset + tdEvents.window_innerHeight >= b.offset_top && (c = tdEvents.window_pageYOffset > b.offset_bottom_top ? 100 : 100 * (tdEvents.window_pageYOffset + tdEvents.window_innerHeight - b.offset_top) / (tdEvents.window_innerHeight + b.full_height)), b.percent_value !== c && (b.percent_value = c, tdAnimationScroll._compute_item_properties(b)), b.top_is_out = tdEvents.window_pageYOffset > b.offset_top
			}
		},
		compute_all_items: function() {
			!1 === tdAnimationScroll.animation_running && (tdAnimationScroll.rAFIndex = window.requestAnimationFrame(tdAnimationScroll._animate_all_items)), tdAnimationScroll.animation_running = !0
		},
		_animate_all_items: function() {
			for (var b = 0; b < tdAnimationScroll.items.length; b++) !1 === tdAnimationScroll.items[b].computation_stopped && tdAnimationScroll.compute_item(tdAnimationScroll.items[b]);
			for (var c = 0; c < tdAnimationScroll.items.length; c++) !0 === tdAnimationScroll.items[c].redraw && tdAnimationScroll.items[c].animation_callback();
			tdAnimationScroll.animation_running = !1
		},
		td_events_resize: function() {
			0 !== tdAnimationScroll.items.length && (tdAnimationScroll.reinitialize_all_items(!1), tdAnimationScroll.compute_all_items())
		},
		log: function(b) {}
	}, tdAnimationScroll.init()
}();
var tdHomepageFull = {};
! function(a, b) {
	"use strict";
	tdHomepageFull = {
		items: [],
		item: function() {
			this.blockUid = "", this.$tmplBlock = b
		},
		addItem: function(b) {
			if (!tdHomepageFull.items.length) {
				b.$tmplBlock = a("#" + b.blockUid + "_tmpl"), a(".td-header-wrap").after(b.$tmplBlock.html());
				var c = a('<div class="backstretch"></div>'),
					d = a('<img class="td-backstretch not-parallax" src="' + b.postFeaturedImage + '"/>');
				c.append(d), a("body").prepend(c);
				var e = new tdBackstr.item;
				e.wrapper_image_jquery_obj = c, e.image_jquery_obj = d, tdBackstr.add_item(e), b.$article = a("#post-" + b.postId), b.$bgImageWrapper = c, b.backstrItem = e, tdHomepageFull.items.push(b)
			}
		},
		deleteItem: function(a) {
			for (var c = 0; c < tdHomepageFull.items.length; c++) {
				var d = tdHomepageFull.items[c];
				d.blockUid === a && (d.$tmplBlock.remove(), d.$article.remove(), d.$bgImageWrapper.remove(), tdHomepageFull.items.splice(c, 1), tdBackstr.deleteItem(a) && (item.backstrItem = b))
			}
			return !1
		}
	}
}(jQuery);
var tdBackstr = {};
! function() {
	"use strict";
	tdBackstr = {
		items: [],
		item: function() {
			this.blockUid = "", this.previous_value = 0, this.image_aspect_rate = 0, this.wrapper_image_jquery_obj = "", this.image_jquery_obj = ""
		},
		add_item: function(a) {
			a.constructor === tdBackstr.item && (a.image_jquery_obj.get(0).complete ? tdBackstr._load_item_image(a) : a.image_jquery_obj.on("load", function() {
				tdBackstr._load_item_image(a)
			}))
		},
		deleteItem: function(a) {
			for (var b = 0; b < tdBackstr.items.length; b++)
				if (tdBackstr.items[b].blockUid === a) return tdBackstr.items.splice(b, 1), !0;
			return !1
		},
		_load_item_image: function(a) {
			a.image_aspect_rate = a.image_jquery_obj.width() / a.image_jquery_obj.height(), tdBackstr.items.push(a), tdBackstr._compute_item(a), a.image_jquery_obj.css("opacity", "1")
		},
		_compute_item: function(a) {
			var b = a.wrapper_image_jquery_obj.width() / a.wrapper_image_jquery_obj.height(),
				c = 0;
			b < a.image_aspect_rate ? (c = 1, a.previous_value !== c && (a.image_jquery_obj.removeClass("td-stretch-width"), a.image_jquery_obj.addClass("td-stretch-height"), a.previous_value = c)) : (c = 2, a.previous_value !== c && (a.image_jquery_obj.removeClass("td-stretch-height"), a.image_jquery_obj.addClass("td-stretch-width"), a.previous_value = c))
		},
		_compute_all_items: function() {
			for (var a = 0; a < tdBackstr.items.length; a++) tdBackstr._compute_item(tdBackstr.items[a])
		},
		td_events_resize: function() {
			0 !== tdBackstr.items.length && tdBackstr._compute_all_items()
		},
		log: function(a) {
			window.console.log(a)
		}
	}
}();
var tdAnimationStack = {};
! function() {
	"use strict";
	tdAnimationStack = {
		_animation_css_class1: "",
		_animation_css_class2: "",
		_animation_default_effect: "type0",
		activated: !1,
		_ready_for_initialization: !0,
		_ready_init_timeout: void 0,
		max_waiting_for_init: 3e3,
		_specific_selectors: "",
		_general_selectors: "",
		ready_init: function() {
			return tdDetect.isIe8 || tdDetect.isIe9 || jQuery(".vc_images_carousel").length > 0 ? (tdAnimationStack._ready_for_initialization = !1, void(void 0 !== window.td_animation_stack_effect && ("" === window.td_animation_stack_effect && (window.td_animation_stack_effect = tdAnimationStack._animation_default_effect), jQuery("body").removeClass("td-animation-stack-" + window.td_animation_stack_effect)))) : void(void 0 === window.tds_animation_stack || void 0 === window.td_animation_stack_effect ? tdAnimationStack._ready_for_initialization = !1 : (void 0 !== window.td_animation_stack_specific_selectors && (tdAnimationStack._specific_selectors = window.td_animation_stack_specific_selectors), "" === window.td_animation_stack_effect && (window.td_animation_stack_effect = tdAnimationStack._animation_default_effect), tdAnimationStack._animation_css_class1 = "td-animation-stack-" + window.td_animation_stack_effect + "-1", tdAnimationStack._animation_css_class2 = "td-animation-stack-" + window.td_animation_stack_effect + "-2", void 0 !== window.td_animation_stack_general_selectors && (tdAnimationStack._general_selectors = window.td_animation_stack_general_selectors), jQuery(tdAnimationStack._general_selectors).addClass(tdAnimationStack._animation_css_class1), tdAnimationStack._ready_init_timeout = setTimeout(function() {
				!0 !== tdAnimationStack.activated && (tdAnimationStack._ready_for_initialization = !1, void 0 !== window.td_animation_stack_effect && jQuery("body").removeClass("td-animation-stack-" + window.td_animation_stack_effect))
			}, tdAnimationStack.max_waiting_for_init)))
		},
		_ITEM_TO_VIEW_PORT: {
			ITEM_ABOVE_VIEW_PORT: 0,
			ITEM_IN_VIEW_PORT: 1,
			ITEM_UNDER_VIEW_PORT: 2
		},
		SORTED_METHOD: {
			sort_left_to_right: function(b, c) {
				return b.offset_top > c.offset_top ? 1 : b.offset_top < c.offset_top ? -1 : b._order > c._order ? 1 : b._order < c._order ? -1 : 0
			},
			sort_right_to_left: function(b, c) {
				return b.offset_top > c.offset_top ? 1 : b.offset_top < c.offset_top ? -1 : b._order > c._order ? -1 : b._order < c._order ? 1 : -1
			}
		},
		_order: 0,
		interval: 70,
		min_interval: 17,
		max_interval: 40,
		_current_interval: void 0,
		_items_in_view_port: [],
		_items_above_view_port: [],
		items: [],
		item: function() {
			this.offset_top = void 0, this.offset_bottom_to_top = void 0, this.jqueryObj = void 0, this._order = void 0
		},
		_initialize_item: function(a) {
			a._order = tdAnimationStack._order++, a.offset_top = a.jqueryObj.offset().top, a.offset_bottom_to_top = a.offset_top + a.jqueryObj.height()
		},
		check_for_new_items: function(a, b, c) {
			if (!1 !== tdAnimationStack.activated && !1 !== tdAnimationStack._ready_for_initialization) {
				void 0 === a && (a = "");
				var d = [];
				jQuery(tdAnimationStack._general_selectors).not("." + tdAnimationStack._animation_css_class2).addClass(tdAnimationStack._animation_css_class1);
				var e = jQuery(a + ", .post").find(tdAnimationStack._specific_selectors).filter(function() {
					return jQuery(this).hasClass(tdAnimationStack._animation_css_class1)
				});
				e.each(function(a, b) {
						var c = new tdAnimationStack.item;
						c.jqueryObj = jQuery(b), tdAnimationStack.log(a), tdAnimationStack._initialize_item(c), d.push(c)
					}),
					function() {
						for (var a = !0, f = 0; f < d.length; f++)
							if (!1 === e[f].complete) {
								a = !1;
								break
							}
						if (!1 === a) {
							var g = new Date,
								h = g.getTime();
							tdAnimationStack.log("TIMER - started");
							var i = setInterval(function() {
								var f = new Date,
									g = 0;
								if (f.getTime() - h > tdAnimationStack.max_waiting_for_init)
									for (clearInterval(i), g = 0; g < d.length; g++) d[g].jqueryObj.removeClass(tdAnimationStack._animation_css_class1), d[g].jqueryObj.addClass(tdAnimationStack._animation_css_class2);
								else {
									for (a = !0, g = 0; g < d.length; g++)
										if (!1 === e[g].complete) {
											a = !1;
											break
										}!0 === a && (clearInterval(i), tdAnimationStack.log("TIMER - stopped"), tdAnimationStack._precompute_items(d, b, c), tdAnimationStack.compute_items())
								}
							}, 100)
						} else tdAnimationStack._precompute_items(d, b, c), tdAnimationStack.compute_items()
					}(), tdAnimationStack.log("checked for new items finished")
			}
		},
		_precompute_items: function(a, b, c) {
			if (a.sort(b), !0 === c)
				for (; a.length > 0;) tdAnimationStack.log("add item 1 : " + a.length), tdAnimationStack._items_in_view_port.push(a.shift());
			else
				for (; a.length > 0;) tdAnimationStack.log("add item 2 : " + a.length), tdAnimationStack.items.push(a.shift())
		},
		init: function() {
			void 0 !== window.tds_animation_stack && !1 !== tdAnimationStack._ready_for_initialization && (clearTimeout(tdAnimationStack._ready_init_timeout), tdAnimationStack.activated = !0, tdAnimationStack.check_for_new_items(".td-animation-stack", tdAnimationStack.SORTED_METHOD.sort_left_to_right, !1))
		},
		reinit: function() {
			!1 !== tdAnimationStack._ready_for_initialization && (tdAnimationStack.items = [], tdAnimationStack._items_in_view_port = [], tdAnimationStack._items_above_view_port = [], tdAnimationStack.init())
		},
		compute_items: function() {
			if (!1 !== tdAnimationStack.activated && !1 !== tdAnimationStack._ready_for_initialization) {
				for (tdAnimationStack._separate_items(); tdAnimationStack._items_above_view_port.length > 0;) {
					tdAnimationStack.log("animation - above the view port");
					var a = tdAnimationStack._items_above_view_port.shift();
					a.jqueryObj.removeClass(tdAnimationStack._animation_css_class1), a.jqueryObj.addClass(tdAnimationStack._animation_css_class2)
				}
				if (tdAnimationStack._items_in_view_port.length > 0) {
					clearInterval(tdAnimationStack._current_interval);
					var b = tdAnimationStack._get_item_from_view_port();
					b.jqueryObj.removeClass(tdAnimationStack._animation_css_class1), b.jqueryObj.addClass(tdAnimationStack._animation_css_class2), tdAnimationStack._items_in_view_port.length > 0 && (tdAnimationStack.log("start animation timer"), tdAnimationStack._to_timer(tdAnimationStack._get_right_interval(tdAnimationStack.interval * (1 / tdAnimationStack._items_in_view_port.length))))
				}
			}
		},
		_to_timer: function(a) {
			tdAnimationStack._current_interval = setInterval(function() {
				if (tdAnimationStack._items_in_view_port.length > 0) {
					var b = tdAnimationStack._get_item_from_view_port();
					tdAnimationStack.log("animation at interval: " + a), b.jqueryObj.removeClass(tdAnimationStack._animation_css_class1), b.jqueryObj.addClass(tdAnimationStack._animation_css_class2), clearInterval(tdAnimationStack._current_interval), tdAnimationStack._items_in_view_port.length > 0 && tdAnimationStack._to_timer(tdAnimationStack._get_right_interval(tdAnimationStack.interval * (1 / tdAnimationStack._items_in_view_port.length)))
				}
			}, a)
		},
		_get_item_from_view_port: function() {
			return tdAnimationStack._items_in_view_port.shift()
		},
		_get_right_interval: function(a) {
			return a < tdAnimationStack.min_interval ? tdAnimationStack.min_interval : a > tdAnimationStack.max_interval ? tdAnimationStack.max_interval : a
		},
		_item_to_view_port: function(a) {
			return tdAnimationStack.log("position item relative to the view port >> " + tdEvents.window_pageYOffset + tdEvents.window_innerHeight + " : " + a.offset_top), tdEvents.window_pageYOffset + tdEvents.window_innerHeight < a.offset_top ? tdAnimationStack._ITEM_TO_VIEW_PORT.ITEM_UNDER_VIEW_PORT : tdEvents.window_pageYOffset + tdEvents.window_innerHeight >= a.offset_top && tdEvents.window_pageYOffset <= a.offset_bottom_to_top ? tdAnimationStack._ITEM_TO_VIEW_PORT.ITEM_IN_VIEW_PORT : tdAnimationStack._ITEM_TO_VIEW_PORT.ITEM_ABOVE_VIEW_PORT
		},
		_separate_items: function() {
			if (0 !== tdAnimationStack.items.length)
				for (; tdAnimationStack.items.length > 0;) {
					var a = tdAnimationStack._item_to_view_port(tdAnimationStack.items[0]);
					switch (a) {
						case tdAnimationStack._ITEM_TO_VIEW_PORT.ITEM_ABOVE_VIEW_PORT:
							tdAnimationStack._items_above_view_port.push(tdAnimationStack.items.shift());
							break;
						case tdAnimationStack._ITEM_TO_VIEW_PORT.ITEM_IN_VIEW_PORT:
							tdAnimationStack._items_in_view_port.push(tdAnimationStack.items.shift());
							break;
						case tdAnimationStack._ITEM_TO_VIEW_PORT.ITEM_UNDER_VIEW_PORT:
							return void tdAnimationStack.log("after separation items >> above: " + tdAnimationStack._items_above_view_port.length + " in: " + tdAnimationStack._items_in_view_port.length + " under: " + tdAnimationStack.items.length)
					}
				}
		},
		td_events_scroll: function() {
			tdAnimationStack.compute_items()
		},
		td_events_resize: function() {
			clearInterval(tdAnimationStack._current_interval), tdAnimationStack.reinit()
		},
		log: function(a) {}
	}
}(), tdAffix.init({
	menu_selector: ".td-header-menu-wrap",
	menu_wrap_selector: ".td-header-menu-wrap-full",
	tds_snap_menu: tdUtil.getBackendVar("tds_snap_menu"),
	tds_snap_menu_logo: tdUtil.getBackendVar("tds_logo_on_sticky"),
	menu_affix_height: 48,
	menu_affix_height_on_mobile: 54
}), "enabled" == tdUtil.getBackendVar("tds_smart_sidebar") && tdDetect.isIos === !1 && jQuery(window).load(function() {
	if (jQuery(".td-ss-row").each(function() {
			var a = new tdSmartSidebar.item;
			a.sidebar_jquery_obj = jQuery(this).children(".td-pb-span4").find(".wpb_wrapper:first"), a.content_jquery_obj = jQuery(this).children(".td-pb-span8").find(".wpb_wrapper:first"), tdSmartSidebar.add_item(a)
		}), jQuery(".td-ss-main-content").length > 0 && jQuery(".td-ss-main-sidebar").length > 0) {
		var a = new tdSmartSidebar.item;
		a.sidebar_jquery_obj = jQuery(".td-ss-main-sidebar"), a.content_jquery_obj = jQuery(".td-ss-main-content"), tdSmartSidebar.add_item(a)
	}
	tdSmartSidebar.td_events_resize()
}), jQuery(".td-subcat-filter").each(function(a, b) {
	var c = jQuery(b),
		d = c.find(".td-subcat-list:first"),
		e = new tdPullDown.item;
	e.blockUid = c.parent().data("td-block-uid"), e.horizontal_jquery_obj = d, e.vertical_jquery_obj = c.find(".td-subcat-dropdown:first"), e.horizontal_element_css_class = "td-subcat-item", e.container_jquery_obj = d.closest(".td-block-title-wrap"), e.excluded_jquery_elements = [e.container_jquery_obj.find(".td-pulldown-size")], tdPullDown.add_item(e)
}), jQuery(".td-category-siblings").each(function(a, b) {
	var c = jQuery(b),
		d = c.find(".td-category:first"),
		e = new tdPullDown.item;
	e.horizontal_jquery_obj = d, e.vertical_jquery_obj = c.find(".td-subcat-dropdown:first"), e.horizontal_element_css_class = "entry-category", e.container_jquery_obj = d.parents(".td-category-siblings:first"), tdPullDown.add_item(e)
});
var td_backstretch_items = [];
jQuery(window).ready(function() {
	jQuery(".td-backstretch").each(function(a, b) {
		if (!jQuery(b).hasClass("not-parallax")) {
			var c = new tdAnimationScroll.item;
			c.jqueryObj = jQuery(b), c.wrapper_jquery_obj = c.jqueryObj.parent(), tdAnimationScroll.add_item(c), td_backstretch_items.push(c), td_compute_backstretch_item(c)
		}
	}), jQuery(".td-parallax-header").each(function(a, b) {
		var c = new tdAnimationScroll.item;
		c.jqueryObj = jQuery(b), c.add_item_property("move_y", 50, 100, 0, 100, ""), c.add_item_property("opacity", 50, 100, 1, 0, ""), c.animation_callback = function() {
			var a = parseFloat(c.computed_item_properties.move_y).toFixed(),
				b = parseFloat(c.computed_item_properties.opacity);
			c.jqueryObj.css({
				"-webkit-transform": "translate3d(0px," + a + "px, 0px)",
				transform: "translate3d(0px," + a + "px, 0px)"
			}), c.jqueryObj.css("transform", "translate3d(0px," + a + "px, 0px)"), c.jqueryObj.css("opacity", b), c.redraw = !1
		}, tdAnimationScroll.add_item(c)
	}), tdAnimationScroll.compute_all_items(), tdAnimationStack.ready_init()
});
var tdAjaxLoop = {};
! function() {
	"use strict";
	tdAjaxLoop = {
		loopState: {
			sidebarPosition: "",
			moduleId: 1,
			currentPage: 1,
			max_num_pages: 0,
			atts: {},
			ajax_pagination_infinite_stop: 0,
			server_reply_html_data: ""
		},
		init: function() {
			jQuery(".td-ajax-loop-infinite").each(function() {
				var a = new tdInfiniteLoader.item;
				a.jqueryObj = jQuery(this), a.uid = "tdAjaxLoop", a.isVisibleCallback = function() {
					0 !== tdAjaxLoop.loopState.ajax_pagination_infinite_stop && tdAjaxLoop.loopState.currentPage >= tdAjaxLoop.loopState.ajax_pagination_infinite_stop && tdAjaxLoop.loopState.currentPage + 1 < tdAjaxLoop.loopState.max_num_pages ? jQuery(".td-load-more-infinite-wrap").css("display", "block").css("visibility", "visible") : tdAjaxLoop.infiniteNextPage(!1)
				}, tdInfiniteLoader.addItem(a)
			}), jQuery(".td-load-more-infinite-wrap").click(function(a) {
				a.preventDefault(), jQuery(".td-load-more-infinite-wrap").css("visibility", "hidden"), tdAjaxLoop.infiniteNextPage(!0)
			})
		},
		infiniteNextPage: function(a) {
			if (tdAjaxLoop.loopState.currentPage++, tdAjaxLoop.loopState.server_reply_html_data = "", !(tdAjaxLoop.loopState.currentPage > tdAjaxLoop.loopState.max_num_pages)) {
				jQuery(".td-ss-main-content").append('<div class="td-loader-gif td-loader-infinite td-loader-animation-start"></div>'), tdLoadingBox.init(tds_theme_color_site_wide, 45), setTimeout(function() {
					jQuery(".td-loader-gif").removeClass("td-loader-animation-start").addClass("td-loader-animation-mid")
				}, 50);
				var b = {
					action: "td_ajax_loop",
					loopState: tdAjaxLoop.loopState
				};
				jQuery.ajax({
					type: "POST",
					url: td_ajax_url,
					cache: !0,
					data: b,
					success: function(b, c, d) {
						tdAjaxLoop._processAjaxRequest(b, a)
					},
					error: function(a, b, c) {}
				})
			}
		},
		_processAjaxRequest: function(a, b) {
			jQuery(".td-loader-gif").remove(), tdLoadingBox.stop();
			var c = jQuery.parseJSON(a);
			return "" === c.server_reply_html_data ? void jQuery(".td-load-more-infinite-wrap").css("visibility", "hidden") : (jQuery(".td-ajax-loop-infinite").before(c.server_reply_html_data), parseInt(c.currentPage) >= parseInt(c.max_num_pages) ? jQuery(".td-load-more-infinite-wrap").css("visibility", "hidden") : !0 === b && jQuery(".td-load-more-infinite-wrap").css("visibility", "visible"), setTimeout(function() {
				tdAnimationStack.check_for_new_items(".td-main-content .td-animation-stack", tdAnimationStack.SORTED_METHOD.sort_left_to_right, !0)
			}, 200), void(!0 !== b && (setTimeout(function() {
				tdInfiniteLoader.computeTopDistances(), tdInfiniteLoader.enable_is_visible_callback("tdAjaxLoop")
			}, 500), setTimeout(function() {
				tdInfiniteLoader.computeTopDistances()
			}, 1e3), setTimeout(function() {
				tdInfiniteLoader.computeTopDistances()
			}, 1500))))
		}
	}
}();
var tdWeather = {};
! function() {
	"use strict";
	tdWeather = {
		_icons: {
			"01d": "clear-sky-d",
			"02d": "few-clouds-d",
			"03d": "scattered-clouds-d",
			"04d": "broken-clouds-d",
			"09d": "shower-rain-d",
			"10d": "rain-d",
			"11d": "thunderstorm-d",
			"13d": "snow-d",
			"50d": "mist-d",
			"01n": "clear-sky-n",
			"02n": "few-clouds-n",
			"03n": "scattered-clouds-n",
			"04n": "broken-clouds-n",
			"09n": "shower-rain-n",
			"10n": "rain-n",
			"11n": "thunderstorm-n",
			"13n": "snow-n",
			"50n": "mist-n"
		},
		_currentRequestInProgress: !1,
		_currentItem: "",
		_currentLatitude: 0,
		_currentLongitude: 0,
		_currentPositionCacheKey: "",
		_currentLocationCacheKey: "",
		_currentLocation: "",
		items: [],
		_is_location_open: !1,
		init: function() {
			jQuery(".td-icons-location").click(function() {
				if (tdWeather._currentRequestInProgress !== !0) {
					tdWeather._currentRequestInProgress = !0, tdWeather._currentItem = tdWeather._getItemByBlockID(jQuery(this).data("block-uid"));
					var a = 1e7;
					navigator.geolocation && navigator.geolocation.getCurrentPosition(tdWeather._updateLocationCallback, tdWeather._displayLocationApiError, {
						enableHighAccuracy: !0,
						timeout: a,
						maximumAge: 6e5
					}), tdWeather._currentRequestInProgress = !1
				}
			}), jQuery(".td-weather-now").click(function() {
				tdWeather._currentRequestInProgress !== !0 && (tdWeather._currentRequestInProgress = !0, tdWeather._currentItem = tdWeather._getItemByBlockID(jQuery(this).data("block-uid")), 1 === tdWeather._currentItem.current_unit ? tdWeather._currentItem.current_unit = 0 : tdWeather._currentItem.current_unit = 1, tdWeather._renderCurrentItem())
			}), jQuery(".td-manual-location-form").submit(function(a) {
				a.preventDefault(), tdWeather._currentRequestInProgress !== !0 && (tdWeather._currentRequestInProgress = !0, tdWeather._currentItem = tdWeather._getItemByBlockID(jQuery(this).data("block-uid")), tdWeather._currentLocation = jQuery("input#" + jQuery(this).data("block-uid")).val(), tdWeather._updateLocationCallback2(tdWeather._currentLocation), tdWeather._currentRequestInProgress = !1, tdWeather._hide_manual_location_form())
			}), jQuery(document).click(function(a) {
				tdWeather._is_location_open === !0 && jQuery(a.target).hasClass("td-location-set-input") !== !0 && jQuery(a.target).hasClass("td-location-set-button") !== !0 && tdWeather._hide_manual_location_form()
			})
		},
		addItem: function(a) {
			tdWeather.items.push(a)
		},
		_updateLocationCallback: function(a) {
			if (tdWeather._currentLatitude = a.coords.latitude, tdWeather._currentLongitude = a.coords.longitude, tdWeather._currentPositionCacheKey = a.coords.latitude + "_" + a.coords.longitude, tdLocalCache.exist(tdWeather._currentPositionCacheKey + "_today")) tdWeather._owmGetTodayDataCallback(tdLocalCache.get(tdWeather._currentPositionCacheKey + "_today"));
			else {
				var b = "http://api.openweathermap.org/data/2.5/weather?lat=" + tdWeather._currentLatitude + "&lon=" + tdWeather._currentLongitude + "&units=metric&lang=" + tdWeather._currentItem.api_language + "&appid=" + tdWeather._currentItem.api_key;
				jQuery.ajax({
					dataType: "jsonp",
					url: b,
					success: tdWeather._owmGetTodayDataCallback,
					cache: !0
				})
			}
		},
		_owmGetTodayDataCallback: function(a) {
			if (tdLocalCache.set(tdWeather._currentPositionCacheKey + "_today", a), tdWeather._currentItem.api_location = a.name, tdWeather._currentItem.today_clouds = tdUtil.round(a.clouds.all), tdWeather._currentItem.today_humidity = tdUtil.round(a.main.humidity), tdWeather._currentItem.today_icon = tdWeather._icons[a.weather[0].icon], tdWeather._currentItem.today_icon_text = a.weather[0].description, tdWeather._currentItem.today_max[0] = tdUtil.round(a.main.temp_max, 1), tdWeather._currentItem.today_max[1] = tdWeather._celsiusToFahrenheit(a.main.temp_max), tdWeather._currentItem.today_min[0] = tdUtil.round(a.main.temp_min, 1), tdWeather._currentItem.today_min[1] = tdWeather._celsiusToFahrenheit(a.main.temp_min), tdWeather._currentItem.today_temp[0] = tdUtil.round(a.main.temp, 1), tdWeather._currentItem.today_temp[1] = tdWeather._celsiusToFahrenheit(a.main.temp), tdWeather._currentItem.today_wind_speed[0] = tdUtil.round(a.wind.speed, 1), tdWeather._currentItem.today_wind_speed[1] = tdWeather._kmphToMph(a.wind.speed), tdLocalCache.exist(tdWeather._currentPositionCacheKey)) tdWeather._owmGetFiveDaysData(tdLocalCache.get(tdWeather._currentPositionCacheKey));
			else {
				var b = "http://api.openweathermap.org/data/2.5/forecast/daily?lat=" + tdWeather._currentLatitude + "&lon=" + tdWeather._currentLongitude + "&units=metric&lang=" + tdWeather._currentItem.api_language + "&appid=" + tdWeather._currentItem.api_key;
				jQuery.ajax({
					dataType: "jsonp",
					url: b,
					success: tdWeather._owmGetFiveDaysData,
					cache: !0
				})
			}
		},
		_owmGetFiveDaysData: function(a) {
			tdLocalCache.set(tdWeather._currentPositionCacheKey, a);
			for (var b = 0; b < tdWeather._currentItem.forecast.length; b++) {
				var c = tdWeather._currentItem.forecast[b];
				c.day_temp[0] = tdUtil.round(a.list[c.owm_day_index].temp.day), c.day_temp[1] = tdWeather._celsiusToFahrenheit(c.day_temp[0])
			}
			tdWeather._renderCurrentItem()
		},
		_renderCurrentItem: function() {
			var a = jQuery("#" + tdWeather._currentItem.block_uid),
				b = tdWeather._currentLatitude,
				c = tdWeather._currentLongitude,
				d = tdWeather._currentLocation;
			a.find(".td-weather-city").html(tdWeather._currentItem.api_location), "" === d && 0 === b && 0 === c && a.find(".td-weather-city").html(tdWeather._currentItem.location), a.find(".td-weather-condition").html(tdWeather._currentItem.today_icon_text);
			var e = a.find(".td-w-today-icon");
			e.removeClass(), e.addClass("td-w-today-icon"), e.addClass(tdWeather._currentItem.today_icon);
			var f = tdWeather._currentItem.current_unit,
				g = "kmh",
				h = "C";
			1 === f && (g = "mph", h = "F"), a.find(".td-big-degrees").html(tdWeather._currentItem.today_temp[f]), a.find(".td-weather-unit").html(h), a.find(".td-w-high-temp").html(tdWeather._currentItem.today_max[f]), a.find(".td-w-low-temp").html(tdWeather._currentItem.today_min[f]), a.find(".td-w-today-humidity").html(tdWeather._currentItem.today_humidity + "%"), a.find(".td-w-today-wind-speed").html(tdWeather._currentItem.today_wind_speed[f] + g), a.find(".td-w-today-clouds").html(tdWeather._currentItem.today_clouds + "%");
			for (var i = 0; i < tdWeather._currentItem.forecast.length; i++) a.find(".td-degrees-" + i).html(tdWeather._currentItem.forecast[i].day_temp[f]);
			tdWeather._currentRequestInProgress = !1
		},
		_getItemByBlockID: function(a) {
			for (var b = 0; b < tdWeather.items.length; b++)
				if (tdWeather.items[b].block_uid === a) return tdWeather.items[b];
			return !1
		},
		_displayLocationApiError: function(a) {
			if (1 === a.code) {
				if (tdDetect.isAndroid) tdWeather._show_manual_location_form();
				else if (tdDetect.isIos) return void alert("Please enable Location services for Safari Websites and reload the page. \n ---------------------- \nSettings > Privacy > Location Services");
				tdWeather._show_manual_location_form()
			}
			tdWeather._show_manual_location_form()
		},
		_celsiusToFahrenheit: function(a) {
			var b = 9 * a / 5 + 32,
				c = tdUtil.round(b, 1);
			return c > 99.9 ? tdUtil.round(b) : c
		},
		_kmphToMph: function(a) {
			return tdUtil.round(.621371192 * a, 1)
		},
		_show_manual_location_form: function() {
			tdWeather._currentItem = tdWeather._getItemByBlockID(tdWeather._currentItem.block_uid), jQuery("#" + tdWeather._currentItem.block_uid).find(".td-weather-set-location").addClass("td-show-location"), jQuery(".td-manual-location-form input").focus(), tdWeather._is_location_open = !0
		},
		_hide_manual_location_form: function() {
			jQuery("#" + tdWeather._currentItem.block_uid).find(".td-weather-set-location").removeClass("td-show-location"), tdWeather._is_location_open = !1
		},
		_updateLocationCallback2: function(a) {
			if (tdWeather._currentLocationCacheKey = a, tdLocalCache.exist(tdWeather._currentLocationCacheKey + "_today")) tdWeather._owmGetTodayDataCallback2(tdLocalCache.get(tdWeather._currentLocationCacheKey + "_today"));
			else {
				var b = "http://api.openweathermap.org/data/2.5/weather?q=" + encodeURIComponent(a) + "&lang=" + tdWeather._currentItem.api_language + "&units=metric&appid=" + tdWeather._currentItem.api_key;
				jQuery.ajax({
					dataType: "jsonp",
					url: b,
					success: tdWeather._owmGetTodayDataCallback2,
					cache: !0
				})
			}
		},
		_owmGetTodayDataCallback2: function(a) {
			if (tdLocalCache.set(tdWeather._currentLocationCacheKey + "_today", a), tdWeather._currentItem.api_location = a.name, tdWeather._currentItem.today_clouds = tdUtil.round(a.clouds.all), tdWeather._currentItem.today_humidity = tdUtil.round(a.main.humidity), tdWeather._currentItem.today_icon = tdWeather._icons[a.weather[0].icon], tdWeather._currentItem.today_icon_text = a.weather[0].description, tdWeather._currentItem.today_max[0] = tdUtil.round(a.main.temp_max, 1), tdWeather._currentItem.today_max[1] = tdWeather._celsiusToFahrenheit(a.main.temp_max), tdWeather._currentItem.today_min[0] = tdUtil.round(a.main.temp_min, 1), tdWeather._currentItem.today_min[1] = tdWeather._celsiusToFahrenheit(a.main.temp_min), tdWeather._currentItem.today_temp[0] = tdUtil.round(a.main.temp, 1), tdWeather._currentItem.today_temp[1] = tdWeather._celsiusToFahrenheit(a.main.temp), tdWeather._currentItem.today_wind_speed[0] = tdUtil.round(a.wind.speed, 1), tdWeather._currentItem.today_wind_speed[1] = tdWeather._kmphToMph(a.wind.speed), tdLocalCache.exist(tdWeather._currentLocationCacheKey)) tdWeather._owmGetFiveDaysData2(tdLocalCache.get(tdWeather._currentLocationCacheKey));
			else {
				var b = "http://api.openweathermap.org/data/2.5/forecast/daily?q=" + tdWeather._currentItem.api_location + "&lang=" + tdWeather._currentItem.api_language + "&units=metric&cnt=7&appid=" + tdWeather._currentItem.api_key;
				jQuery.ajax({
					dataType: "jsonp",
					url: b,
					success: tdWeather._owmGetFiveDaysData2,
					cache: !0
				})
			}
		},
		_owmGetFiveDaysData2: function(a) {
			tdLocalCache.set(tdWeather._currentLocationCacheKey, a);
			for (var b = 0, c = 0; c < a.list.length; c++) {
				var d = a.list[c].dt,
					f = (td_date_i18n("Y m d, H:i a, T", d), td_date_i18n("Ymd")),
					g = td_date_i18n("Ymd", d);
				f >= g && (b += 1)
			}
			for (var h = 0; h < tdWeather._currentItem.forecast.length; h++) {
				var i = tdWeather._currentItem.forecast[h];
				if (0 === h && b > 1 && i.owm_day_index < 2) var j = !0;
				if (j && (i.owm_day_index = i.owm_day_index + 1), 0 === h && b < 2 && i.owm_day_index > 1) var k = !0;
				k && (i.owm_day_index = i.owm_day_index - 1), i.day_temp[0] = tdUtil.round(a.list[i.owm_day_index].temp.day), i.day_temp[1] = tdWeather._celsiusToFahrenheit(i.day_temp[0])
			}
			tdWeather._renderCurrentItem()
		}
	}
}(), tdWeather.init(), jQuery(window).load(function() {
	"use strict";
	jQuery("body").addClass("td-js-loaded"), window.tdAnimationStack.init()
}), jQuery(window).ready(function() {
	"use strict";
	jQuery(".td_smart_list_1 a, .td_smart_list_3 a").click(function(a) {
		if (a.target === a.currentTarget) {
			var b = jQuery(this).attr("target"),
				c = jQuery(this)[0].hasAttribute("download"),
				d = jQuery(this).attr("href");
			c || ("_blank" == b ? (a.preventDefault(), window.open(d)) : window.location.href !== d && tdUtil.isValidUrl(d) && (window.location.href = d))
		}
	}), jQuery(".td_block_trending_now").each(function() {
		var a = new tdTrendingNow.item,
			b = jQuery(this).find(".td-trending-now-wrapper"),
			c = b.data("start"),
			d = 0;
		a.blockUid = jQuery(this).data("td-block-uid"), "manual" !== c && (a.trendingNowAutostart = c), jQuery("#" + a.blockUid + " .td-trending-now-post").each(function() {
			a.trendingNowPosts[d] = jQuery(this), d++
		}), tdTrendingNow.addItem(a)
	}), jQuery(".td-trending-now-nav-left").on("click", function(a) {
		a.preventDefault();
		var b = jQuery(this).data("block-id");
		tdTrendingNow.itemPrev(b)
	}), jQuery(".td-trending-now-nav-right").on("click", function(a) {
		a.preventDefault();
		var b = jQuery(this).data("block-id");
		tdTrendingNow.itemNext(b)
	})
});
var tdAnimationSprite = {};
! function() {
	"use strict";
	tdAnimationSprite = {
		items: [],
		isInRequestAnimation: !1,
		item: function() {
			this.blockUid = "", this._isInitialized = !1, this.paused = !1, this.automatStart = !0, this.properties = [], this.readyToAnimate = !1, this.nextFrame = 1, this.interval = void 0, this.jqueryObj = void 0, this.animationSpriteClass = void 0, this._currentDirection = "right", this._executedLoops = 0, this._prop_background_position = void 0, this.frames = void 0, this.frameWidth = void 0, this.velocity = void 0, this.reverse = void 0, this.loops = void 0, this.animate = function() {
				this._prop_background_position = -1 * this.nextFrame * this.frameWidth + "px 0", this.readyToAnimate = !0, !0 === this.reverse ? "right" === this._currentDirection ? this.nextFrame === this.frames - 1 ? (this._currentDirection = "left", this.nextFrame--) : this.nextFrame++ : "left" === this._currentDirection && (0 === this.nextFrame ? (this._currentDirection = "right", this.nextFrame++, this._executedLoops++, 0 !== this.loops && this._executedLoops === this.loops && clearInterval(this.interval)) : this.nextFrame--) : this.nextFrame === this.frames - 1 ? (this._executedLoops++, 0 !== this.loops && this._executedLoops === this.loops && clearInterval(this.interval), this.nextFrame = 0) : this.nextFrame++, !1 === tdAnimationSprite.isInRequestAnimation && (tdAnimationSprite.isInRequestAnimation = !0, window.requestAnimationFrame(tdAnimationSprite.animateAllItems))
			}
		},
		_initializeItem: function(a) {
			if (!0 !== a._isInitialized) {
				var b = /(td_animation_sprite\S*)/gi,
					c = a.jqueryObj.attr("class").match(b);
				if (null !== c) {
					a.offsetTop = a.jqueryObj.offset().top, a.offsetBottomToTop = a.offsetTop + a.jqueryObj.height(), a.animationSpriteClass = c[c.length - 1];
					var d = a.animationSpriteClass.split("-");
					7 === d.length && (a.frames = parseInt(d[1]), a.frameWidth = parseInt(d[2]), a.velocity = parseInt(d[3]), a.loops = parseInt(d[4]), 1 === parseInt(d[5]) ? a.reverse = !0 : a.reverse = !1, 1 === parseInt(d[6]) ? a.automatStart = !0 : a.automatStart = !1, a._isInitialized = !0)
				}
			}
		},
		addItem: function(a) {
			a.constructor === tdAnimationSprite.item && (tdAnimationSprite.items.push(a), tdAnimationSprite._initializeItem(a), !0 === a.automatStart && tdAnimationSprite.computeItem(a))
		},
		deleteItem: function(a) {
			for (var b = 0; b < tdAnimationSprite.items.length; b++)
				if (tdAnimationSprite.items[b].blockUid === a) return tdAnimationSprite.items.splice(b, 1), !0;
			return !1
		},
		computeItem: function(a) {
			if (a.frames > 1) {
				if (void 0 !== a.interval) return;
				a.interval = setInterval(function() {
					!1 === a.paused && a.animate()
				}, a.velocity)
			}
		},
		recomputeItem: function(a) {
			clearInterval(a.interval), a.interval = void 0, a._isInitialized = !1, tdAnimationSprite._initializeItem(a), tdAnimationSprite.computeItem(a)
		},
		stopItem: function(a) {
			a.constructor === tdAnimationSprite.item && !0 === a._isInitialized && (clearInterval(a.interval), a.interval = void 0)
		},
		startItem: function(a) {
			a.constructor === tdAnimationSprite.item && !0 === a._isInitialized && (a.paused = !1)
		},
		pauseItem: function(a) {
			a.constructor === tdAnimationSprite.item && !0 === a._isInitialized && (a.paused = !0)
		},
		computeAllItems: function() {
			for (var a = 0; a < tdAnimationSprite.items.length; a++) tdAnimationSprite.computeItem(tdAnimationSprite.items[a])
		},
		recomputeAllItems: function() {
			for (var a = 0; a < tdAnimationSprite.items.length; a++) tdAnimationSprite.recomputeItem(tdAnimationSprite.items[a])
		},
		stopAllItems: function() {
			for (var a = 0; a < tdAnimationSprite.items.length; a++) tdAnimationSprite.stopItem(tdAnimationSprite.items[a])
		},
		pauseAllItems: function() {
			for (var a = 0; a < tdAnimationSprite.items.length; a++) tdAnimationSprite.pauseItem(tdAnimationSprite.items[a])
		},
		startAllItems: function() {
			for (var a = 0; a < tdAnimationSprite.items.length; a++) tdAnimationSprite.startItem(tdAnimationSprite.items[a])
		},
		animateAllItems: function() {
			for (var a, b = 0; b < tdAnimationSprite.items.length; b++) a = tdAnimationSprite.items[b], !0 === a.readyToAnimate && (a.jqueryObj.css("background-position", a._prop_background_position), a.readyToAnimate = !1);
			tdAnimationSprite.isInRequestAnimation = !1
		}
	};
	for (var a = jQuery('span[class^="td_animation_sprite"]'), b = 0; b < a.length; b++) {
		var c = new tdAnimationSprite.item;
		c.jqueryObj = jQuery(a[b]), c.blockUid = c.jqueryObj.data("td-block-uid"), tdAnimationSprite.addItem(c)
	}
}();