﻿jQuery(document).ready(function () {
    var IDSong;
    //update information guest 
    jQuery(document).on("submit", ".frm-update_infor", function () {
        var link = jQuery(this).data("link");
        AjaxCallServer(link, this);
    });
    //add music to play list
    jQuery('.quickAddPlaylist').on('click', function () {
        GetViewPlayListForGuest(jQuery('#md-add-playlist').data("link"));
        jQuery('#md-add-playlist').modal('show');
        IDSong = jQuery(this).data("id");
    });
    //create play list
    jQuery(document).on("submit", ".frm-create_playlist", function () {
        var link = jQuery(this).data("link");
        AjaxCallServer(link, this);
    });
    jQuery('.quickCreatePlayList').on("click", function () {
        var link = jQuery(this).data("link");
        AjaxCallServer(link, $('.playList-Value'));
        jQuery('input[name="Name"]').val('');
        GetViewPlayListForGuest(jQuery('#md-add-playlist').data("link"));
    });
    //edit playlist 
    jQuery('.updatePlayList').on("click", function () {
        var dataItem = jQuery(this).data("value");
        jQuery("input[name='ID']").val(dataItem.ID);
        jQuery("input[name='Name']").val(dataItem.Name);
        jQuery("select[name='IDCategory']").val(dataItem.IDCategory);
        jQuery("textarea[name='Describe']").val(dataItem.Describe)
        jQuery('#UpdateAvPlayList').attr("src", dataItem.ThumbPath);
        jQuery('#md-playlist-edit').modal('show');
    });
    //reload content modal playlist 
    jQuery(document).on("submit", ".frm-update_playlist", function () {
        var link = jQuery(this).data("link");
        AjaxCallServer(link, this);
    });
    //set active tab-playlist-upload
    jQuery('.tab-playlist-active').on('click', function () {
        jQuery('.tab-playlist-active').css('background', '#fff');
        jQuery('.tab-playlist-active').css('color', '#333');
        jQuery(this).css("background", "#ff5500");
        jQuery(this).css("color", "#fff");
    });
    //set active class for bxh
    jQuery('.tab_national_rank_select').on('click', 'li', function () {
        jQuery('.tab_national_rank_select li a.active').removeClass('active');
        jQuery(this).find("a").addClass('active');
    });

    jQuery('.music-nav').on('click', 'li', function () {
        jQuery('.music-nav li a.active').removeClass('active');
        jQuery(this).find("a").addClass('active');
        var myString = $("input").val().split("-{}.html").pop();
    });
    //delete
    jQuery('.deletePlayList').on("click", function () {
        var dataItem = jQuery(this).data("value");
        jQuery("input[name='ID']").val(dataItem.ID);
        jQuery('#md-playlist-delete').modal('show');
    });

    jQuery(document).on("submit", ".frm-delete_playlist", function () {
        var link = jQuery(this).data("link");
        AjaxCallServer(link, this);
    });
    //add song to play list 
    jQuery(document).on("click", '.quickAddToPlayList', function () {
        
        var playList = {
            'IDSong': IDSong, 'IDPlayList': jQuery(this).data("id"),
        };
        console.log(playList);
        jQuery.ajax({
            url: jQuery(this).data("link"),
            type: "POST",
            contentType: 'application/json; charset=utf-8',
            dataType: 'json',
            data: JSON.stringify(playList),
            success: function (data) {
                console.log(data);
            },
            error: function (ex) {
                console.log(ex);
            }
        });
    });

    jQuery("input[name='ThumbPath']").ajaxUploader({
        name: "FileDocument",
        postUrl: "http://emico.docpro.vn/uploader/upfile",
        onBegin: function (e, dragBox) {
            return true;
        },
        onClientLoadStart: function (e, file, dragBox) {
        },
        onClientProgress: function (e, file) {
        },
        onServerProgress: function (e, file, dragBox) {
        },
        onClientAbort: function (e, file) {
        },
        onClientError: function (e, file) {
        },
        onServerAbort: function (e, file, dragBox) {
        },
        onServerError: function (e, file, dragBox) {
        },
        onSuccess: function (e, file, dragBox, data) {
            jQuery('#UpdateGuestImg').attr("src", Cdata.Storage.domain + data.FilePath);
            jQuery("input[name='ThumbPath']").val(data.FilePath);
            jQuery("input[name='ThumbName']").val(data.FileName);
        }
    });
    //hiden 
    if (jQuery("#divDescription").height() > 50) {
        jQuery(".singer_profile").css({ "height": "110px" });
        jQuery("#seeMoreDescription").removeClass("hide");
        jQuery("#hideMoreDescription").addClass("hide");
    } else {
        jQuery("#seeMoreDescription").addClass("hide");
        jQuery("#hideMoreDescription").addClass("hide");
        jQuery("#dotedDetail").html("");
    }
    jQuery("#seeMoreDescription").unbind().click(function () {
        jQuery(".singer_profile").css({ "height": "auto" });
        jQuery("#seeMoreDescription").addClass("hide");
        jQuery("#hideMoreDescription").removeClass("hide");
        jQuery("#dotedDetail").html("");
    });
    jQuery("#hideMoreDescription").unbind().click(function () {
        jQuery(".singer_profile").css({ "height": "110px" });
        jQuery("#seeMoreDescription").removeClass("hide");
        jQuery("#hideMoreDescription").addClass("hide");
        jQuery("#dotedDetail").html("(...)");
    });
    //fuction post data to server
    function AjaxCallServer(link, idContainer) {
        jQuery.ajax({
            url: link,
            type: "POST",
            data: Utils.getSerialize(jQuery(idContainer)),
            success: function (data) {
                Utils.setMessage(data.message);
            },
            error: function (ex) {
                console.log(ex);
            }
        });
        function AjaxCallServer(link, idContainer) {
            jQuery.ajax({
                url: link,
                type: "POST",
                data: Utils.getSerialize(jQuery(idContainer)),
                success: function (data) {
                    Utils.setMessage(data.message);
                },
                error: function (ex) {
                    console.log(ex);
                }
            });
        };
    };

    function GetViewPlayListForGuest(link) {
        jQuery.ajax({
            url: link,
            type: "POST",
            success: function (html) {
                jQuery(".view-playlist-item").html(html);
            },
            error: function (ex) {
            }
        });
    };
});
