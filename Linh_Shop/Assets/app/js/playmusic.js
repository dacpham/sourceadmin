﻿jQuery(document).ready(function () {
    // init---------
    init();
    //--------------
    //Function
    $(document).on("click", ".control-prev", function () {
        Run('.control-play', ".control-music", 'prev')
    });
    $(document).on("click", ".control-next", function () {
        Run('.control-play', ".control-music", 'next')
    });
    $(document).on("click", ".control-play", function () {
        Run('.control-play', ".control-music", 'play')
    });

    //------Method

    function updateTrackTime(track) {

        var currTimeDiv = $("#currentTime");
        var durationDiv = $("#duration");
        var currTime = Math.floor(track.currentTime).toString();
        var duration = Math.floor(track.duration).toString();

        currTimeDiv.html( formatSecondsAsTime(currTime));

        if (isNaN(duration)) {
            durationDiv.html ('00:00');
        }
        else {
            durationDiv.html('/'+ formatSecondsAsTime(duration));
        }
    }
    function formatSecondsAsTime(secs, format) {
        var hr = Math.floor(secs / 3600);
        var min = Math.floor((secs - (hr * 3600)) / 60);
        var sec = Math.floor(secs - (hr * 3600) - (min * 60));

        if (min < 10) {
            min = "0" + min;
        }
        if (sec < 10) {
            sec = "0" + sec;
        }

        return min + ':' + sec;
    }
    function Run(main,ctrmusic , type)
    {
        var types=['audio/ogg','audio/mpeg'];
        var controlms = $(ctrmusic);
        var audio = controlms.data("audio");
        var maxlst = controlms.data("maxlist");
        var list = controlms.data("list");
        var clst = controlms.data("clst"); // bài hát hiện tại
        var nt = controlms.data("nghetiep");
        if(type=='next')
        {
            if(clst==maxlst) // bài hát hiện tại là bài hát cuối của list
            {
                if (nghetiep(nt))
                    return;
                clst = 1;
            }
            else
            {
                clst = clst + 1;
            }
            setplayplause(true, main);
            playmusic(clst, controlms);
        }
        else if(type =='prev')
        {
            if (clst == 1) // bài hát hiện tại là bài hát cuối của list
            {
                clst = 1;
            }
            else {
                clst = clst - 1;
            }
            setplayplause(true, main);
            playmusic(clst, controlms);
        }
        else {
            setplaytoogle(main, audio) == true ? $(audio).trigger("play") : $(audio).trigger("pause");
        }
    }
    
    function playmusic(clst, controlms)
    {
        var audio = controlms.data("audio");
        var list = controlms.data("list");
        var hitm = ".hitm" + clst;
        $(list).find('.hpost .music-button a.fa-pause').addClass('fa-play').removeClass('fa-pause') 
        $(list).find(hitm + ' a.fa-play').removeClass('fa-play').addClass('fa-pause');
        var link = $(list).find(hitm).data("link");
        controlms.data("clst", clst);
        setactivelst(hitm, list);
        $(audio).attr("src", link).trigger("play");
        sethiddennextprev(controlms);
    }
    function setplaytoogle(main,audio)
    {
        if (document.getElementById($(audio).attr("id")).paused) {
            setplayplause(true, main)
            return true;
        }
        else {
            setplayplause(false, main);
            return false;
        }
    }
    function setplayplause(isplay, main)
    {
        if(isplay)
        {
            $(main).removeClass("fa-play");
            $(main).addClass("fa-pause");
        }
        else
        {
            $(main).removeClass("fa-pause");
            $(main).addClass("fa-play");
        }
    }
    function sethiddennextprev(controlms)
    {
        var maxlst = controlms.data("maxlist");
        var clst = controlms.data("clst");
        if (maxlst == 1) {
            $(controlms).find('.control-prev').addClass("hidden");
            $(controlms).find('.control-next').addClass("hidden");
        }
        else {
            if (clst == 1) {
                $(controlms).find('.control-next').removeClass("hidden");
                $(controlms).find('.control-prev').removeClass("hidden");
                $(controlms).find('.control-prev').addClass("hidden");
            }
            else if (clst == maxlst) {
                $(controlms).find('.control-next').removeClass("hidden");
                $(controlms).find('.control-prev').removeClass("hidden");
                $(controlms).find('.control-next').addClass("hidden");
            }
            else {
                $(controlms).find('.control-next').removeClass("hidden");
                $(controlms).find('.control-prev').removeClass("hidden");
            }
        }
    }
    //
    function setactivelst(item, list)
    {
        $(list).find('.hpost').each(function () {
            $(this).removeClass('active');
            $(this).find('img').addClass('hidden');
            $(this).find('.stt').removeClass('hidden');
        })
        $(list).find(item).addClass('active');
        $(list).find(item).find('img').removeClass('hidden');
        $(list).find(item).find('.stt').addClass('hidden');

    }
    //function volumn
    function refreshVolumn() {
        var value = $("#music-volum").slider("value");
        var audio = $("#music-volum").data("audio");
        var icon = $("#music-volum").data("icon");
        setValueVolumn(value, audio,icon);
    }
    function setValueVolumn(value, audio, icon)
    {
        if (value == 0)
        {
            setVolumnMute(icon);
        }
        else if (value < 20)
        {
            setVolumnDown(icon);
        }
        else {
            setVolumnUp(icon);
        }
        $(audio).prop('volume', value/100);
    }
    function setVolumnUp(icon)
    {
        $(icon).removeClass();
        $(icon).addClass("fa fa-volume-up");
    }
    function setVolumnDown(icon) {
        $(icon).removeClass();
        $(icon).addClass("fa fa-volume-down");
    }
    function setVolumnMute(icon) {
        $(icon).removeClass();
        $(icon).addClass("fa fa-volume-off");
    }
    // end function volumn 
    //function Processbar
    function refreshProcess() {
        var value = $("#music-process").slider("value");
        var audio = $("#music-process").data("audio");
    }
    function setProcessBar(value,audio)
    {
        var controlms = $(audio).get(0);
        var duration = $(audio).get(0).duration;
        controlms.currentTime =  duration * (value/10000);
    }
    // end function volumn refreshProcess
    // nghe tiếp
    function nghetiep (main)
    {
        if ($(main).find('.checkbox-slider-danger input[type="checkbox"]').is(":checked")) {
            var firstit = $(main).first('.hpost');
            var link = firstit.find('.post-title a').attr('href');
            window.location.href = link;
            return true;
        }
        return false;
    }
    function loadinformusic( main,mainref)
    {
        var url = main.data('url');
        var id = main.data('id');
        $.ajax({
            url: url,
            type: "POST",
            data: { id: id },
            success: function (data) {
                var status = data.status;
                if (status) {
                    $(mainref).html(data.html);
                }
                else {
                }
            },
            error: function () {
                alert("Thất bại");
            }
        });
    }
    //
    function init() {
        $("audio#ctl-audio").get(0).addEventListener("timeupdate", function () {
            updateTrackTime(this)
        });

        //Call event when audio is ended
        $('#ctl-audio').on('ended', function (e) {
            Run(this, ".control-music", 'next')
        });
        // nếu list có 1 phần tử xóa next and prev
        sethiddennextprev($(".control-music"));
        // CLick item list music
        $(document).on('click', '.list-play .play', function () {
            // nếu đang chạy thì dừng lại
            if ($(this).hasClass('fa-pause'))
            {
                var main = $('.control-music .control-play');
                $($('.control-music').data("audio")).trigger("pause");
                $(this).removeClass("fa-pause");
                $(this).addClass("fa-play");
                setplayplause(false, main);
                return;
            }
            var itmusic = $(this).parents(".itmusic");
            var stt = itmusic.data("stt");
            setplayplause(true, main);
            playmusic(stt, $('.control-music'))
        })
        // proress bar
        var player = document.getElementById('ctl-audio');
        player.addEventListener("timeupdate", function () {
            var currentTime = player.currentTime;
            var duration = player.duration;
            $("#music-process").slider("option", "value", ((currentTime + .25) / duration) * 10000);
            $('.hp_range').stop(true, true).animate({ 'width': (currentTime + .25) / duration * 10000 + '%' }, 250, 'linear');
        });

        //volumn
        $("#music-volum").slider({
            orientation: "horizontal",
            range: "min",
            max: 100,
            value: 100,
            slide: refreshVolumn,
            change: refreshVolumn,
        });
        $("#music-process").slider({
            orientation: "horizontal",
            range: "min",
            max: 10000,
            value: 0,
            slide: refreshProcess,
            change: refreshProcess,
        });
        $("#music-process").on("slidestop", function (event, ui) {
            var value = ui.value;
            var audio = $("#music-process").data("audio");
            setProcessBar(value, audio);
        });
        // icon volumn
        $(document).on("click", "#icon-volum", function () {
            var idvolumn = $(this).data("volumn");
            var cvolumn = $(idvolumn).data("cvalue"); 
            var audio = $(idvolumn).data("audio"); 
            var volumn = $(audio).get(0).volume * 100;
            if (volumn < 1) {
                setValueVolumn(cvolumn, audio, this);
                $(idvolumn).slider("option", "value", cvolumn);
            }
            else {
                setValueVolumn(0, audio, this);
                $(idvolumn).data("cvalue", volumn);
                $(idvolumn).slider("option", "value", 0);
            }
        });
    }
    
});
