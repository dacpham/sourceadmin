﻿
jQuery(document).ready(function() {
    $('.login-form input[type="text"], .login-form input[type="password"], .login-form textarea').on('focus', function() {
    	$(this).removeClass('input-error');
    });
    
    $('.login-form').on('submit', function(e) {
    	
    	$(this).find('input[type="text"], input[type="password"], textarea').each(function(){
    		if( $(this).val() == "" ) {
    			e.preventDefault();
    			$(this).addClass('input-error');
    		}
    		else {
    			$(this).removeClass('input-error');
    		}
    	});
    	
    });
    $('.registration-form input[type="text"], .registration-form textarea').on('focus', function() {
    	$(this).removeClass('input-error');
    });
    
    $('.registration-form').on('submit', function(e) {
    	
    	$(this).find('input[type="text"], textarea').each(function(){
    		if( $(this).val() == "" ) {
    			e.preventDefault();
    			$(this).addClass('input-error');
    		}
    		else {
    			$(this).removeClass('input-error');
    		}
    	});
    	
    });
    
    //Login by mạng xã hội
    $(document).on("click", ".btn-social", function () {
        var type = $(this).data("type");
        var cpage = window.location.href;
        var link = $(this).data("link");
        var modal = $(this).data("modal");
        $(modal).modal('hide');
        var new_window = window.open(link, "searchMemberID", "toolbar=no, status=no, menubar=no, scrollbars=yes, width=800, height=550, top=50, left=100");
        var timer = setInterval(function () {
            if(new_window.closed)
            {
                clearInterval(timer);
                window.location.reload();
            }
        }, 100);
    })
    //login system
    $(document).on("submit",".frm-reslogin",function () {
        var link = $(this).data("link");
        var idmes = $(this).data("idmess");
        var message = $(this).data("mess");
        var mesemail = $(this).data("mesemail");
        var guest = {};
        var main = this;
        guest.UserName = $(main).find("#Username").val();
        guest.PassWord = $(main).find("#Password").val();
        if ($(this).find("#RePassword").length >0)
        {
            var repassword = $(main).find("#RePassword").val();
            guest.Email = $(main).find("#Email").val();
            guest.Name = $(main).find("#Name").val();
            if (repassword != $(main).find("#Password").val())
            {
                showmesserro(main, idmes, message);
                return false;
            }
            if (!Utils.isEmpty(guest.Email) && !isEmail(guest.Email)) {
                showmesserro(main, idmes, mesemail);
                return false;
            }
        }


        $.ajax({
            url: link,
            type: "POST",
            data: Utils.getSerialize(jQuery(main)),
            success: function (data)
            {
                if (data.isSuccess)
                {
                    showmesssus(main, idmes, data.msSuccess);
                }
                else {
                    showmesserro(main, idmes, data.msError);
                }
            },
            error: function () {
                alert("Thất bại");
            }
        });
        return false;
    });



    // function  check email
    function isEmail(email) {
        var regex = /^([a-zA-Z0-9_.+-])+\@(([a-zA-Z0-9-])+\.)+([a-zA-Z0-9]{2,4})+$/;
        return regex.test(email);
    }
    function showmesserro(main,idmes, mess)
    {
        $(main).find(idmes).html(mess);
        $(main).find(idmes).removeClass();
        $(main).find(idmes).addClass("alert alert-danger");
        setTimeout(function () {
            $(main).find(idmes).addClass("hidden");
        }, 4000);
    }
    function showmesssus(main,idmes, mess) {
        $(main).find(idmes).html(mess);
        $(main).find(idmes).removeClass();
        $(main).find(idmes).addClass("alert alert-success");
        setTimeout(function () {
            window.location.reload();
        }, 2000);
    }
});
