﻿
using ShopRetailModel.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ShopRetail.Models
{
    public class HomeModel
    {
        public ContentCategroryModel TheHinhNam { get; set; }
        public ContentCategroryModel TheHinhNu { get; set; }
        public ContentCategroryModel GridHeader { get; set; }
        public ContentCategroryModel DinhDuongTheHinh { get; set; }
        public ContentCategroryModel LichTapGym { get; set; }
        public ContentCategroryModel KienThucTheHinh { get; set; }
        public ContentCategroryModel VideoHuongDan { get; set; }
        public ContentCategroryModel TapYoga { get; set; }
        public ContentCategroryModel ContentOfCategories { get; set; }
    }
}