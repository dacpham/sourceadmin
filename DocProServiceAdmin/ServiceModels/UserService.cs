﻿using ShopRetailModel.Interfaces;
using ShopRetailModel.Models;
using ShopRetailServiceAdmin.Context;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ShopRetailCore.Interfaces;
using PetaPoco;
using Common.Dac;

namespace ShopRetailServiceAdmin.ServiceModels
{
    public class UserService : Repository<User>, IUser
    {
        public UserService(IDbContextAdmin dbContext, IUnitOfWork unitOfWork) : base(dbContext, unitOfWork)
        {
            
        }

        public bool ValidateLogin(string username, string password, out User user)
        {
            Sql sql = Sql.Builder.From(TableName).
                Where("( UserName = @0 Or Email = @1 and PassWord and IsActive = 1 )", username, username, DACS.Encrypt(password, true));
            user = GetOne(sql);
            return !DACS.IsEmpty(user);
        }
    }
}
