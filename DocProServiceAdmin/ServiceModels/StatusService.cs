﻿using ShopRetailModel.Interfaces;
using ShopRetailModel.Models;
using ShopRetailServiceAdmin.Context;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ShopRetailCore.Interfaces;

namespace ShopRetailServiceAdmin.ServiceModels
{
    public class StatusService : Repository<Status>, IStatus
    {
        public StatusService(IDbContextAdmin dbContext, IUnitOfWork unitOfWork) : base(dbContext, unitOfWork)
        {

        }
    }
}
