﻿using ShopRetailModel.Interfaces;
using ShopRetailModel.Models;
using ShopRetailServiceAdmin.Context;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ShopRetailCore.Interfaces;
using Common.Dac;
using DocProUtil;
using PetaPoco;

namespace ShopRetailServiceAdmin.ServiceModels
{
    public class ProductService : Repository<Product>, IProduct
    {
        public ProductService(IDbContextAdmin dbContext, IUnitOfWork unitOfWork) : base(dbContext, unitOfWork)
        {

        }

        public List<Product> GetListByCategory(long idcategory,int take=10)
        {
            var _resCategory = _unitOfWork.GetDbSet<Category>();
            var oCategory = _resCategory.GetById(idcategory);
            var ids = new List<long>();
            if (DACS.IsEmpty(oCategory))
            {
                return new List<Product>();
            }
            ids.Add(oCategory.ID);
            var oCategoryAll = _resCategory.GetAll().Where(t => t.Parents.Split('|').ToList().Contains(oCategory.ID.ToString())).ToList();
            var oProducts = new List<Product>();
            if(!DACS.IsEmpty(oCategoryAll))
            {
                ids.AddRange(oCategoryAll.Select(t => t.ID).ToList());
            }
            return GetListByFields("IDCategory", ids).Take(take).ToList();
        }
        public List<Product> GetListByCategory(long idcategory ,Pagination paging)
        {
            var _resCategory = _unitOfWork.GetDbSet<Category>();
            var oCategory = _resCategory.GetById(idcategory);
            var ids = new List<long>();
            if (DACS.IsEmpty(oCategory))
            {
                return new List<Product>();
            }
            ids.Add(oCategory.ID);
            var oCategoryAll = _resCategory.GetAll().Where(t => t.Parents.Split('|').ToList().Contains(oCategory.ID.ToString())).ToList();
            var oProducts = new List<Product>();
            if (!DACS.IsEmpty(oCategoryAll))
            {
                ids.AddRange(oCategoryAll.Select(t => t.ID).ToList());
            }
            Sql sql = Sql.Builder.From(TableName).Where("IDCategory IN (@0)", ids);
            var result = Page(sql, paging.PageIndex, paging.PageSize);
            paging.RowCount = (int)result.TotalItems;
            return result.Items;
        }
    }
}
