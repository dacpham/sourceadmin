﻿using ShopRetailCore.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using PetaPoco;

namespace ShopRetailServiceAdmin.Context
{
    public class Repository : IRepository
    {
        protected readonly IUnitOfWork _unitOfWork;
        protected readonly IDbContext _dbContext;
        public Repository(IDbContext dbContext, IUnitOfWork unitOfWork)
        {
            _unitOfWork = unitOfWork;
            _unitOfWork.SetDataContext(dbContext);
            _dbContext = dbContext;
        }
        public int GetCount(Sql sqlQuery)
        {
            return _dbContext.DbInstance.ExecuteScalar<int>(sqlQuery);
        }
    }
}
