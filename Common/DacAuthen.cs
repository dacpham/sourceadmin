﻿using Newtonsoft.Json;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Security.Claims;
using System.Security.Cryptography;
using System.Text;
using System.Web;
using System.Web.Mvc;
using System.Web.Security;
using Microsoft.AspNet.Identity;
using Microsoft.Owin;
using Microsoft.Owin.Security;
using ShopRetailModel.Models;
using ShopRetailModel.ModelRepository;

namespace Common.Dac
{
    public static class DacAuthen
    {
        public static Guest GetCurrentGuest
        {
            get
            {
                try
                {
                    if (!object.Equals(HttpContext.Current.Session["Guest"], null))
                    {
                        return HttpContext.Current.Session["Guest"] as Guest;
                    }
                    var c = HttpContext.Current.User.Identity as ClaimsIdentity;
                    if (!c.IsAuthenticated)
                    {
                        return new Guest();
                    }
                    var autheticationManeger = c as ClaimsIdentity;
                    var entity = ServiceStatic.GetGuestById(int.Parse( autheticationManeger.FindFirst(ClaimTypes.PrimarySid).Value));
                    if (!object.Equals(entity, null))
                    {
                        HttpContext.Current.Session["Guest"] = entity;
                        return entity;
                    }
                }
                catch (Exception)
                {
                    return new Guest();
                }
                return new Guest();
            }
        }
        public static User GetCurrentUser
        {

            get
            {
                try
                {
                    if (!object.Equals(HttpContext.Current.Session["User"], null))
                    {
                        return HttpContext.Current.Session["User"] as User;
                    }
                    var c = HttpContext.Current.User.Identity as ClaimsIdentity;
                    if (!c.IsAuthenticated)
                    {
                        return new User();
                    }
                    var autheticationManeger = c as ClaimsIdentity;
                    var entity = ServiceStatic.GetUserById(int.Parse(autheticationManeger.FindFirst(ClaimTypes.PrimarySid).Value));
                    if (!object.Equals(entity, null))
                    {
                        HttpContext.Current.Session["Guest"] = entity;
                        return entity;
                    }
                }
                catch (Exception)
                {
                    return new User();
                }
                return new User();
            }
        }

        /// <summary>
        /// set login cho tài khoản khách đăng nhập hệ thống
        /// </summary>
        /// <param name="entitty"> thông tin  khách</param>
        /// <param name="isRemember">Nhờ cho lần đăng nhập sau hay không</param>
        public static void SetLoginGuest(Guest entitty, bool isRemember, HttpRequestBase Requests)
        {
            List<Claim> claims = new List<Claim>();
            claims.Add(new Claim(ClaimTypes.PrimarySid, entitty.ID.ToString()));
            var claimsIdentity = new ClaimsIdentity(claims, DefaultAuthenticationTypes.ApplicationCookie);
            var ctx = Requests.GetOwinContext();
            var authenticationManager = ctx.Authentication;
            authenticationManager.SignIn(new AuthenticationProperties
            {
                IsPersistent = isRemember,
            }, claimsIdentity);
        }
        /// <summary>
        /// Set login cho tài khoản người dùng hệ thống
        /// </summary>
        /// <param name="entitty">Thông tin người dùng  hệ thống</param>
        /// <param name="isRemember">Nhờ cho lần đăng nhập sau hay không</param>
        public static void SetLoginUser(User entitty, bool isRemember, HttpRequestBase Requests)
        {
            List<Claim> claims = new List<Claim>();
            claims.Add(new Claim(ClaimTypes.Name, entitty.UserName));
            var claimsIdentity = new ClaimsIdentity(claims, DefaultAuthenticationTypes.ApplicationCookie);
            var ctx = Requests.GetOwinContext();
            var authenticationManager = ctx.Authentication;
            authenticationManager.SignIn(new AuthenticationProperties
            {
                IsPersistent = isRemember,
            }, claimsIdentity);
        }
        /// <summary>
        /// Đăng xuất với người dùng khách
        /// </summary>
        public static void SetLogOutGuest(HttpRequestBase Requests)
        {
            var ctx = Requests.GetOwinContext();
            var authenticationManager = ctx.Authentication;
            authenticationManager.SignOut();
        }
        /// <summary>
        /// Đăng xuất với người dùng hệ thống
        /// </summary>
        public static void SetLogOutUser(HttpRequestBase Requests)
        {
            var ctx = Requests.GetOwinContext();
            var authenticationManager = ctx.Authentication;
            authenticationManager.SignOut();
        }


    }

}
