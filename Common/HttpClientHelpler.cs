﻿
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;

namespace Common.Dac
{
    public class HttpClientHelpler<T>
    {
        public async Task<T> Excute(string url, Dictionary<string, string> parameters)
        {
            T model = Activator.CreateInstance<T>();
            var content = new FormUrlEncodedContent(parameters);
            using (var client = new HttpClient())
            {
                try
                {
                    client.Timeout = TimeSpan.FromSeconds(30);
                    var response = await client.PostAsync(url, content).Result.Content.ReadAsStringAsync();
                    var result = JsonConvert.DeserializeObject<T>(response);
                    return result;
                }
                catch (Exception ex)
                {
                    DacLoger.Log(ex.ToString(), "httpclient");
                    throw;
                }
            }
        }
    }
}
