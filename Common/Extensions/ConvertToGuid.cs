﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.ModelBinding;

namespace Common.Extensions
{
    public static class ConvertToGuid
    {
        public static Guid[] ConvertToArrGuid(this string[] arrs)
        {
            var guids = new List<Guid>();
            if (arrs != null && arrs.Length > 0)
            {
                foreach (var item in arrs)
                {
                    var guid = new Guid();
                    if (Guid.TryParse(item, out guid))
                        guids.Add(guid);
                }
            }
            return guids.ToArray();
        }
    }
}
