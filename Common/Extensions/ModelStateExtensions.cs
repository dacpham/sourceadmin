﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.ModelBinding;

namespace Common.Extensions
{
    public static class ModelStateExtensions
    {
        
        //public static List<string> GetMessage(this ModelState valid)
        //{
        //    var Errors = new List<string>();
        //    foreach (ModelError error in valid.Errors)
        //    {
        //        Errors.Add(error.ErrorMessage);
        //    }
        //    return Errors;
        //}
        public static string GetA(this string thisObject)
        {
            return null;
        }
        public static List<string> GetMessage(this ModelStateDictionary valid)
        {
            var Errors = new List<string>();
            foreach (ModelState state in valid.Values)
            {
                foreach (ModelError error in state.Errors)
                {
                    Errors.Add(error.ErrorMessage);
                }
            }
            return Errors;
        }
    }
}
