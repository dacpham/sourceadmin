﻿
using Common.Dac;
using StgManeger.Attribute;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace StgManeger.Controllers
{
    public class UpLoadController : BaseController
    {
        public JsonResult Index()
        {
            string dic = string.Empty, ContentLength= string.Empty , ContentType=string.Empty,Name=string.Empty;
            string dicstg = "Stg/" + DateTime.Now.Year + "/" + DateTime.Now.Month + "/" + DateTime.Now.Day + "/";
            HttpPostedFileBase file;
            for (int i = 0; i < Request.Files.Count; i++)
            {
                file = Request.Files[i]; //Uploaded file
                byte[] buffer = new byte[256];
                // byte[] data = new byte[srcFile.Length];
                //srcFile.Read(data, 0, (Int32)srcFile.Length);
                var srcFile = file.InputStream;
                if (srcFile.Length >= 256)
                    srcFile.Read(buffer, 0, 256);
                else
                    srcFile.Read(buffer, 0, (int)srcFile.Length);


                ContentLength = file.ContentLength.ToString();
                string fileName = DateTime.Now.Ticks +"_"+ file.FileName;
                Name = file.FileName;
                ContentType = file.ContentType;
                dic = Path.Combine(Server.MapPath("~/"), dicstg);
                dicstg = dicstg + fileName;
                DACS.CreateDir(dic);
                file.SaveAs(Path.Combine(dic, fileName));
            }
            //new {FileName=file.FileName,FilePath=dicstg, ContentLength = file.ContentLength, ContentType= file.ContentType }
            return Json(new { FileName = Name, FilePath = dicstg, ContentLength = ContentLength, ContentType = ContentType },JsonRequestBehavior.AllowGet);
          //  return Json(dic);
            }

        }
    }