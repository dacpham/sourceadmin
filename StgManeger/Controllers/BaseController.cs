﻿using Common.Dac;
using DocProUtil;
using ShopRetailModel.Interfaces;
using ShopRetailModel.Models;
using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Web.Mvc;

namespace StgManeger.Controllers
{
    public class BaseController : Controller
    {
      
        /// <summary>
        ///     Title page
        /// </summary>
        private string _title;
        private readonly List<string> _warns = new List<string>();
        private readonly List<string> _errors = new List<string>();
        private readonly List<string> _success = new List<string>();
        private readonly List<string> _notifies = new List<string>();

        #region Form Data
        private Hashtable _data;
        protected Hashtable DATA
        {
            get
            {
                if (Equals(_data, null))
                    _data = DACS.GetDataPost();

                return _data;
            }
        }

        #endregion

        #region ResResult

        private bool _isLogout;
        private bool _isMsg;

        private bool _isDL;
        private object _wDL;
        private string _htDL;

        private bool _isCust;
        private string _htCust;

        private bool _resOnlyData;
        private dynamic _dataRes;

        public void SetIsLogout()
        {
            _isLogout = true;
        }
        public void SetDataResponse(dynamic data)
        {
            _dataRes = data;
        }
        public void SetOnlyDataResponse(dynamic data)
        {
            _resOnlyData = true;
            _dataRes = data;
        }
        /// <summary>
        /// Set html of dialog
        /// </summary>
        /// <param name="html"></param>
        public void SetHtmlDialog(string html, object width)
        {
            _isDL = true;
            _wDL = width;
            _htDL = html;
        }

        /// <summary>
        /// Set html of custom placed
        /// </summary>
        /// <param name="html"></param>
        public void SetHtmlResponse(string html)
        {
            _isCust = true;
            _htCust = html;
        }

        protected JsonResult GetResult()
        {
            if (_resOnlyData)
                return Json(new Hashtable {
                    {"data", _dataRes ?? string.Empty}
                });

            var res = new Hashtable();
            res.Add("data", _dataRes ?? string.Empty);
            res.Add("title", ViewBag.Title ?? string.Empty);

            if (_errors.Any())
                res.Add("isErr", 1);
            if (_isLogout)
                res.Add("isLogout", 1);

            if (_isMsg)
            {
                var messages = new List<string>();
                messages.AddRange(_errors);
                messages.AddRange(_warns);
                messages.AddRange(_notifies);
                messages.AddRange(_success);

                res.Add("isMsg", 1);
                res.Add("isError", HasError);
                res.Add("isWarn", HasWarn);
                res.Add("isNotify", HasNotify);
                res.Add("isSuccess", HasSuccess);

                res.Add("msError", string.Join("\n", _errors));
                res.Add("msWarn", string.Join("\n", _warns));
                res.Add("msNotify", string.Join("\n", _notifies));
                res.Add("msSuccess", string.Join("\n", _success));
                res.Add("htMsg", GetMessages());
            }
            if (_isDL)
            {
                res.Add("isDL", 1);
                res.Add("wDL", _wDL);
                res.Add("htDL", _htDL ?? string.Empty);
            }
            if (_isCust)
            {
                res.Add("isCust", 1);
                res.Add("htCust", _htCust ?? string.Empty);
            }
            return Json(res);
            //json.MaxJsonLength = int.MaxValue;
        }

        #endregion

        #region RenderOptions
        //protected static string RenderOptions(dynamic data, bool hasUndefined = true)
        //{
        //    return HtmlOption.RenderOption(Utils.GetOptions(data, hasUndefined));
        //}
        //protected static string RenderOptions(dynamic data, int selected, bool hasUndefined = true)
        //{
        //    return HtmlOption.RenderOption(Utils.GetOptions(data, selected, hasUndefined));
        //}
        //protected static string RenderOptions(dynamic data, List<int> selecteds, bool hasUndefined = true)
        //{
        //    return HtmlOption.RenderOption(Utils.GetOptions(data, selecteds, hasUndefined));
        //}
        #endregion

        /// <summary>
        ///     Session ID
        /// </summary>
        protected string SessionID
        {
            get
            {
                try
                {
                    return Session.SessionID;
                }
                catch
                {
                    return string.Empty;
                }
            }
        }

        /// <summary>
        ///     Current user
        /// </summary>
        protected User CUser;

        /// <summary>
        ///     Pagination
        /// </summary>
        protected Pagination Paging;

        /// <summary>
        ///     Stylesheets
        /// </summary>
        protected List<string> Css;

        /// <summary>
        ///     Javascripts
        /// </summary>
        protected List<string> Scripts;
        public bool HasError
        {
            get { return _errors.Any(); }
        }
        public bool HasWarn
        {
            get { return _warns.Any(); }
        }
        public bool HasNotify
        {
            get { return _notifies.Any(); }
        }
        public bool HasSuccess
        {
            get { return _success.Any(); }
        }

        /// <summary>
        ///     Set title page
        /// </summary>
        /// <param name="title"></param>
        protected void SetTitle(string title)
        {
            _title = title;
        }

        /// <summary>
        ///     Set success
        /// </summary>
        /// <param name="success"></param>
        public void SetSuccess(string success)
        {
            _isMsg = true;
            _success.Add(success);
            Session["Success"] = _success;
        }
        public void SetSuccess(List<string> success)
        {
            if (!Equals(success, null) && success.Any())
            {
                _isMsg = true;
                _success.AddRange(success);
                Session["Success"] = _success;
            }
        }
        public void SetNotify(string notify)
        {
            _isMsg = true;
            _notifies.Add(notify);
            Session["Notifies"] = _notifies;
        }
        public void SetNotify(List<string> notifies)
        {
            if (!Equals(notifies, null) && notifies.Any())
            {
                _isMsg = true;
                _notifies.AddRange(notifies);
                Session["Notifies"] = _notifies;
            }
        }

        /// <summary>
        ///     Set errors
        /// </summary>
        /// <param name="error"></param>
        public void SetError(string error)
        {
            _isMsg = true;
            _errors.Add(error);
            Session["Errors"] = _errors;
        }
        public void SetErrors(List<string> errors)
        {
            if (!Equals(errors, null) && errors.Any())
            {
                _isMsg = true;
                _errors.AddRange(errors);
                Session["Errors"] = _errors;
            }
        }

        /// <summary>
        ///     Set warnings
        /// </summary>
        /// <param name="warn"></param>
        public void SetWarn(string warn)
        {
            _isMsg = true;
            _warns.Add(warn);
            Session["Warns"] = _warns;
        }
        public void SetWarns(List<string> warns)
        {
            if (!Equals(warns, null) && warns.Any())
            {
                _isMsg = true;
                _warns.AddRange(warns);
                Session["Warns"] = _warns;
            }
        }

        /// <summary>
        ///     Include stylesheet
        /// </summary>
        /// <param name="item"></param>
        protected void IncludeCss(string item)
        {
            if (Equals(Css, null))
                Css = new List<string>();

            Css.Add(item);
        }

        /// <summary>
        ///     Include javascript
        /// </summary>
        /// <param name="item"></param>
        protected void IncludeScript(string item)
        {
            if (Equals(Scripts, null))
                Scripts = new List<string>();

            Scripts.Add(item);
        }

        /// <summary>
        ///     Called before the action method is invoked.
        /// </summary>
        /// <param name="filterContext">
        ///     Information about the current request and action.
        /// </param>
        protected override void OnActionExecuting(ActionExecutingContext filterContext)
        {
            base.OnActionExecuting(filterContext);
            Paging = new Pagination(HttpContext.Request);
            ViewBag.Title = ("LShoper");

            GetCUser();
        }
        protected void GetCUser()
        {
            CUser = new User();
             ViewBag.CUser = CUser;
        }
        /// <summary>
        ///     Called after the action method is invoked.
        /// </summary>
        /// <param name="filterContext">
        ///     Information about the current request and action.
        /// </param>
        protected override void OnActionExecuted(ActionExecutedContext filterContext)
        {
            base.OnActionExecuted(filterContext);
            if (Request.IsAjaxRequest() == false)
            {
                //if (CUser.ID > 0)
                //{
                //    //TODO DacPV
                //}

                #region Css, JS, CDATA

                //Is mobile
                if (Request.Browser.IsMobileDevice)
                {
                    IncludeCss("~/Assets/jquery/css/jquery.mobile.css");
                    IncludeScript("~/Assets/jquery/js/jquery.mobile.js");
                    IncludeScript("~/Assets/jquery/js/jquery.plugins.js");
                    IncludeScript("~/Assets/jquery/js/jquery.functions.js");
                }

                //Set title
                if (!Equals(_title, null))
                    ViewBag.Title = _title;

                //Set stylesheets
                if (!Equals(Css, null))
                    ViewBag.Css = Css.ToArray();

                //Set javascripts
                if (!Equals(Scripts, null))
                    ViewBag.Scripts = Scripts.ToArray();

                //Set pagination
                ViewBag.Pagination = Paging;

                //set dashboard

                //Set CDATA
                #endregion
            }
        }
        protected override void OnResultExecuted(ResultExecutedContext filterContext)
        {
            base.OnResultExecuted(filterContext);

            if (!Equals(Session["user"], null))
                Session.Remove("user");
        }
        protected string GetFormSearch(string name)
        {
            return string.IsNullOrEmpty(name)
                ? string.Empty
                : GetView(string.Format(
                    "~/Views/Shared/Searchs/{0}.cshtml",
                    name
                ));
        }
        protected string GetView(string viewName, object data = null)
        {
            try
            {
                //ViewBag.Pagination = Paging;
                if (!Equals(_title, null))
                    ViewBag.Title = _title;

                using (var sw = new StringWriter())
                {
                    ControllerContext.Controller.ViewData.Model = data;
                    var viewResult = ViewEngines.Engines.FindPartialView(ControllerContext, viewName);
                    var viewContext = new ViewContext(ControllerContext, viewResult.View, ViewData, TempData, sw);
                    viewResult.View.Render(viewContext, sw);
                    return sw.ToString();
                }
            }
            catch (Exception ex)
            {
                //Loger.Log(ex);
                return string.Empty;
            }
        }
        protected string GetMessages()
        {
            return GetView("~/Views/Shared/Message.cshtml");
        }
        protected string GetReferrerOrDefault(string defaultPath)
        {
            if (Equals(Request.UrlReferrer, null))
                return defaultPath;

            return Request.UrlReferrer.ToString();
        }
        protected string GetRedirectOrDefault(string defaultPath)
        {
            var redirectPath = DACS.GetString(DATA, "RedirectPath");
            return string.IsNullOrEmpty(redirectPath)
                ? defaultPath
                : redirectPath;
        }
        protected ActionResult GetResultOrRedirectDefault(string defaultPath)
        {
            if (Request.IsAjaxRequest())
                return GetResult();
            return RedirectToPath(GetRedirectOrDefault(defaultPath));
        }
        protected ActionResult GetResultOrReferrerDefault(string defaultPath)
        {
            if (Request.IsAjaxRequest())
                return GetResult();
            return RedirectToPath(GetReferrerOrDefault(defaultPath));
        }
        protected ActionResult GetResultOrView(string viewName, object data = null)
        {
            if (Request.IsAjaxRequest())
            {
                return GetResult();
            }
            return GetViewResult(viewName, data);
        }
        protected ActionResult GetViewResult(string viewName, object data = null)
        {
            return View(viewName, data);

        }
        protected RedirectResult RedirectToPath(string path)
        {
            path = (path ?? "/");
            return Redirect(path);
        }
        protected RedirectResult RedirectToPath(string path, params object[] param)
        {
            return Redirect(string.Format(path, param));
        }

    }
}