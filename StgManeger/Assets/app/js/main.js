var message = {
    ShowAllMessage: function () {
        var msgsuccess = $("#st-msg-success").val();
        var msgwarning = $("#st-msg-warnings").val()
        var msgerror = $("#st-msg-errors").val()
        var msgnotify = $("#st-msg-notifys").val()
        if (!Utils.isEmpty(msgsuccess)) {
            Utils.setToastSuccess(msgsuccess);
        }
        else {
            if (!Utils.isEmpty(msgerror)) {
                Utils.setToastError(msgerror);
            }
            if (!Utils.isEmpty(msgwarning)) {
                Utils.setToastWarning(msgwarning);
            }
            if (!Utils.isEmpty(msgnotify)) {
                Utils.setToastInfor(msgnotify);
            }
        }
    }
}

$(document).ready(function(){
	

    message.ShowAllMessage();
});
