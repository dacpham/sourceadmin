﻿using PetaPoco;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ShopRetailCore.Interfaces
{
    public interface IRepository
    {
        /// <summary>
        /// Lấy về số lượng bản ghi theo query truyền vào
        /// </summary>
        /// <param name="sqlQuery"></param>
        /// <returns></returns>
        int GetCount(Sql sqlQuery);
    }
}
