﻿using PetaPoco;
using ShopRetailServiceAdmin.Interfaces;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ShopRetailCore.Interfaces
{
    public interface IUnitOfWork
    {
        IDbContext DataContext { get; }
        ITransaction GetTransaction();
        void SetDataContext(IDbContext dbContext);
        IRepository<T> GetDbSet<T>() where T : class, new();
        void BeginTransaction();
        void BeginTransaction(IsolationLevel isolationLevel = IsolationLevel.Unspecified);
        void CommitTransaction();
        void AbortTransaction();
    }

}
