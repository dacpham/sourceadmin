﻿using System.Web.Optimization;

namespace ShopAdmin
{
    public class BundleConfig
    {
        // For more information on Bundling, visit http://go.microsoft.com/fwlink/?LinkId=254725
        public static void RegisterBundles(BundleCollection bundles)
        {
            //App - theme 1
            bundles.Add(new StyleBundle("~/assets/app/Admin.css").Include(
                "~/assets/css/bootstrap.min.css",
                     "~/assets/css/font-awesome.min.css",
                          "~/assets/css/weather-icons.min.css",
                               "~/assets/css/beyond.min.css",
                                    "~/assets/css/demo.min.css",
                                         "~/assets/css/typicons.min.css",
                                              "~/assets/css/animate.min.css",
                                                 "~/assets/select2/css/select2.min.css",
                                                   "~/assets/css/maincss.css"
             ));

            bundles.Add(new ScriptBundle("~/assets/app/Admin.js").Include(
            "~/assets/js/jquery.min.js",
            "~/assets/js/bootstrap.min.js",
            "~/assets/js/slimscroll/jquery.slimscroll.min.js",
            "~/assets/js/beyond.js",
            "~/assets/js/charts/sparkline/jquery.sparkline.js",
            "~/assets/js/charts/sparkline/sparkline-init.js",
            "~/assets/js/charts/easypiechart/jquery.easypiechart.js",
            "~/assets/js/charts/easypiechart/easypiechart-init.js",
            "~/assets/js/charts/flot/jquery.flot.js",
            "~/assets/js/charts/flot/jquery.flot.resize.js",
            "~/assets/js/charts/flot/jquery.flot.pie.js",
            "~/assets/js/charts/flot/jquery.flot.tooltip.js",
            "~/assets/js/charts/flot/jquery.flot.orderBars.js",
                 "~/assets/js/toastr/toastr.js",
               "~/assets/js/utils.js",
              "~/assets/select2/js/select2.min.js",
              "~/assets/js/editors/summernote/summernote.js",
            "~/assets/js/mainjs.js" ,
             "~/assets/js/uploader.js"
            

            ));

            //compressor
            //    BundleTable.EnableOptimizations = GlobalConfig.WebOptimization;
        }
    }
}