﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ShopDetail.Customs.Params
{
    public class SearchParam
    {
        public int ID { get; set; }
        public int Type { get; set; }
        public int Position { get; set; }
        public string Term { get; set; }
        public string Content { get; set; }
        public int ChuyenMuc { get; set; }
        public string TuKhoa { get; set; }
        public string TuNgay { get; set; }
        public string DenNgay { get; set; }
    }
}