﻿var urlupload = "http://phukiensieuxinh.com.vn/stg/"
var main = {
    Init: function () {
        // dacPV check Utils CheckAll
        $(document).on("change", ".checkboxitem", function () {
            var container = $(this).data("container"); // vùng chứa tất cả của checkbox item và check box all
            var itemshow = $(this).data("itemshow"); // item muốn hiển thị khi check box được tích , xóa tất cả, ....
            var ids = checktick($(container), ".checkboxitem")
            setItemcheck(ids, container, itemshow);
            if (Utils.isEmpty(ids)) {
                $(container).find(".checkboxall").prop("checked", false);
            }
        });
        $(document).on("change", ".checkboxall", function () {
            var container = $(this).data("container"); // vùng chứa tất cả của checkbox item và check box all
            var itemshow = $(this).data("itemshow"); // item muốn hiển thị khi check box được tích , xóa tất cả, ....
            if ($(this).prop("checked")) {
                checkalltick(container, ".checkboxitem", true);
                var ids = checktick($(container), ".checkboxitem")
                setItemcheck(ids, container, itemshow);
            }
            else {
                checkalltick(container, ".checkboxitem", false);
                setItemcheck("", container, itemshow);
            }
        });
        function checktick(container, checkboxitem) {
            var result = [];
            container.find(checkboxitem).each(function () {
                if ($(this).prop("checked")) {
                    var id = $(this).data("id");
                    result.push(id);
                }
            });
            return result.join(",");
        }
        function setItemcheck(ids, container, itemshow) {
            if (Utils.isEmpty(ids)) {
                $(container).find(itemshow).each(function () {
                    $(this).data("ids", "");
                    $(this).addClass("hidden");
                });
            }
            else {
                $(container).find(itemshow).each(function () {
                    $(this).data("ids", ids);
                    $(this).removeClass("hidden");
                });
            }
        }
        function checkalltick(container, checkboxitem, status) // bỏ check hoặc check tất cả
        {
            $(container).find(checkboxitem).each(function () {
                $(this).prop("checked", status)

            });
        }
        ////////--------------
    },
    InitAutoComplete: function (container) {
        if (Utils.isEmpty(container))
            container = $(document);
        container.find(".autoselect2").each(function () {
            $(this).select2();
        })

    },
    InitEditor:function()
    {
        $('.note-editor').summernote({ height: 300 });
    },
    InitEvent:function()
    {
        $(document).on("click", ".btndelete", function () {
            var link = $(this).data("link");
            $("#md-comfirmdelete").find("#Ids").val($(this).data("ids"));
            $("#md-comfirmdelete").find("form").prop("action", link)
            $("#md-comfirmdelete").modal("show");
        });
        $(document).on("click", ".btnxuatban", function () {
            var link = $(this).data("link");
            $("#md-comfirmxuatban").find("#Ids").val($(this).data("ids"));
            $("#md-comfirmxuatban").find("form").prop("action", link)
            $("#md-comfirmxuatban").modal("show");
        });
        $(document).on("click", ".btnhuyxuatban", function () {
            var link = $(this).data("link");
            $("#md-comfirmhuyxuatban").find("#Ids").val($(this).data("ids"));
            $("#md-comfirmhuyxuatban").find("form").prop("action", link)
            $("#md-comfirmhuyxuatban").modal("show");
        });
        $(document).on("submit", ".frm-submit", function () {
            var link =$(this).prop("action");
            main.ajaxCRUD(link,this);
            location.reload();
            return false;
        });
        $(document).on("click", ".btnclose_img", function () {
            $(this).parents(".img_item").remove();
            return false;
        });
    },
    ajaxCRUD :function(url, container) {
        jQuery.ajax({
            url: url,
            type: "POST",
            data: Utils.getSerialize(jQuery(container)),
            success: function (data) {
                if (data.isSuccess) {
                    Utils.setToastSuccess(data.msSuccess);
                }
                else {
                    Utils.setToastError(data.msError);
                }
            },
        });
    } ,
    IntUpload:function()
    {
        $(document).find(".searchdropbox").each(function () {
            var searchdropbox = $(this);
            var Inputfile = searchdropbox.find(".inputfile").first();
            var npath = searchdropbox.data("npath"); // id item can thay the tuy bien name
            var multi = searchdropbox.data("multi"); // 1 duy nhất . 2 nhiều
            searchdropbox.ajaxUploader({
                name: "FileDocument",
                postUrl: urlupload,
                onBegin: function (e, dragBox) {
                    return true;
                },
                onClientProgress: function (e, file) {
                },
                onSuccess: function (e, file, dragBox, data) {
                    console.log(data);
                    var lstimage = searchdropbox.children(".List_image").first();
                    var temp = $("#temp_img_item").html();
                    temp = temp.replace("#SrcPath", urlupload + data.FilePath);
                    temp = temp.replace("#Path", data.FilePath);
                    temp = temp.replace("#NamePath", npath);
                    if (multi == '1') {
                        lstimage.find(".img_item_add").remove();
                        lstimage.append(temp);
                    }
                    else {
                        lstimage.append(temp);
                    }
                    //jQuery('#PathSearchByFile').val(data.FilePath);
                    //jQuery('#PathSearchByFile').parents('form').submit();
                }
            });
            Inputfile.ajaxUploader({
                name: "FileDocument",
                postUrl: urlupload,
                onBegin: function (e, dragBox) {
                    return true;
                },
                onClientProgress: function (e, file) {
                },
                onSuccess: function (e, file, dragBox, data) {
                    console.log(data);
                    var lstimage = searchdropbox.find(".List_image").first();
                    var temp = $("#temp_img_item").html();
                    temp = temp.replace("#SrcPath", urlupload + data.FilePath);
                    temp = temp.replace("#Path", data.FilePath);
                    temp = temp.replace("#NamePath", npath);
                    if (multi == '1') {
                        lstimage.find(".img_item_add").remove();
                        lstimage.append(temp);
                    }
                    else {
                        lstimage.append(temp);
                    }
                    //jQuery('#PathSearchByFile').val(data.FilePath);
                    //jQuery('#PathSearchByFile').parents('form').submit();
                }
            });
        });
        
       
        $(document).find(".img_item_choose").click(function () {
            var input = $(this).siblings(".inputfile").first();
            input.click();
        });
    },
}
var message = {
    ShowAllMessage: function () {
        var msgsuccess = $("#st-msg-success").val();
        var msgwarning = $("#st-msg-warnings").val()
        var msgerror = $("#st-msg-errors").val()
        var msgnotify = $("#st-msg-notifys").val()
        if (!Utils.isEmpty(msgsuccess)) {
            Utils.setToastSuccess(msgsuccess);
        }
        else {
            if (!Utils.isEmpty(msgerror)) {
                Utils.setToastError(msgerror);
            }
            if (!Utils.isEmpty(msgwarning)) {
                Utils.setToastWarning(msgwarning);
            }
            if (!Utils.isEmpty(msgnotify)) {
                Utils.setToastInfor(msgnotify);
            }
        }
    }
}
var event = {
    init: function ()
    {
        
    }
};
$(document).ready(function () {
    main.Init();
    main.InitAutoComplete();
    main.InitEditor();
    main.InitEvent();
    main.IntUpload();
    message.ShowAllMessage();
})