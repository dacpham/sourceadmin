﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace DocProEMICO.Attribute
{
    public class HasCredentialAttribute : AuthorizeAttribute
    {
        #region---------Properti--------
        public string Role { get; set; }
        #endregion----------------------


        /// <summary>
        /// đây là hàm check quyền truy nhập
        /// </summary>
        /// <param name="httpContext"></param>
        /// <returns></returns>
        protected override bool AuthorizeCore(HttpContextBase httpContext)
        {
            //    if(httpContext.User.Identity.Name)
            //    {

            //    }
            return base.AuthorizeCore(httpContext);
        }
        /// <summary>
        /// Đây là hàm  để dẫn link khi mà không có quyền truy nhập
        /// </summary>
        /// <param name="filterContext"></param>
        protected override void HandleUnauthorizedRequest(AuthorizationContext filterContext)
        {
            base.HandleUnauthorizedRequest(filterContext);
        }
    }
}