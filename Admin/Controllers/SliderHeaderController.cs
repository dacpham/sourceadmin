﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Common.Dac;
using ShopRetailModel.Models;
using System.IO;
using ShopRetailModel.Params;
using ShopRetailModel.Interfaces;
using static ShopRetailReponsitoty.Enum.Enum;

namespace ShopAdmin.Controllers
{
    public class SliderHeaderController : BaseController
    {
        public SliderHeaderController(IUser resUser, ICategory resCategory, IExport resExport, IEvent resEvent, IExportDetail resExportDetail, IGuest resGuest, Import resImport, IOrder resOrder, IOrderDetail resOrderDetail, IProduct resProduct, IProductViewed resProductViewed, ISliderInfor resSliderInfor, IStatus resStatus, IStgFiles resStgFiles) : base(resUser, resCategory, resExport, resEvent, resExportDetail, resGuest, resImport, resOrder, resOrderDetail, resProduct, resProductViewed, resSliderInfor, resStatus, resStgFiles)
        {
        }

        public ActionResult Index(int id = 0)
        {
            var sliderinforpr = DACS.Bind<SliderInforParam>(DATA);
            var sliderinfors = _resSliderInfor.Search(sliderinforpr,Paging);
            ViewBag.SliderInforParam = sliderinforpr;
            return GetCustResultOrView("Index", sliderinfors);
        }
        public ActionResult Sua(long id = 0)
        {
            var sliderinfor = _resSliderInfor.GetById(id) ?? new SliderInfor();
            if(sliderinfor.ID ==0)
            {
                return RedirectToPath("/admin/slider-header.html");
            }
            return GetCustResultOrView("ChiTiet", sliderinfor);
        }
        public ActionResult ThemMoi(int id = 0)
        {
            var sliderinfor = new SliderInfor();
            return GetCustResultOrView("ChiTiet",sliderinfor);
        }
        /// <summary>
        /// Them cac file anh cua san pham
        /// </summary>
        /// <param name="IDDoc"></param>
        private void BindFileRefer(long IDDoc)
        {
            var filepaths = DACS.GetStrings(DATA, "FilePaths");
            var stgfiles = new List<StgFiles>();
            if(filepaths != null && filepaths.Count() >0)
            {
                foreach (var item in filepaths)
                {
                    var stgfile = new StgFiles();
                    stgfile.Created = DateTime.Now;
                    stgfile.CreatedBy = CUser.ID;
                    stgfile.ThumbPath = item;
                    stgfile.ThumbName = GetNameFromPath(item);
                    stgfile.Type = (int)FileType.Hinhanh;
                    stgfile.IDDoc = IDDoc;
                    stgfiles.Add(stgfile);
                }
                _resStgFiles.Deletes("IDDoc",IDDoc);
                _resStgFiles.Inserts(stgfiles);
            }
        }
        public ActionResult CapNhat()
        {
            var sliderinfor = DACS.Bind<SliderInfor>(DATA);
            if (sliderinfor.ID == 0) // Insert
            {
                var entity = DACS.BindCreate<SliderInfor>(sliderinfor, CUser.ID);
                entity.ThumbName = Path.GetFileName(entity.ThumbPath);
                if (_resSliderInfor.Insert(entity))
                {
                    SetSuccess(DACS.T("Thêm thành công"));
                }
                else
                {
                    SetError(DACS.T("Thêm không thành công , đã xảy ra lỗi"));
                }
            }
            else
            {
                var entity = _resSliderInfor.GetById(sliderinfor.ID);
                //entity = DACS.Bind<Category>(entity, DACS.KeyValue<SliderInfor>(sliderinfor), 0, CUser.ID);
                entity = DACS.Bind<SliderInfor>(entity, DATA, 0, CUser.ID);
                entity.ThumbName = Path.GetFileName(entity.ThumbPath);
                if (_resSliderInfor.Update(entity))
                {
                    SetSuccess(DACS.T("Cập nhật thành công"));
                }
                else
                {
                    SetError(DACS.T("Cập nhật không thành công , đã xảy ra lỗi"));
                }
            }
            return RedirectToPath(DACS.ReUrl(DACS.admin +"/sliderheader.html",""));
        }
        public ActionResult Xoa()
        {
            var ids = DACS.GetBigInts(DATA, "ids");
            var entitys = _resSliderInfor.GetItems(ids);
            if (Equals(entitys, null))
                SetError(DACS.T("Phẩn tử cần xóa không tồn tại , vui lòng kiểm tra lại"));
            else
            {
                if (_resSliderInfor.Deletes(entitys.Select(t=>t.ID).ToArray())>=0)
                    SetSuccess(DACS.T("Xóa thành công"));
                else
                    SetError(DACS.T("Xóa không thành công ,vui lòng kiểm tra lại"));
            }
            return RedirectToPath( GetReferrerOrDefault("/home.html"));
        }
        //huy xuất bản
        public ActionResult XuatBan()
        {
            var ids = DACS.GetBigInts(DATA, "ids");
            var entitys = _resSliderInfor.GetItems(ids).ToList();
            if (Equals(entitys, null))
                SetError(DACS.T("Phẩn từ không tồn tại , vui lòng kiểm tra lại"));
            else
            {
                try
                {
                    entitys.ForEach(t =>
                    {
                        t.IsPublish = true;
                        t.PublishBy = CUser.ID;
                        t.Published = DateTime.Now;
                        t.BindUpdate<SliderInfor>(CUser.ID);
                    });
                }
                catch (Exception ex)
                {
                    Loger.Log(ex.ToString());
                    throw;
                }
              
                if (_resSliderInfor.Updates(entitys, "IsPublish", "PublishBy", "Published", "Updated","UpdatedBy"))
                    SetSuccess(DACS.T("Thao tác xuất bản thành công"));
                else
                {
                    SetError(DACS.T("Thao tác xuất bản không thành công"));
                }
            }
            return RedirectToPath(GetReferrerOrDefault("/home.html"));
        }
        public ActionResult HuyXuatBan()
        {
            var ids = DACS.GetBigInts(DATA, "ids");
            var entitys = _resSliderInfor.GetItems(ids);
            if (Equals(entitys, null))
                SetError(DACS.T("Phẩn từ không tồn tại , vui lòng kiểm tra lại"));
            else
            {
                entitys.ForEach(t =>
                {
                    t.IsPublish = false;
                    t.PublishBy = CUser.ID;
                    t.Published = DateTime.Now;
                    t.BindUpdate<SliderInfor>(CUser.ID);
                });
                if (_resSliderInfor.Updates(entitys, "IsPublish", "PublishBy", "Published", "Updated", "UpdatedBy"))
                    SetSuccess(DACS.T("Thao tác xuất bản thành công"));
                else
                {
                    SetError(DACS.T("Thao tác xuất bản không thành công"));
                }
            }
            return RedirectToPath(GetReferrerOrDefault("/home.html"));
        }
    }
}