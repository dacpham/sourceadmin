﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Common.Dac;
using ShopRetailServiceAdmin.ServiceModels;
using ShopRetailModel.Models;
using ShopRetailModel.Interfaces;
using ShopRetailModel.Params;
using PetaPoco;
using ShopAdmin.Controllers;

namespace ShopDetail.Controllers
{
    public class DanhMucController : BaseController
    {
        public DanhMucController(IUser resUser, ICategory resCategory,
            IExport resExport, IEvent resEvent, IExportDetail resExportDetail,
            IGuest resGuest, Import resImport, IOrder resOrder, IOrderDetail resOrderDetail,
            IProduct resProduct, IProductViewed resProductViewed, ISliderInfor resSliderInfor,
            IStatus resStatus, IStgFiles resStgFiles) : base(resUser, resCategory, resExport,
                resEvent, resExportDetail, resGuest, resImport, resOrder, resOrderDetail, resProduct,
                resProductViewed, resSliderInfor, resStatus, resStgFiles)
        {
            
        }

        public ActionResult Index(int id = 0)
        {
            var param = DACS.Bind<CategoryParam>(DATA);
            param.Parent = id;
            var categorys = _resCategory.Search(param,Paging);
            ViewBag.Parent = id;
            ViewBag.Term = param.Term ;
            return GetCustResultOrView("Index", categorys);
        }
        public ActionResult Sua(long id = 0)
        {
            var category = _resCategory.GetById(id) ?? new Category();
            ViewBag.Category = DACS.RenderOptions(_resCategory.GetAll(), category.Parent, true, "Chọn danh mục sản phẩm");
            return GetCustResultOrView("ChiTiet", category);
        }
        public ActionResult ThemMoi(int id = 0)
        {
            var category =  new Category();
            category.Parent = id;
            ViewBag.Category = DACS.RenderOptionGroups(_resCategory.GetAll(),0, true, "Chọn danh mục sản phẩm");
            return GetCustResultOrView("ChiTiet", category);
        }
        public ActionResult CapNhat()
        {
            var category = DACS.Bind<Category>(DATA);
            if (category.ID == 0) // Insert
            {
                var entity = DACS.BindCreate<Category>(category, CUser.ID);
                entity.Parents = GetCategoryParents(entity.Parent);
                entity.IsBody = DACS.GetBool(DATA, "IsBody");
                entity.IsHeader = DACS.GetBool(DATA, "IsHeader");
                if (_resCategory.Insert(entity))
                {
                    SetSuccess(DACS.T("Thêm thành công"));
                }
                else
                {
                    SetError(DACS.T("Thêm không thành công , đã xảy ra lỗi"));
                }
            }
            else
            {
                var entity = _resCategory.GetById(category.ID);
                entity = DACS.Bind<Category>(entity, DACS.KeyValue<Category>(category), 0, CUser.ID);
                entity = DACS.Bind<Category>(entity, DATA, 0, CUser.ID);
                entity.Parents = GetCategoryParents(entity.Parent);
                entity.IsBody = DACS.GetBool(DATA, "IsBody");
                entity.IsHeader = DACS.GetBool(DATA, "IsHeader");
                if (_resCategory.Update(entity,"Name", "Describe", "Body", "Parent", "Parents",
                    "Image", "Url","IsBody","IsHeader"))
                {
                    SetSuccess(DACS.T("Cập nhật thành công"));
                }
                else
                {
                    SetError(DACS.T("Cập nhật không thành công , đã xảy ra lỗi"));
                }
            }
            return RedirectToPath(DACS.ReUrl(DACS.admin +"/danhmuc/Index/{0}.html",category.Parent.ToString()));
        }

        public ActionResult Xoa()
        {
            var ids = DACS.GetBigInts(DATA, "ids");
            var entitys = _resCategory.GetItems(ids).ToList();
            if (Equals(entitys, null))
                SetError(DACS.T("Phẩn tử cần xóa không tồn tại , vui lòng kiểm tra lại"));
            else
            {
                if (_resCategory.Deletes(entitys.Select(t=>t.ID).ToArray()) >=0)
                    SetSuccess(DACS.T("Xóa thành công"));
                else
                    SetError(DACS.T("Xóa không thành công ,vui lòng kiểm tra lại"));
            }
            return RedirectToPath( GetReferrerOrDefault("/home.html"));
        }
        //huy xuất bản
        public ActionResult XuatBan()
        {
            var ids = DACS.GetBigInts(DATA, "ids");
            var entitys = _resCategory.GetItems(ids).ToList();
            if (Equals(entitys, null))
                SetError(DACS.T("Phẩn từ không tồn tại , vui lòng kiểm tra lại"));
            else
            {
                try
                {
                    entitys.ForEach(t =>
                    {
                        t.IsPublish = true;
                        t.PublishBy = CUser.ID;
                        t.Published = DateTime.Now;
                        t.BindUpdate<Category>(CUser.ID);
                    });
                }
                catch (Exception ex)
                {
                    Loger.Log(ex.ToString());
                    throw;
                }
              
                if (_resCategory.Updates(entitys,"IsPublish", "Published","Updated","UpdatedBy"))
                    SetSuccess(DACS.T("Thao tác xuất bản thành công"));
                else
                {
                    SetError(DACS.T("Thao tác xuất bản không thành công"));
                }
            }
            return RedirectToPath(GetReferrerOrDefault("/home.html"));
        }
        public ActionResult HuyXuatBan()
        {
            var ids = DACS.GetBigInts(DATA, "ids");
            var entitys = _resCategory.GetItems(ids).ToList();
            if (Equals(entitys, null))
                SetError(DACS.T("Phẩn từ không tồn tại , vui lòng kiểm tra lại"));
            else
            {
                entitys.ForEach(t =>
                {
                    t.IsPublish = false;
                    t.PublishBy = CUser.ID;
                    t.Published = DateTime.Now;
                    t.BindUpdate<Category>(CUser.ID);
                });
                if (_resCategory.Updates(entitys, "IsPublish", "Published", "Updated", "UpdatedBy"))
                    SetSuccess(DACS.T("Thao tác xuất bản thành công"));
                else
                {
                    SetError(DACS.T("Thao tác xuất bản không thành công"));
                }
            }
            return RedirectToPath(GetReferrerOrDefault("/home.html"));
        }
        private string GetCategoryParents(int idparent)
        {
            if (idparent == 0)
                return "0";
            var oCategory = _resCategory.GetById(idparent);
            if (DACS.IsEmpty(oCategory))
                return "0";
            return oCategory.Parents + "|" + idparent;
        }
    }
}