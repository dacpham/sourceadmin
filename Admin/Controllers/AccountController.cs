﻿using ShopDetail.Models;
using Facebook;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Configuration;
using System.Web.Configuration;
using System.Threading.Tasks;
using System.Net.Configuration;
using System.Net.Mail;
using System.Net;
using System.Text;
using Common.Dac;
using ShopDetail.Customs.Enum;
using ShopRetailModel.Models;
using ShopRetailModel.Interfaces;

namespace ShopAdmin.Controllers
{
    public class AccountController : BaseController
    {
        public AccountController(IUser resUser, ICategory resCategory, IExport resExport, IEvent resEvent, IExportDetail resExportDetail, IGuest resGuest, Import resImport, IOrder resOrder, IOrderDetail resOrderDetail, IProduct resProduct, IProductViewed resProductViewed, ISliderInfor resSliderInfor, IStatus resStatus, IStgFiles resStgFiles) : base(resUser, resCategory, resExport, resEvent, resExportDetail, resGuest, resImport, resOrder, resOrderDetail, resProduct, resProductViewed, resSliderInfor, resStatus, resStgFiles)
        {
        }

        [HttpGet]
        public ActionResult Login()
        {
            var crRequest = DACS.GetString(DATA, "RedirectPath");
            SetTitle(DACS.T("Đăng nhập hệ thống"));
            return GetResultOrView("Login",crRequest);
        }
        [HttpPost]
        public ActionResult Login(int id=0)
        {
            var user = new User();
            var redirectPath=DACS.Base64Decode(DACS.GetString(DATA, "RedirectPath"));
            var entity = DACS.Bind<User>(DATA);
            if(_resUser.ValidateLogin(entity.UserName, entity.PassWord, out user))
            {
                DacAuthen.SetLoginUser(user, true, HttpContext.Request);
                SetSuccess(DACS.T("Đăng nhập thành công"));
                return RedirectToPath(redirectPath);
            }
            SetError(DACS.T("Đăng nhập không thành công"));
            return GetResultOrView("Login");
        }
        [HttpGet]
        public ActionResult LogOut()
        {
            DacAuthen.SetLogOutUser(HttpContext.Request);
            return RedirectToPath("/login/account/login.html");
        }
        //public ActionResult Auth(int id = 0)
        //{
        //    switch (id)
        //    {
        //        case (int)GuestType.Facebook:

        //            Session["CPage"] = GetReferrerOrDefault("home.html");
        //            var face = new FacebookClient();
        //            var loginurl = face.GetLoginUrl(new
        //            {
        //                client_id = GlobalConfig.GetStringSetting("FbAppID"),
        //                client_secrect = GlobalConfig.GetStringSetting("FbAppSecrect"),
        //                redirect_uri = CUtility.GetRedirectUri(Request.Url, "FacebookCallback"),
        //                response_type = "code",
        //                scope = "email"
        //            });
        //            return Redirect(loginurl.AbsoluteUri);

        //        default:
        //            var guest = Utils.Bind<Guest>(DATA);
        //            TempData["UserLogin"] = guest;
        //            if (GuestRepository.ValidateLogin(guest.Username, guest.Password, out guest))
        //            {
        //                if (guest.IsApproved && guest.IsPublished)
        //                {
        //                    AclConfig.SetLogedInGuest(guest, true);
        //                    SetSuccess(Locate.T("Đăng nhập thành công"));
        //                }
        //                else
        //                {
        //                    SetError(Locate.T("Tài khoản chưa được kích hoạt từ email đăng ký"));
        //                }
        //            }
        //            else
        //            {
        //                SetError(Locate.T("Đăng nhập không thành công"));
        //            }
        //            if (HasError)
        //            {

        //                return RedirectToPath("/account/login.html");
        //            }
        //            return GetResultOrRedirectDefault("/home.html");
        //    }
        //}
        #region Facebook
        //public ActionResult FacebookCallback(string code)
        //{
        //    var facebookClient = new FacebookClient();
        //    dynamic result = facebookClient.Post("oauth/access_token", new
        //    {
        //        client_id = DACS.GetStringSetting("FbAppID"),
        //        client_secrect = DACS.GetStringSetting("FbAppSecrect"),
        //        redirect_uri = DACS.GetRedirectUri(Request.Url, "FacebookCallback"),
        //        code = code,
        //    });

        //    var accessToken = result.access_token;
        //    if (!string.IsNullOrEmpty(accessToken))
        //    {
        //        facebookClient.AccessToken = accessToken;
        //        dynamic me = facebookClient.Get("me?fields=first_name,middle_name,last_name,id,email");

        //        var guest = new Guest();
        //        guest.Email = me.email;
        //        guest.PhoneNumber = string.Empty;
        //        guest.UserName = me.first_name;
        //        guest.Code = me.id;
        //        guest.CreateDate = DateTime.Now;
        //        guest.CreatedBy = 0;
        //        guest.TypeGuest = (int)CusEnum.TypeGuest.Facebook;
        //        guest.Avatar = string.Format("https://graph.facebook.com/{0}/picture?type=large", me.id);
        //        if (GuestReponsitory.InsertOrSubmit(guest))
        //        {
        //            DacAuthen.SetLoginGuest(guest, true);
        //            SetSuccess(DACS.T("Đăng nhập thành công"));
        //        }
        //        else
        //        {
        //            SetError(DACS.T("Đăng nhập không thành công,vui lòng đăng nhập lại"));
        //        }
        //    }
        //    return RedirectToPath("/Account/FormWinDowClose.html");
        //}
        //public ActionResult FormWinDowClose()
        //{
        //    return GetCustResultOrView("FormWinDowClose");
        //}
        #endregion
    }
}