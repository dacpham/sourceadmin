﻿
using ShopRetailModel.Interfaces;
using ShopRetailModel.Params;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using ShopRetailModel.Models;

namespace ShopAdmin.Controllers
{
    public class AutoCompleteController : BaseController
    {
        public AutoCompleteController(IUser resUser, ICategory resCategory, IExport resExport, IEvent resEvent, IExportDetail resExportDetail, IGuest resGuest, Import resImport, IOrder resOrder, IOrderDetail resOrderDetail, IProduct resProduct, IProductViewed resProductViewed, ISliderInfor resSliderInfor, IStatus resStatus, IStgFiles resStgFiles) : base(resUser, resCategory, resExport, resEvent, resExportDetail, resGuest, resImport, resOrder, resOrderDetail, resProduct, resProductViewed, resSliderInfor, resStatus, resStgFiles)
        {
        }

        public JsonResult GetCategory(string q = "")
        {
            var param = new CategoryParam() { Term = q };
             var categories = _resCategory.Search(param);
          //  var categories = _resCategory.GetAll();
            return Json(categories, JsonRequestBehavior.AllowGet);
        }
    }
}