﻿using ShopRetailModel.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using PetaPoco;
using ShopRetailCore.Interfaces;

namespace ShopRetailModel.ModelRepository
{
    public static class ServiceStatic
    {
        public static User GetUserById(object id)
        {
            var db = new PetaPoco.Database("ShopRetailConnectionString");
            return db.SingleOrDefault<User>(id);
        }
        public static Guest GetGuestById(object id)
        {
            var db = new PetaPoco.Database("ShopRetailConnectionString");
            return db.SingleOrDefault<Guest>(id);
        }
    }
}
