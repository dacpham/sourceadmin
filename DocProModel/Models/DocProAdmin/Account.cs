using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Poco = PetaPoco;
namespace ShopRetailModel.Models
{
    [Table("Account")]
    public partial class Account :CreateUpdate
    {
        [Key]
        [Poco.Column]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        [Display(Name = "ID")]
        public int ID { get; set; } // int
        [Poco.Column]
        [MaxLength(30)]
        [StringLength(30)]
        [Display(Name = "Username")]
        public string Username { get; set; } // varchar(30)
        [Poco.Column]
        [MaxLength(256)]
        [StringLength(256)]
        [Display(Name = "User Password")]
        public string UserPassword { get; set; } // varchar(256)
        [Poco.Column]
        [MaxLength(120)]
        [StringLength(120)]
        [Display(Name = "Email")]
        public string Email { get; set; } // varchar(120)
        [Poco.Column]
        [MaxLength(30)]
        [StringLength(30)]
        [Display(Name = "Phone Number")]
        public string PhoneNumber { get; set; } // varchar(30)
        [Poco.Column]
        [MaxLength(300)]
        [StringLength(300)]
        [Display(Name = "Address Text")]
        public string AddressText { get; set; } // nvarchar(300)
        [Poco.Column]
        [MaxLength(1000)]
        [StringLength(1000)]
        [Display(Name = "Avatar")]
        public string Avatar { get; set; } // nvarchar(1000)
        [Poco.Column]
        [Display(Name = "Last Login")]
        public DateTime? LastLogin { get; set; } // datetime
        [Poco.Column]
        [Display(Name = "Is Locked")]
        public bool? IsLocked { get; set; } // bit
        [Poco.Column]
        [Display(Name = "Is Admin")]
        public bool? IsAdmin { get; set; } // bit
        [Poco.Column]
        [Display(Name = "Is Ldap")]
        public bool? IsLdap { get; set; } // bit
        [Poco.Column]
        [Display(Name = "Is Ctv")]
        public bool? IsCtv { get; set; } // bit
        [Poco.Column]
        [Display(Name = "Is Deleted")]
        public bool? IsDeleted { get; set; } // bit
        
    }
}
