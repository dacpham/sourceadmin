using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Poco = PetaPoco;
namespace ShopRetailModel.Models
{
 
    public partial class CreateUpdate
    {
        [Display(Name = "Created")]
        public DateTime Created { get; set; } // datetime
        [Poco.Column]
        [Display(Name = "Created By")]
        public int CreatedBy { get; set; } // int
        [Poco.Column]
        [Display(Name = "Updated")]
        public DateTime? Updated { get; set; } // datetime
        [Poco.Column]
        [Display(Name = "Updated By")]
        public int? UpdatedBy { get; set; } // int
        public void SetCreated(int iduser)
        {
            this.Created = DateTime.Now;
            this.CreatedBy = iduser;
        }
        public void SetUpdated(int iduser)
        {
            this.Updated = DateTime.Now;
            this.UpdatedBy = iduser;
        }
    }
}
