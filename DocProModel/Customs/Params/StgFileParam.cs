﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ShopRetailModel.Params
{
    public class StgFileParam
    {
        public string Term { get; set; }
        public string ID { get; set; }
    }
}
